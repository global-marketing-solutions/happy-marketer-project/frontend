import { ModelSpec } from '../mappers/projectMapper';
import { ModelSpecDTO } from '../mappers/positioningMapper';

export default (specs: ModelSpecDTO, isNestedProp = false): ModelSpec[] => {
    return Object.keys(specs).reduce((acc, specKey) => {
        return [
            ...acc,
            {
                id: specKey.toLowerCase(),
                name: specKey,
                value: isNestedProp ? (Object.values(specs[specKey]) || [])[0] : specs[specKey]
            }
        ]
    }, [] as ModelSpec[])
}
