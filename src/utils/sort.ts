export default (name: string, isAsc: boolean, a: string | number, b: string | number): number  => {
    switch(name) {

        case 'automatic':
            for (const v of [
                'fullautom.',
                'semi-autom.',
                'non-autom.'
            ]) {
                const x = decide(v, a, b, isAsc)
                if (x) return x
            }
            return 0

        case 'energy label eu':
            for (const v of [
                'a+++',
                'a++',
                'a+',
                'a',
                'b',
                'unknown',
                'not applicable'
            ]) {
                const x = decide(v, a, b, isAsc)
                if (x) return x
            }
            return 0

        case 'front decorat.':
            for (const v of [
                'others/mixed',
                'all other col',
                'stainless steel',
                'silver',
                'metal look',
                'black',
                'white/coloured',
                'white',
                'no f.decoration'
            ]) {
                const x = decide(v, a, b, isAsc)
                if (x) return x
            }
            return 0

        case 'triple classes':
            for (const v of [
                'a++aa',
                'a++ab',
                'a++ac',
                'a+ab',
                'a+ac',
                'a+ad',
                'a+ae',
                'aaa',
                'aab',
                'aac',
                'aad',
                'abe',
                'bab',
                'bac',
                'ccd',
                'n.a.',
                'unknown'
            ]) {
                const x = decide(v, a, b, isAsc)
                if (x) return x
            }
            return 0

        case 'smart connect':
            for (const v of [
                'voice control',
                'smart check/dia',
                'smart app ctrl.',
                'no',
                'N.A.'
        ]) {
            const x = decide(v, a, b, isAsc)
            if (x) return x
        }
        return 0

        default:
            if (Number(a)) {
                return isAsc ? Number(a) - Number(b) : Number(b) - Number(a)
            }

            const values = ['yes', 'no', 'n.a.', 'unknown']
            if (values.includes(String(a))) {
                for (const v of values) {
                    const x = decide(v, a, b, isAsc)
                    if (x) return x
                }
            }

            return 0
    }
}

function decide(v: string | number, a: string | number, b: string | number, isAsc: boolean): number | undefined {
    if (v === a) return isAsc ? 1 : -1
    if (v === b) return isAsc ? -1 : 1
}
