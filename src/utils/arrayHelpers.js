import _ from 'lodash';

const filterEmptyValues = (value) => !_.isArray(value) ? value : _.filter(value, (i) => !_.isEmpty(i))

const collectionMap = (collection, ...iterators) => _.flow(
    _.toPairs,
    (arr) => _.reduce(iterators, (prev, iterator) => {
        return _.map(prev, ([key, value]) => [key, iterator(value, key)])
    }, arr),
    _.fromPairs
)(collection);

const stringifyArrays = (value) => !_.isArray(value) ? value : JSON.stringify(value);

export {
    filterEmptyValues,
    collectionMap,
    stringifyArrays
}