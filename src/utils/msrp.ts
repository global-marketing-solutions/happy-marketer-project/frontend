export default (price: number, msrpVsMgp: number): number => {
    const msrp = String(Math.round(msrpVsMgp * price / 100));

    return +msrp.replace(/.$/, '9');
}
