import React from 'react';
import EntityList from '../lineup/projectList/EntityList';
import LineUpMap from './LineUpMap';
import { Switch, Route } from 'react-router-dom';

function Tools({location, isArchived = false}) {
    return (
        <section className='LineUp' style={{flex: '1'}}>
            {location.pathname === '/product-management/tools' && (
                <EntityList type={'tools'} isArchived={isArchived} />
            )}
            <Switch>
                <Route path='/product-management/tools/:documentId' component={LineUpMap} />
                <Route path='/archive/:documentId'                  component={LineUpMap} />
            </Switch>
        </section>
    )
}

export default Tools;
