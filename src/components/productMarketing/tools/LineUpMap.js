import React, {useState, useEffect} from 'react';

import './LineUpMap.css'

import Loader from '../../EmptyScreen/Loader';

import { Popups, Inputs, Button, BottomButtons, MultipleStateButton } from '../../common'

import * as DOCUMENT_ACTIONS from '../../../actions/document'

import HeaderRow from '../../productMarketing/lineup/project/HeaderRow'
import TableRow from '../../productMarketing/lineup/project/TableRow'


export default function({ match, location }) {

    const isArchived = location.pathname.startsWith('/archive')

    const [doc, setDoc] = useState()

    const [errors, setErrors] = useState(new Set())

    const [selectedStateIds, setSelectedStateIds] = useState([])

    const [isShowPopup, setIsShowPopup] = useState()

    const [isSavingPoup, setIsSavingPopup] = useState(false)
    const [isSavingMain,  setIsSavingMain] = useState(false)

    useEffect(_ => {
        DOCUMENT_ACTIONS.getDocument(match.params.documentId, isArchived).then(doc => {
            if (doc.Params) {
                setSelectedStateIds([
                    ...doc.Params.Specs[0].Values.map(v => `x_${v}`),
                    ...doc.Params.Specs[1].Values.map(v => `y_${v}`)
                ])
            } else {
                setIsShowPopup(true)

                doc.Params = {
                    Specs: [
                        {Name: '', Values: ['']},
                        {Name: '', Values: ['']}
                    ]
                }

            }

            (doc.Models || []).forEach(m => m._id = +String(Math.random()).split('.')[1])

            setDoc(doc)
        })
    }, [match.params.documentId, isArchived])

    return doc? <>
        {isShowPopup && <p id='dimOfTools'></p>}
        {isShowPopup && <section id='configurator'>
            <Popups.Close setIsShowPopup={setIsShowPopup}/>

            <div style={{display: 'flex'}}>
                <input
                    style={{margin: '0 12px', width: '176px'}}
                    placeholder='Brand name'
                    defaultValue={doc.Params.Brand}
                    onChange={e => {
                        doc.Params.Brand = e.target.value
                        setDoc({ ...doc})
                    }}
                />

                <Inputs.Select
                    style={{width: '170px'}}
                    placeholder={'Currency name'}
                    options={[
                        {value: 'EUR', label: 'EUR'},
                        {value: 'USD', label: 'USD'},
                        {value: 'CHF', label: 'CHF'},
                        {value: 'NOK', label: 'NOK'},
                        {value: 'DKK', label: 'DKK'},
                        {value: 'SEK', label: 'SEK'},
                        {value: 'GBP', label: 'GBP'},
                        {value: 'PLN', label: 'PLN'},
                        {value: 'TRY', label: 'TRY'},
                        {value: 'ILS', label: 'ILS'},
                        {value: 'RUB', label: 'RUB'},
                        {value: 'UAH', label: 'UAH'},
                        {value: 'BYN', label: 'BYN'},
                        {value: 'JPY', label: 'JPY'},
                        {value: 'CNY', label: 'CNY'},
                        {value: 'AUD', label: 'AUD'}
                    ]}
                    value={doc.Params.Currency}
                    onChange={v => {
                        doc.Params.Currency = v
                        setDoc({ ...doc})
                    }}
                    fullWidth={true}
                />

                <Inputs.Select
                    style={{width: '170px'}}
                    placeholder={'Type of the price'}
                    options={[
                        {value: 'MSRP', label: 'MSRP'},
                        {value: 'MGP', label: 'MGP'}
                    ]}
                    value={doc.Params.TypeOfPrice}
                    onChange={v => {
                        doc.Params.TypeOfPrice = v
                        setDoc({ ...doc})
                    }}
                    fullWidth={true}
                />

            </div>

            <div id='addSpec' style={{paddingRight: '35px'}}>
                <Button
                    label='ADD SPEC'
                    onPress={_ => {
                        doc.Params.Specs.push({Name: '', Values: ['']})
                        setDoc({...doc})
                    }}
                />
            </div>

            <div style={{display: 'flex', overflow: 'auto', maxHeight: '65vh'}}>

                {doc.Params.Specs.map(({Name, Values}, i) => {
                    return <div className='spec' style={{margin: '8px'}} key={i}>

                        <div style={{display: 'flex', marginBottom: '12px'}}>
                            {i > 0 && <span
                                style={{
                                    fontSize: '36px',
                                    cursor: 'pointer',
                                }}
                                onClick={_ => {
                                    [doc.Params.Specs[i],     doc.Params.Specs[i - 1]] =
                                    [doc.Params.Specs[i - 1], doc.Params.Specs[i]]
                                    setDoc({...doc})
                                }}
                            >‹</span>}
                            <input
                                style={{margin: '5px'}}
                                placeholder={i === 0 ? 'X value name' : i === 1 ? 'Y value name' : 'Value name'}
                                value={Name}
                                onChange={e => {
                                    doc.Params.Specs[i] = {Name: e.target.value, Values}
                                    setDoc({...doc})
                                    if (doc.Params.Specs.filter(s => s.Name === e.target.value).length > 1) {
                                        e.target.setCustomValidity('This spec name is duplicate, please set another entry')
                                        e.target.reportValidity()
                                        setErrors(errors => errors.add(i))
                                    } else {
                                        e.target.setCustomValidity('')
                                        errors.delete(i)
                                        setErrors(new Set(errors))
                                    }
                                }}
                                key={i}
                            />
                            {i < doc.Params.Specs.length - 1 && <span
                                style={{
                                    fontSize: '36px',
                                    cursor: 'pointer',
                                }}
                                onClick={_ => {
                                    [doc.Params.Specs[i],     doc.Params.Specs[i + 1]] =
                                    [doc.Params.Specs[i + 1], doc.Params.Specs[i]]
                                    setDoc({...doc})
                                }}
                            >›</span>}
                        </div>

                        {
                            Values.map((v, j) =>
                                <div className='specRow' key={j}>
                                    <div style={{display: 'flex', height: '36px', textAlign: 'right'}}>
                                        {j > 0 && <div className='arrow' style={{
                                            fontSize: '31px',
                                            transform: 'rotate(90deg)',
                                            position: 'relative',
                                            top: '-5px',
                                            left: '47px',
                                            cursor: 'pointer',
                                        }}
                                        onClick={_ => {
                                            [doc.Params.Specs[i].Values[j],     doc.Params.Specs[i].Values[j - 1]] =
                                            [doc.Params.Specs[i].Values[j - 1], doc.Params.Specs[i].Values[j]]
                                            setDoc({...doc})
                                        }}>‹</div>}
                                        {j < Values.length - 1 && <div className='arrow' style={{
                                            position: 'relative',
                                            fontSize: '31px',
                                            top: '9px',
                                            left: j > 0 ? '29px' : '36px',
                                            transform: 'rotate(-90deg)',
                                            cursor: 'pointer',
                                        }}
                                        onClick={_ => {
                                            [doc.Params.Specs[i].Values[j],     doc.Params.Specs[i].Values[j + 1]] =
                                            [doc.Params.Specs[i].Values[j + 1], doc.Params.Specs[i].Values[j]]
                                            setDoc({...doc})
                                        }}>‹</div>}
                                    </div>
                                    <input
                                        placeholder='Value'
                                        value={v}
                                        onChange={e => {
                                            doc.Params.Specs[i].Values[j] = e.target.value
                                            setDoc({...doc})
                                            if (doc.Params.Specs[i].Values.filter(v => v === e.target.value).length > 1) {
                                                e.target.setCustomValidity('This value is duplicate, please set another entry')
                                                e.target.reportValidity()
                                                setErrors(errors => errors.add(`${i}-${j}`))
                                            } else {
                                                e.target.setCustomValidity('')
                                                errors.delete(`${i}-${j}`)
                                                setErrors(new Set(errors))
                                            }
                                        }}
                                        key={j}
                                    />
                                    {j > 0 && <img
                                        src='../../../images/icons/remove.svg'
                                        alt='remove'
                                        style={{width: '15px'}}
                                        onClick={_ => {
                                            doc.Params.Specs[i].Values.splice(j, 1)
                                            setDoc({...doc})
                                        }}
                                    />}
                                </div>
                            )
                        }

                        <Button
                            label='ADD VALUE'
                            margined={10}
                            centered={true}
                            onPress={_ => {
                                doc.Params.Specs[i].Values.push('')
                                setDoc({...doc})
                            }}
                        />

                        {i > 1 && <Button
                            label='DELETE VALUE'
                            margined={10}
                            centered={true}
                            destruction={true}
                            onPress={_ => {
                                doc.Params.Specs.splice(i, 1)
                                setDoc({...doc})
                            }}
                        />}

                    </div>
                })}

            </div>

            <Button
                label='APPLY'
                centered={true}
                showLoader={isSavingPoup}
                onPress={_ => {
                    if (errors.size) {
                        alert('Please use unique spec names and values')
                        return
                    }

                    if (!doc.Params.Specs.some(s => s.Name) || !doc.Params.Specs.some(s => s.Values.some(v => v))) {
                        alert('Please fill specs')
                        return
                    }

                    doc.Params.Specs = doc.Params.Specs.filter(s =>
                        s.Name && s.Values.some(v => v)
                    )
                    doc.Params.Specs.forEach(({Name, Values}, i, arr) => {
                        arr[i] = {Name, Values: Values.filter(v => v)}
                    })

                    setIsSavingPopup(true)
                    DOCUMENT_ACTIONS.saveDocumentParams(doc.Name, doc.OwnerUsername, doc.Params).then(async _ => {

                        const defer = function() {
                            setSelectedStateIds([
                                ...doc.Params.Specs[0].Values.map(v => `x_${v}`),
                                ...doc.Params.Specs[1].Values.map(v => `y_${v}`)
                            ])

                            setIsSavingPopup(false)
                            setIsShowPopup(false)
                        }

                        if (!doc.Models) {
                            defer()
                            return
                        }

                        const Models = doc.Models.reduce((acc, m) => {
                            const spX = doc.Params.Specs.find(s => s.Name === m.specs[0].name)
                            const spY = doc.Params.Specs.find(s => s.Name === m.specs[1].name)
                            if (spX && spX.Values.includes(m.specs[0].value) &&
                                spY && spY.Values.includes(m.specs[1].value)) {

                                    m.specs = m.specs.map(s => doc.Params.Specs.find(S => S.Name === s.name)
                                        .Values.includes(s.value) ? s : {...s, value: '-'}
                                    )

                                    acc.push(m)

                            }

                            return acc

                        }, [])

                        if (Models.length !== doc.Models.length) {
                            await DOCUMENT_ACTIONS.saveModels(doc.Name, doc.OwnerUsername, Models)
                        }

                        setDoc({
                            ...doc,
                            Models
                        })

                        defer()
                    })
                }}
            />

        </section>}

        <label
            style={{cursor: 'pointer', paddingBottom: '30px'}}
            onClick={_ =>
                setIsShowPopup(true)
            }
        >
            Document name:
            <h1 style={{display: 'inline'}}>{match.params.documentId}</h1>
            <img
                src={require('../../../images/icons/settings.svg')}
                style={{
                    position: 'relative',
                    top: '4px',
                    left: '4px'
                }}
                alt=''
            />
        </label>

        {!isShowPopup && <table style={{
            borderSpacing: '10px',
            transformOrigin: '0 0',
            transform: 'scale(calc(var(--zoom) / 100), calc(var(--zoom) / 100))'
        }}>
            <thead>

                <tr>

                    <td><MultipleStateButton
                        name={'X-Y axis'}
                        style={{position: 'absolute'}}
                        states={(_ => {
                            const states = [
                                ...doc.Params.Specs[0].Values
                                    .map(v => ({id: `x_${v}`, label: v })),
                                ...doc.Params.Specs[1].Values
                                    .map(v => ({id: `y_${v}`, label: v }))
                            ]
                            const i = states.findIndex(s => s.id.startsWith('y_'))
                            const xSpecName = doc.Params.Specs[0].Name
                            const ySpecName = doc.Params.Specs[1].Name
                            states.splice(i, 0, {id: ySpecName, label: ySpecName, class: 'disabled'})
                            return [
                                {id: xSpecName, label: xSpecName, class: 'disabled'},
                                ...states
                            ]
                        })()}
                        selectedStateIds={selectedStateIds}
                        onChangeSelectedState={ids => setSelectedStateIds([
                            ...doc.Params.Specs[0].Values
                                .filter(x => ids.includes(`x_${x}`))
                                .map(v => `x_${v}`),
                            ...doc.Params.Specs[1].Values
                                .filter(y => ids.includes(`y_${y}`))
                                .map(v => `y_${v}`),
                        ])}
                    /></td>

                    {doc.Params.Specs[1].Values
                        .map(y => <td key={y}></td>)
                    }

                    <td style={{ display: 'flex', justifyContent: 'center' }}>
                        {<Button onPress={_ => createNewModel(setDoc)} label={'ADD NEW'} />}
                    </td>

                </tr>

                <HeaderRow
                    columns={selectedStateIds
                        .filter(s => s.startsWith('y_'))
                        .map(s => {
                            const v = s.slice(2)
                            const i = doc.Params.Specs[1].Values.findIndex(_v => _v === v)
                            const col = new String(doc.Params.Specs[1].Values[i])  // eslint-disable-line no-new-wrappers
                            col.percent = (doc.Params.SpecsPercentsY || [])[i]
                            return col
                        })
                    }
                    callback={(v, n) => {
                        const i = doc.Params.Specs[1].Values.findIndex(_v => _v === v)
                        if (!doc.Params.SpecsPercentsY) {
                             doc.Params.SpecsPercentsY = []
                        }
                        doc.Params.SpecsPercentsY[i] = n
                        setDoc({...doc})
                    }}
                    newCodes={true}
                    showInput={true}
                />

            </thead>
            <tbody>

                {selectedStateIds
                    .filter(s => s.startsWith('x_'))
                    .map(_v => {
                        const v = _v.slice(2)
                        const i = doc.Params.Specs[0].Values.findIndex(_v => _v === v)
                        const modelsForRow = (doc.Models || []).filter(m => m.specs.some(s => s.value === v))
                        const modelsAdapted = modelsForRow.map(m => ({
                            ...m,
                            changedSpecs: m.changedSpecs || m.specs,
                            typeOfPrice: doc.Params.TypeOfPrice
                        }))
                        return <TableRow
                            key={v}
                            title={{
                                toString: () => v,
                                percent: (doc.Params.SpecsPercentsX || [])[i],
                                callback: (v, n) => {
                                    const i = doc.Params.Specs[0].Values.findIndex(_v => _v === v)
                                    if (!doc.Params.SpecsPercentsX) {
                                         doc.Params.SpecsPercentsX = []
                                    }
                                    doc.Params.SpecsPercentsX[i] = n
                                    setDoc({...doc})
                                }
                            }}
                            ySpec={{id: doc.Params.Specs[1].Name}}
                            columnValues={selectedStateIds
                                .filter(s => s.startsWith('y_'))
                                .map(s => s.slice(2))
                            }
                            specs={doc.Params.Specs.map(s => ({id: s.Name, name: s.Name, value: s.Values}))}
                            models={modelsAdapted}
                            currency={doc.Params.Currency}
                            brand={doc.Params.Brand}
                            isEditable={true}
                            showPrice={false}
                            onChangeModel={mn => {
                                const i = doc.Models.findIndex(m => m._id === mn._id)
                                doc.Models[i] = mn
                                setDoc({...doc})
                            }}
                        />
                    })
                }

        </tbody>
        </table>}

        <BottomButtons>
            <Button label={'APPLY'}
                showLoader={isSavingMain}
                onPress={() => {

                    if (doc.Models) {
                        const s = new Set()
                        doc.Models.forEach(m => s.add(m.id))
                        if (s.size !== doc.Models.length) {
                            alert('Model names must be unique')
                            return
                        }
                    }

                    setIsSavingMain(true)

                    const models = (doc.Models || []).filter(m => !m.toDelete)

                    Promise.all([
                        DOCUMENT_ACTIONS.saveModels(
                            doc.Name,
                            doc.OwnerUsername,
                            models.map(m => {
                                m.specs = m.changedSpecs || m.specs
                                delete m.changedSpecs
                                return m
                            })
                        ),
                        DOCUMENT_ACTIONS.saveDocumentParams(doc.Name, doc.OwnerUsername, doc.Params)
                    ]).then(_ => {
                            setDoc({
                                ...doc,
                                Models: models.map(m => {
                                    m.specs = m.changedSpecs || m.specs
                                    delete m.isNew
                                    return m
                                })
                            })
                            setIsSavingMain(false)
                        })
                }}
                isBottomButton={true}
            />
        </BottomButtons>

    </> : <Loader/>
}


function createNewModel(setDoc) {

    setDoc(doc => {
        if (!doc.Models)
             doc.Models = []

        doc.Models.push({
            _id: Date.now(),
            isNew: true,
            specs: doc.Params.Specs.reduce((acc, {Name, Values}) => ([
                ...acc,
                {
                    id: Name,
                    name: Name,
                    value: Values[0]
                }
            ]), []),
            typeOfPrice: doc.Params.TypeOfPrice,
            currency: doc.Params.Currency
        })

        return {...doc}
    })
}
