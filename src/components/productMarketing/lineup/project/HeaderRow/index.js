import React from 'react';
import HeaderCell from '../HeaderCell';
import './HeaderRow.css';

function HeaderRow({ firstTitle = '', columns, newCodes, callback }) {
    return (
        <tr>
            <th className='cell'>
                {!!firstTitle && firstTitle}
            </th>
            {columns.map(yValue => (<th className='cell' key={yValue}>
                <HeaderCell
                    alignment='horizontal'
                    title={yValue}
                    callback={callback}
                />
            </th>))}
            {newCodes && (<th className='cell'>
                <HeaderCell
                    alignment='horizontal'
                    title={'New Codes'}
                />
            </th>)}
        </tr>
    )
}

export default HeaderRow;
