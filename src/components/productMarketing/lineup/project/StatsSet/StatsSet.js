import React from 'react';
import './StatsSet.css'

function StatsSet({ percent, empty = false, hide = false }) {
    if (hide) return <div></div>
    return !empty ? (
        <div className='StatsSet'>
            {percent}%
            <span className='stats-icon'>monetization_on</span>
            <span className='stats-icon'>pie_chart</span>
        </div>
    ) : (<div className='StatsSet'></div>)
}

export default StatsSet;