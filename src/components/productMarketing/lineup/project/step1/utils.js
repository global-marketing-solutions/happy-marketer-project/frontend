import _ from 'lodash';
import {Regexes, Validators} from '../../../../common/Regexes'

const isNotEmpty = (val) => Array.isArray(val) ? val.reduce((acc, v) => acc && !!v, true) : !!val
const arrayLengthIsMoreOrEqual = (count) => (val) => val.length >= count

export function isValid(project, specs = []) {
    const maxModelsNumber = _.reduce(specs, (a, s) => a * _.size(_.get(s, 'value', [])), 1) || 99;
    const rules = {
        brand: [isNotEmpty, brand => brand.match(Regexes.someText)],
        numberOfModels: [isNotEmpty, Validators.gt(1), Validators.lte(maxModelsNumber)],
        category: [isNotEmpty],
        channel: [isNotEmpty],
        competitors: [isNotEmpty],//SS
        countries: [isNotEmpty],//SS
        currency: [isNotEmpty],//S
        priceIndex: [isNotEmpty],//N
        segment: [isNotEmpty],//S
        specs: [isNotEmpty, arrayLengthIsMoreOrEqual(2)],//SS
    };

    return _.reduce(project, (res, value, key) => {
        return res && _.reduce(rules[key], (acc, item) => acc && item(value), true)
    }, true)
}