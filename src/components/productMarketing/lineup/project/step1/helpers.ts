import _ from 'lodash';
import { Regexes, Validators } from '../../../../common/Regexes';
import { Project } from '../../../../../mappers/projectMapper';
import { Parameters } from '../../../../../mappers/parametersMapper';

const isNotEmpty = (val: any): boolean => Array.isArray(val) ? val.reduce((acc, v) => acc && !!v, true) : !!val
const arrayLengthIsMoreOrEqual = (count: number) => (val: Array<any>) => val.length >= count

export function isValid(project: Project | null, parameters: Parameters): boolean {
    if (project === null) return false;
    const maxModelsNumber = _.reduce(_.filter(parameters.specs, s => project.params?.specs?.includes(s.name)), (acc, s) => acc * _.size(_.get(s, 'value', [])), 1) || 99;
    const rules = {
        brand: [isNotEmpty, (brand: any) => !!brand.match(Regexes.someText)],
        numberOfModels: [isNotEmpty, Validators.gte(1), Validators.lte(maxModelsNumber)],
        category: [isNotEmpty],
        channel: [isNotEmpty],
        competitors: [isNotEmpty],//SS
        countries: [isNotEmpty],//SS
        currency: [isNotEmpty],//S
        priceIndex: [isNotEmpty],//N
        segment: [isNotEmpty],//S
        specs: [isNotEmpty, arrayLengthIsMoreOrEqual(2)],//SS
    };

    return _.reduce(project.params, (res, value, key) => {
        //@ts-ignore
        return res && _.reduce(rules[key], (acc, item) => acc && item(value), true);
    }, true);
}