import React, { useState, useEffect, useRef } from 'react';
import _ from 'lodash';

import './Step1.css';

import Loader from '../../../../EmptyScreen/Loader';
import { getParams } from '../../../../../actions/parameters';
import { getLineUpMap } from '../../../../../actions/lineupmap';
import * as PROJECT_ACTIONS from '../../../../../actions/project';
import { getPositioning } from '../../../../../actions/positioning';
import { BottomButtons, Button, Inputs } from '../../../../common';
import { isValid } from './helpers';
import msrp from '../../../../../utils/msrp';

export const emptyProjectParams = {
    countries: [''],
    category: '',
    segment: '',
    channel: '',
    specs: ['', ''],
    brand: '',
    numberOfModels: '',
    priceIndex: '',
    currency: '',
    competitors: [''],
}

function idNameToValueLabel(item) {
    return { value: item.id, label: item.name };
}

function findValue(options, name) {
    if (Array.isArray(name)) {
        return name.map(v => findValue(options, v)) || '';
    } else {
        return (options.find(o => o.name === name) || {}).id || ''
    }
}

function findName(options, value) {
    if (Array.isArray(value)) {
        return value.map(v => findName(options, v));
    } else {
        return (options.find(o => o.id === value) || {}).name
    }
}

function Step1({ onNextStep, newProject, setNewProject, disableEdit }) {
    const [localProject, setLocalProject] = useState(newProject || {});
    const [parameters, setParameters] = useState({});
    const [priceRange, setPriceRange] = useState({});
    const [restrictions, setRestrictions] = useState();
    const [rstrs, setRstrs] = useState();
    const [numberOfModelsMax, setNumberOfModelsMax] = useState();

    const [isSaving, setIsSaving] = useState(false);

    useEffect(() => {
        setLocalProject({
            ...newProject,
            params: !_.isEmpty(newProject.params) ? newProject.params : emptyProjectParams
        });

        if (newProject.params) {
            setRestrictions(newProject.params.restrictions);
        }
    }, [newProject]);

    const prevProjParams = useRef(localProject.params);
    useEffect(() => {
        if (!_.isEmpty(localProject) && (!_.isEmpty(_.xor(
            _.values(_.pick(prevProjParams.current, ['countries', 'category', 'segment', 'channel'])),
            _.values(_.pick(localProject.params,    ['countries', 'category', 'segment', 'channel'])),
        )) || _.isEmpty(parameters))) {
            getParams(localProject.params).then(params => {
                setParameters(params);
                if (params?.priceIndexRange) {
                    setPriceRange({
                        min: params.priceIndexRange.Min,
                        max: params.priceIndexRange.Max,
                        value: priceRange.value
                    })
                }
            });
        }
        prevProjParams.current = localProject.params;
    }, [localProject, parameters, priceRange]);

    const getParametersValues = (paramName) => {
        return _.get(parameters, `${paramName}.values`, []) || []
    }

    const newUpdateProperty = (propKey, params) => (val) => {
        const inputOrder = ['countries', 'category', 'segment', 'channel', 'specs'];
        const order = inputOrder.findIndex(a => a === propKey)
        if (~order) {
            setRestrictions();
            localProject.params.restrictions = null;
        }
        let models = localProject.models;

        if (['countries', 'category', 'segment'].includes(propKey) && priceRange.value) {
            setPriceRange({
                ...priceRange,
                ...{
                    value: null
                }
            });
        }

        if (['specs', 'restrictions', 'numberOfModels'].includes(propKey) ||
            (propKey === 'priceIndexRange' &&
                localProject.params.priceIndexRange &&
                !_.isEqual(localProject.params.priceIndexRange.Value, val.Value))) {
            models = null;
        }
        if (propKey === 'priceIndex' && localProject.models) {
            models = localProject.models.map(m => ({ ...m, price: 0 }))
        }
        setLocalProject({
            ...localProject,
            models,
            params: {
                ...localProject.params,
                [propKey]: params ? findName(params, val) : propKey === 'priceIndex' ? +val : val,
                ..._.pick(
                    emptyProjectParams,
                    inputOrder.slice(
                        ~order ? order + 1 : inputOrder.length + 1,
                        inputOrder.length + 1
                    )
                )
            }
        })
    }

    useEffect(_ => {
        setRstrs((restrictions || []).reduce((acc, val) => {
            acc[val.specId].add(val.value);
            return acc;
        }, new Proxy({}, { get: (target, name) => target[name] || (target[name] = new Set()) })));
    }, [restrictions]);

    useEffect(() => {
        if (localProject.params?.specs[0] && localProject.params.specs.every(s => s)) {
            getLineUpMap({...localProject.params, numberOfModels: 0}).then(lineupmap => {
                // TODO do not fetch Step 2 again if we already have data?
                setNumberOfModelsMax(lineupmap?.models?.length);
            });
        } else {
            setNumberOfModelsMax();
        }
    }, [localProject]);


    if (!localProject || _.isEmpty(parameters)) return <div className='loaderWrapper'><Loader /></div>;

    return (
        <>
            <section className='Step1'>
                {localProject && !_.isEmpty(parameters) && (
                    <div className='columns'>
                        <div className='column'>
                            <Inputs.Select
                                style={{ textTransform: 'capitalize' }}
                                label={'Country'}
                                placeholder={'Select country...'}
                                options={getParametersValues('countries').map(idNameToValueLabel)}
                                isMultiple={true}
                                value={findValue(getParametersValues('countries'), localProject.params.countries)}
                                onChange={newUpdateProperty('countries', getParametersValues('countries'))}
                                addLabel={'ADD COUNTRY'}
                                disabled={disableEdit}
                            />

                            <Inputs.Select
                                label={'Category'}
                                placeholder={'Select category...'}
                                options={getParametersValues('categories').map(idNameToValueLabel)}
                                value={findValue(getParametersValues('categories'), localProject.params.category)}
                                onChange={newUpdateProperty('category', getParametersValues('categories'))}
                                disabled={disableEdit}
                                fullWidth={true}
                            />

                            <Inputs.Select
                                label={'Segment'}
                                placeholder={'Select segment...'}
                                options={getParametersValues('segments').map(idNameToValueLabel)}
                                value={findValue(getParametersValues('segments'), localProject.params.segment)}
                                onChange={newUpdateProperty('segment', getParametersValues('segments'))}
                                disabled={disableEdit}
                                fullWidth={true}
                            />

                            <Inputs.Toggle
                                label={'Price Index Range'}
                                disabled={disableEdit || !localProject.params.segment}
                                value={!!localProject.params.priceIndexRange}
                                onChange={() => {
                                    if (localProject.params.priceIndexRange) {
                                        setLocalProject({
                                            ...localProject,
                                            params: {
                                                ...localProject.params,
                                                priceIndexRange: null
                                            }
                                        })
                                        setPriceRange({ ...priceRange, value: null })
                                    } else {
                                        setLocalProject({
                                            ...localProject,
                                            params: {
                                                ...localProject.params,
                                                priceIndexRange: {
                                                    Value: [priceRange.min, priceRange.max],
                                                    Percent: parameters.priceIndexRange.Percent
                                                }
                                            }
                                        })
                                    }
                                }}
                            />

                            {
                                localProject.params.priceIndexRange && <Inputs.RangeSelect
                                    min={priceRange.min}
                                    max={priceRange.max}
                                    value={localProject.params.priceIndexRange.Value}
                                    onChange={value => {
                                        const pir = localProject.params.priceIndexRange.Value;
                                        if (value[0] !== pir[0] || value[1] !== pir[1]) {
                                            setLocalProject({
                                                ...localProject,
                                                params: {
                                                    ...localProject.params,
                                                    priceIndexRange: {
                                                        Value: value,
                                                        Percent: localProject.params.priceIndexRange.Percent
                                                    }
                                                }
                                            })
                                        }
                                    }}
                                />
                            }

                            <Inputs.Select
                                label={'Channel'}
                                placeholder={'Select channel...'}
                                options={getParametersValues('channels').map(idNameToValueLabel)}
                                value={findValue(getParametersValues('channels'), localProject.params.channel)}
                                onChange={newUpdateProperty('channel', getParametersValues('channels'))}
                                showAddLabel={false}
                                disabled={disableEdit}
                                fullWidth={true}
                            />

                            <Inputs.Select
                                label={'Specs Prioritization'}
                                placeholder={'Select specs prioritization...'}
                                options={parameters.specs.map(idNameToValueLabel)}
                                isMultiple={true}
                                value={findValue(parameters.specs, localProject.params.specs)}
                                onChange={newUpdateProperty('specs', parameters.specs)}
                                minCountOfValues={2}
                                addLabel={'ADD SPEC'}
                                disabled={disableEdit}
                            />

                        </div>

                        <div className='column'>

                            <Inputs.Input
                                label={'Brand Name'}
                                value={localProject.params.brand}
                                onChange={newUpdateProperty('brand')}
                                disabled={disableEdit}
                            />

                            <Restrictions
                                restrictions={restrictions}
                                setRestrictions={setRestrictions}
                                rstrs={rstrs}
                                updateProperty={newUpdateProperty('restrictions')}
                                specs={parameters.specs.filter(s =>
                                    localProject.params.specs.find(name => s.name === name)
                                )}
                                disableEdit={disableEdit}
                            />

                            <p>Number of models (max {numberOfModelsMax})</p>

                            <Inputs.Input
                                label={`Initial Request`}
                                type='number'
                                classAdditional='line noMargin'
                                pattern='\\d'
                                min={'1'}
                                max={numberOfModelsMax}
                                value={localProject.params.numberOfModels}
                                onChange={newUpdateProperty('numberOfModels')}
                                disabled={disableEdit}
                            />

                            <Inputs.Input
                                label={'Current Models'}
                                classAdditional='line'
                                placeholder=''
                                value={(localProject.models || []).length}
                                disabled={true}
                            />

                        </div>

                        <div className='column'>
                            <Inputs.Input
                                label={'MGP vs avg'}
                                type='number'
                                classAdditional='line'
                                value={localProject.params.priceIndex}
                                onChange={newUpdateProperty('priceIndex')}
                                disabled={disableEdit}
                                after='%'
                            />
                            <Inputs.Input
                                label={'MSRP vs MGP'}
                                type='number'
                                classAdditional='line'
                                value={localProject.params.msrpVsMgp}
                                onChange={newUpdateProperty('msrpVsMgp')}
                                disabled={disableEdit}
                                after='%'
                            />
                            <Inputs.Select
                                label={'Currency'}
                                classAdditional='line selectValueUppercase'
                                placeholder={'Select...'}
                                options={getParametersValues('currencies').map(idNameToValueLabel)}
                                value={findValue(getParametersValues('currencies'), localProject.params.currency)}
                                onChange={newUpdateProperty('currency', getParametersValues('currencies'))}
                                disabled={disableEdit}
                            />
                            <Inputs.Select
                                label={'Competitors'}
                                classAdditional='selectValueUppercase'
                                placeholder={'Select competitors...'}
                                options={getParametersValues('competitors').map(idNameToValueLabel)}
                                isMultiple={true}
                                value={findValue(getParametersValues('competitors'), localProject.params.competitors)}
                                onChange={newUpdateProperty('competitors', getParametersValues('competitors'))}
                                addLabel={'ADD COMPETITOR'}
                                disabled={disableEdit}
                            />
                        </div>
                    </div>
                )}

            </section>

            {!_.isEmpty(parameters) && <BottomButtons>
                <Button
                    label={'APPLY'}
                    margined={20}
                    isBottomButton={true}
                    showLoader={isSaving}
                    disabled={!isValid(localProject, parameters) || _.isEqual(localProject, newProject)}
                    onPress={async () => {
                        setIsSaving(true)
                        let projectToPersist = localProject
                        projectToPersist.params.selectedStateIds = null
                        if (localProject.models) {
                            if (newProject.params.priceIndex !== localProject.params.priceIndex)
                                projectToPersist = await updatePrices(localProject, 'mgp')
                            else
                                if (newProject.params.msrpVsMgp !== localProject.params.msrpVsMgp)
                                    projectToPersist = await updatePrices(localProject, 'msrp')
                        }
                        PROJECT_ACTIONS.saveProject(projectToPersist).then(() => {
                            setNewProject(projectToPersist);
                            setIsSaving(false)
                        })
                    }}
                />
                <Button
                    label={'NEXT'}
                    margined={20}
                    isBottomButton={true}
                    disabled={!isValid(localProject, parameters) || !_.isEqual(localProject, newProject) || disableEdit}
                    onPress={onNextStep}
                />
            </BottomButtons>}

        </>
    )
}

function updatePrices(localProject, type) {
        const priceIndexRange = localProject.params.priceIndexRange
        return getPositioning(localProject.owner, localProject.name, priceIndexRange ? JSON.stringify([
        priceIndexRange.Value[0] * priceIndexRange.Percent | 0,
        priceIndexRange.Value[1] * priceIndexRange.Percent | 0
    ]) : '').then(positioning => {
        return {
            ...localProject,
            models: localProject.models.map((m, i) => {
                const price = Math.round(positioning.models[i].price * localProject.params.priceIndex / 100)
                const _m = {
                    ...m,
                    ...{
                        msrp: msrp(price, localProject.params.msrpVsMgp)
                    }
                }
                if (type === 'mgp')
                    _m.price = price

                return _m
            })
        }
    })
}

function Restrictions({ restrictions, setRestrictions, rstrs, updateProperty, specs, disableEdit }) {

    const emptyRestriction = { specId: '', value: '' };

    return (
        <>
            <Inputs.Toggle
                label={'Restrictions'}
                disabled={disableEdit}
                value={!!restrictions}
                onChange={_ => {
                    if (restrictions) {
                        setRestrictions(null);
                        updateProperty(null);
                    } else {
                        setRestrictions([emptyRestriction]);
                    }
                }}
            />

            {
                restrictions && restrictions.map((restriction, ind) => {
                    const selectedSpec = specs.find(s => s.id === restriction.specId) || { id: '', value: [] };
                    return (
                        <div key={ind} style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Inputs.Select
                                style={{ width: '120px' }}
                                placeholder={'Select spec'}
                                classAdditional={'noMargin'}
                                options={specs.map(item => {
                                    if (rstrs[item.id].size === item.value.length - 1)
                                        return Object.assign(idNameToValueLabel(item), { isDisabled: true });
                                    return idNameToValueLabel(item);
                                })}
                                isMultiple={false}
                                value={selectedSpec.id}
                                onChange={(specId) => {
                                    const defaultValue = specs.find(s => s.id === specId).value
                                        .find(v => !rstrs[specId].has(v));
                                    const values = [
                                        ...restrictions.slice(0, ind),
                                        { specId, value: defaultValue },
                                        ...restrictions.slice(ind + 1, restrictions.length + 1)
                                    ];
                                    setRestrictions(values);
                                    updateProperty(values);
                                }}
                                disabled={disableEdit}
                                fullWidth={true}
                            />
                            <Inputs.Select
                                style={{ width: '120px' }}
                                placeholder={'.. value'}
                                options={[...selectedSpec.value
                                    .filter(v => !rstrs[selectedSpec.id].has(v)).map(a => ({
                                        value: a,
                                        label: a
                                    })),
                                { value: restriction.value, label: restriction.value }]
                                }
                                isMultiple={false}
                                classAdditional={'noMargin'}
                                value={restriction.value}
                                onChange={specValue => {
                                    setRestrictions(restrictions => {
                                        const _restrictions = [
                                            ...restrictions.slice(0, ind),
                                            { ...restriction, value: specValue },
                                            ...restrictions.slice(ind + 1, restrictions.length + 1)
                                        ]
                                        updateProperty(_restrictions);
                                        return _restrictions;
                                    });
                                }}
                                onDelete={() => {
                                    setRestrictions(restrictions => {
                                        const _restrictions = restrictions.filter((_, i) => i !== ind);
                                        updateProperty(_restrictions);
                                        return _restrictions;
                                    });
                                }}
                                disabled={disableEdit}
                                hiddenDelete={ind === 0}
                            />
                        </div>
                    )
                })
            }

            {restrictions && !disableEdit && (<div className='input-group' style={{ marginTop: 10 }}>
                <Button label={'ADD SPEC'}
                    centered={true}
                    onPress={() => {
                        setRestrictions([...restrictions, emptyRestriction])
                    }} />
            </div>)}

        </>
    )
}

export default Step1;
