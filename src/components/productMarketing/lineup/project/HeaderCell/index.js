import React from 'react';
import './HeaderCell.css';

function HeaderCell({ alignment = 'horizontal', title, subtitle, callback }) {
    const minWidth = alignment === 'top' ? 0 : alignment === 'vertical' ? 40 : 150;
    const minHeight = alignment === 'top' ? 0 : alignment === 'horizontal' ? 40 : 160;
    const addStyles = (!subtitle && alignment === 'vertical') ?
        {transform: 'rotate(-180deg)', writingMode: 'vertical-lr'} : {};
    return (
        <div
            className='cell-wrapper'
            style={{
                minWidth,
                minHeight,
                display: 'flex'
            }}
        >
            <div className='content-block header-element' style={{
                paddingLeft: alignment === 'vertical' ? 10 : 0, 
                paddingRight: alignment === 'vertical' ? 10 : 0
            }}>
                <span style={addStyles}>{title}</span>
                {callback && <span style={{position: 'absolute', right: 0}}>
                    <input
                        type='number'
                        min='1'
                        defaultValue={title.percent}
                        onChange={e => callback(title.toString(), +e.target.value)}
                        style={{
                            width: title.percent > 9 ? '31px' : '21px',
                            height: '13px',
                            background: '#eee',
                            border: 'none',
                            textAlign: 'left'
                        }}/>
                    <span style={{position: 'relative', 'left': '-17px'}}>%</span>
                </span>}
                {subtitle && <div style={{ marginTop: 10 }}>{subtitle}</div>}
            </div>
        </div>
    );
}

export default HeaderCell;
