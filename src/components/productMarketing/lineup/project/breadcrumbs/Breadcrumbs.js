import React from 'react';
import './Breadcrumbs.css'
import Breadcrumb from './Breadcrumb';

function Breadcrumbs({breadcrumbs, isActive}) {
    return (
        <div className='breadcrumbs'>
            {breadcrumbs.map((bc, i) => <Breadcrumb key={i} {...bc} enabled={isActive(bc.id)} />)}
        </div>
    )
}

export default Breadcrumbs;