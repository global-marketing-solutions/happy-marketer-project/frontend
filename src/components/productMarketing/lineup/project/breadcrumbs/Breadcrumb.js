import React from 'react';
import './Breadcrumb.css'
import { Link, useLocation } from "react-router-dom";

function Breadcrumb({ id, title, description, enabled = true }) {
    const location = useLocation();
    const isActive = location.pathname.endsWith(id);
    return (
        <Link
            className={`${enabled ? '' : 'disabled'}`}
            to={enabled ? `./${id}` : location.pathname}
        >
            <div className={`breadcrumb ${isActive ? 'selected' : ''}`}>
                <div className='title'>{title}</div>
                <div className='subtitle'>{description}</div>
            </div>
        </Link>
    )
}

export default Breadcrumb;
