import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import './Step5.css';
import HeaderRow from '../HeaderRow';
import TableRow from '../TableRow';
import sort from '../../../../../utils/sort';
import Loader from '../../../../EmptyScreen/Loader';
import { BottomButtons, Button, Popups, MultipleStateButton } from '../../../../common';
import { prod } from '../../../../../config.json';

import * as PROJECT_ACTIONS from '../../../../../actions/project';
import { getParams } from '../../../../../actions/parameters';
import { getLineUpMap } from '../../../../../actions/lineupmap';
import { getPositioning } from '../../../../../actions/positioning';
import { getHeaders } from '../../../../../actions/headers';

import { finCols, kpis } from '../step4/Step4';
import { KPI_IDS } from '../step4/ModelLine';
import { sumArray, getFinancials } from '../step4/ModelLine';
import { formatPriceNumber, getCurrencySymbol, formatNumber } from '../../../../utils/formatPrice';
import msrp from '../../../../../utils/msrp';
import countries from '../../../../common/Inputs/countriesList';
import '../../../../common/Inputs/countries.css';

const DEFAULT_SELECTED_FINANCIAL_COLUMNS = finCols.filter(f => !f.hidden && f.showOnCard && f.id !== 'msrp')
    .map(f => f.id);

const modelSums = {volume: [0], value: [0], margin: [0]};

function Step5({
    newProject,
    setNewProject,
    disableEdit
}) {
    const [xSpec, setXSpec] = useState(null);
    const [ySpec, setYSpec] = useState(null);
    const [specs, setSpecs] = useState(null);

    const [selectedStateIdsDefault, setSelectedStateIdsDefault] = useState()
    const [selectedStateIds, setSelectedStateIds] = useState()

    const [segmentDescriptions, setSD] = useState(null);
    const [localModels, setLocalModels] = useState(null);

    const [selectedSpecs, setSelectedSpecs] = useState([]);
    const [selFinParams, setSelectedFinParams] = useState(DEFAULT_SELECTED_FINANCIAL_COLUMNS);
    const [selKpi, setKpi] = useState([KPI_IDS.VOLUME, KPI_IDS.VALUE, KPI_IDS.MARGIN]);

    const [isSaving, setIsSaving] = useState(false);

    const [isShowNotification, setIsShowNotification] = useState();

    useEffect(() => {
        if (!_.isEmpty(newProject)) {
            const priceIndexRange = newProject.params.priceIndexRange;
            Promise.all([
                getParams(newProject.params),
                getLineUpMap(newProject.params),
                getPositioning(newProject.owner, newProject.name, priceIndexRange ? JSON.stringify([
                    priceIndexRange.Value[0] * priceIndexRange.Percent | 0,
                    priceIndexRange.Value[1] * priceIndexRange.Percent | 0
                ]) : '')
            ]).then(([params, lineupmap, positioning]) => {
                const sortedSpecs = _.sortBy(
                    params.specs,
                    spec => (!!~_.findIndex(newProject.params.specs, s => s === spec.name) ? _.findIndex(newProject.params.specs, s => s === spec.name) : 999)
                );
                const xSpec = sortedSpecs[0];
                const ySpec = sortedSpecs[1];
                setSpecs(_.take(sortedSpecs, newProject.params.specs.length));
                setXSpec(xSpec);
                setYSpec(ySpec);

                if (newProject.params.selectedStateIds?.step2) {
                    setSelectedStateIdsDefault(newProject.params.selectedStateIds.step2)
                    setSelectedStateIds(newProject.params.selectedStateIds.step2)
                } else {
                    const specsUnordered = (lineupmap.models || newProject.models).reduce((acc, m) => {
                        acc.add(`x_${m.specs.find(m => m.id === xSpec.id).value}`)
                        acc.add(`y_${m.specs.find(m => m.id === ySpec.id).value}`)
                        return acc
                    }, new Set())

                    const selectedStateIds = [
                        ...xSpec.value
                            .filter(x => specsUnordered.has(`x_${x}`))
                            .map(v => `x_${v}`),
                        ...ySpec.value
                            .filter(y => specsUnordered.has(`y_${y}`))
                            .map(v => `y_${v}`),
                    ]
                    setSelectedStateIdsDefault(selectedStateIds)
                    setSelectedStateIds(selectedStateIds)
                }

                setSD(lineupmap.segments);
                setLocalModels(newProject.models.map(m => {
                    const compareSpecs = (specs) => specs.reduce((acc, item) => ({
                        ...acc,
                        [item.id]: item.value
                    }), {});
                    const positionedModel = positioning.models.find(posModel => {
                        return _.isEqual(compareSpecs(posModel.specs), compareSpecs(m.specs))
                    });
                    const modelPrice = m.price ||
                        Math.round(newProject.params.priceIndex * (positionedModel?.price || 0) / 100);

                    return {
                        ...m,
                        changedSpecs: m.specs,
                        price: modelPrice,
                        msrp: m.msrp || msrp(modelPrice, newProject.params.msrpVsMgp, modelPrice === m.price)
                    };
                }));

                if (newProject?.summary) {
                    setSelectedSpecs(newProject.summary.SelSpecs || []);
                    setSelectedFinParams(newProject.summary.SelFinParams || []);
                    setKpi(newProject.summary.SelKpi || []);
                } else {
                    setSelectedSpecs(sortedSpecs.map(s => s.id));
                }
            });
        }
    }, [newProject]);


    const [viewMode, setViewMode] = useState('map')

    const [isShowAsk, setIsShowAsk] = useState();

    const shouldShowFinancials = true;// !_.every((localModels || []).map(m => _.every(m.financials, isNaN)));

    if (!(xSpec && ySpec && localModels && newProject)) return <div className='loaderWrapper'><Loader /></div>;
    return (
        <div className='Step5'>
            <div className='mode-select-buttons'>
                <Button
                    label={'LIST VIEW'}
                    onPress={() => setViewMode('list')}
                    disabled
                    margined={5}
                />
                <Button
                    label={'MAP VIEW'}
                    onPress={() => setViewMode('map')}
                    margined={5}
                />
            </div>
            <SummaryBlock
                models={localModels}
                newProject={newProject}
            />
            <div className='header-buttons'>

                <MultipleStateButton
                    name={'X-Y axis'}
                    states={(() => {
                        const states = [
                            ...xSpec.value.map(v => ({id: `x_${v}`, label: v})),
                            ...ySpec.value.map(v => ({id: `y_${v}`, label: v})),
                        ]
                        const i = states.findIndex(s => s.id.startsWith('y_'))
                        states.splice(i, 0, {id: ySpec.name, label: ySpec.name, class: 'disabled'})
                        return [
                            {id: xSpec.name, label: xSpec.name, class: 'disabled'},
                            ...states
                        ]
                    })()}
                    selectedStateIds={selectedStateIds}
                    onChangeSelectedState={ids => setSelectedStateIds([
                        ...specs[0].value
                            .filter(x => ids.includes(`x_${x}`))
                            .map(v => `x_${v}`),
                        ...specs[1].value
                            .filter(y =>ids.includes(`y_${y}`))
                            .map(v => `y_${v}`),
                    ])}
                />

                <MultipleStateButton
                    name={'SPECS'}
                    states={specs.map(s => ({ id: `${s.id}`, label: s.name }))}
                    selectedStateIds={selectedSpecs}
                    onChangeSelectedState={setSelectedSpecs}
                />
                {shouldShowFinancials && <>
                    <MultipleStateButton
                        name={'FINANCIALS'}
                        states={finCols.filter(f => !f.hidden && f.showOnCard && f.id !== 'msrp')}
                        selectedStateIds={selFinParams}
                        onChangeSelectedState={setSelectedFinParams}
                    />
                    <MultipleStateButton
                        name={'KPIS'}
                        states={kpis}
                        selectedStateIds={selKpi}
                        onChangeSelectedState={setKpi}
                    />
                </>}
            </div>
            {viewMode === 'map' ? <div className='table-wrapper'>
                <div className='row' style={{ maxHeight: 50 }}>
                    <div className='cell'></div>
                    {ySpec.value.map(yValue => (<div key={yValue} className='cell'></div>))}
                </div>

                <table>
                    <thead>

                    <HeaderRow
                        columns={ySpec.value
                            .filter(y => selectedStateIds.includes(`y_${y}`))
                            .sort((a, b) => sort(ySpec.name, true, a, b))
                        }
                    />

                    </thead>
                    <tbody>

                    {selectedStateIds
                        .filter(s => s.startsWith('x_'))
                        .sort((a, b) => sort(xSpec.name, false, a.slice(2), b.slice(2)))
                        .map(_xValue => {
                            const xValue = _xValue.slice(2)
                            const segments = segmentDescriptions.filter(sd => sd.x === String(xValue))
                            return (
                                <TableRow
                                    key={xValue}
                                    title={xValue}
                                    subtitle={`${segments.reduce((acc, val) => acc + val.percent, 0)}%`}
                                    ySpec={ySpec}
                                    columnValues={selectedStateIds
                                        .filter(s => s.startsWith('y_'))
                                        .map(s => s.slice(2))
                                        .sort((a, b) => sort(ySpec.name, true, a, b))
                                    }
                                    specs={specs}
                                    segmentDescriptions={segments}
                                    models={localModels
                                        .filter(
                                            model => model.specs.find(s => s.id === xSpec.id).value === `${xValue}`
                                        )
                                        .map(
                                            m => m.sums ? m : {...m, sums: modelSums}
                                        )
                                    }
                                    currency={newProject.params.currency}
                                    brand={newProject.params.brand}
                                    isEditable={false}
                                    showPrice={true}
                                    showFinancials={shouldShowFinancials}
                                    shownSpecs={selectedSpecs}
                                    shownFinancials={selFinParams}
                                    shownKpis={selKpi}
                                />
                            )
                        })
                    }

                </tbody>
            </table>

            </div> : <div></div>}
            <BottomButtons>
                <Button
                    margined={20}
                    label={'COMPLETE'}
                    disabled={disableEdit}
                    isBottomButton={true}
                    showLoader={isSaving}
                    onPress={() => {
                        setIsShowAsk(true);
                        setIsSaving(true)
                    }}
                />
            </BottomButtons>

            {isShowAsk && <Popups.Ask
                header={`Do you want to complete "${newProject.name}"?`}
                body='If you will need to make corrections later, you can do this by entering the project.'
                button='COMPLETE'
                setIsShowAsk={setIsShowAsk}
                callback={() => {
                    const promises = []

                    promises.push(persist({
                        ...newProject,
                        selSpecs: specs.reduce((a, v) => (selectedSpecs || []).includes(v.id) ? a.concat(v.id) : a, []),
                        selFinParams,
                        selKpi
                    }))

                    if (JSON.stringify(selectedStateIds) !== JSON.stringify(selectedStateIdsDefault)) {

                        const _selectedStateIds = {
                            ...newProject.params.selectedStateIds,
                            step2: selectedStateIds
                        }

                        promises.push(PROJECT_ACTIONS.saveProjectParams(
                            newProject.name,
                            newProject.owner,
                            {...newProject.params,
                                selectedStateIds: _selectedStateIds
                            }
                        ))

                        setSelectedStateIdsDefault(selectedStateIds)

                        setNewProject({
                            ...newProject,
                            params: {
                                ...newProject.params,
                                selectedStateIds: _selectedStateIds,
                            }
                        });

                    }
                    Promise.all(promises).then(() => {
                        setIsSaving(false)
                        setIsShowNotification(true);
                    })
                }}
                cancelCallback={() => setIsSaving(false)}
            />}

            {isShowNotification && <Popups.Notification
                text={'Completed'}
                setIsShowNotification={setIsShowNotification}
            />}

        </div>
    );
}

function SummaryBlock({ models, newProject }) {
    const modelsFinancials = models.map(model => {
        const f = getFinancials({ msrp: model.price, ...model.financials });
        return {
            msrp: f.msrp * sumArray((model.sums || modelSums).volume),
            tow_v: f.tow_v * sumArray((model.sums || modelSums).volume)
        }
    });
    const sums = {
        vol: sumArray(models.map(m => sumArray((m.sums || modelSums).volume))),
        val: sumArray(models.map(m => sumArray((m.sums || modelSums).value))),
        mar: sumArray(models.map(m => sumArray((m.sums || modelSums).margin)))
    }
    const fins = {
        gasv: sums.val / sums.vol,
        tow: sumArray(modelsFinancials.map(mf => mf.msrp)) / sumArray(modelsFinancials.map(mf => mf.msrp)),
        margin: sums.mar / sums.vol
    }
    return (<div className='summary-block content-block'>
        <table>
            <thead>
                <tr>
                    <td colSpan='3'>Parameters</td>
                    <td colSpan='3'>Financials</td>
                    <td colSpan='3'>KPIs</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Country</td><td>
                        <div style={{ display: 'flex' }}>
                            {newProject.params.countries.map(cc => {
                                const country = _.find(countries, c => c.name === cc)
                                return (
                                    <React.Fragment key={country.className}>
                                        <div className={`flag ${country.className}`} /> <div style={{ textTransform: 'capitalize' }}>{country.name}</div>
                                    </React.Fragment>
                                )
                            })}
                        </div>
                    </td><td></td>
                    <td>GASV</td><td>{isNaN(fins.gasv) ? '-' : getCurrencySymbol(newProject.params.currency) + formatPriceNumber(fins.gasv)}</td><td></td>
                    <td>FY volume</td><td>{sums.vol ? formatNumber(sums.vol) : '-'}</td><td></td>
                </tr>
                <tr>
                    <td>Sub category</td><td>{newProject.params.segment}</td><td></td>
                    <td>TOW/ave</td><td>{isNaN(fins.tow) ? '-' : (formatNumber(fins.tow) + '%')}</td><td></td>
                    <td>FY value</td><td>{sums.val ? getCurrencySymbol(newProject.params.currency) + formatPriceNumber(sums.val) : '-'}</td><td></td>
                </tr>
                <tr>
                    <td>MGP vs avg</td><td>{newProject.params.priceIndex}</td><td></td>
                    <td>Margin/ave</td><td>{isNaN(fins.margin) ? '-' : getCurrencySymbol(newProject.params.currency) + formatPriceNumber(fins.margin)}</td><td></td>
                    <td>FY margin</td><td>{sums.mar ? getCurrencySymbol(newProject.params.currency) + formatPriceNumber(sums.mar) : '-'}</td><td></td>
                </tr>
            </tbody>
        </table>
    </div>);
}

async function persist({ name, owner, selSpecs, selFinParams, selKpi }) {
    return fetch(`${prod.stage}projects/${name}/summary?owner=${owner}`, {
        method: 'PUT',
        headers: await getHeaders(),
        body: JSON.stringify({selSpecs, selFinParams, selKpi})
    });
}

export default Step5;
