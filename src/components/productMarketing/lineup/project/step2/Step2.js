import React, { useEffect, useState } from 'react';

import _ from 'lodash';

import './Step2.css';

import Loader from '../../../../EmptyScreen/Loader';
import { getLineUpMap } from '../../../../../actions/lineupmap';
import { getParams } from '../../../../../actions/parameters';
import HeaderRow from '../HeaderRow';
import TableRow from '../TableRow';
import sort from '../../../../../utils/sort';
import { BottomButtons, Button, MultipleStateButton } from '../../../../common';
import { saveModels } from '../../../../../actions/models';
import { saveCompetitors } from '../../../../../actions/competitors';
import * as PROJECT_ACTIONS from '../../../../../actions/project';

function Step2({
    newProject,
    setNewProject,
    onNextStep,
    disableEdit
}) {
    const [xSpec, setXSpec] = useState(null);
    const [ySpec, setYSpec] = useState(null);
    const [specs, setSpecs] = useState(null);

    const [selectedStateIdsDefault, setSelectedStateIdsDefault] = useState();
    const [selectedStateIds, setSelectedStateIds] = useState();

    const [segmentDescriptions, setSD] = useState(null);
    const [localModels, setLocalModels] = useState(null);
    const [isSaving, setIsSaving] = useState({ apply: false, next: false });

    useEffect(() => {
        if (Object.keys(newProject).length && isSaving.apply === false && isSaving.next === false) {
            Promise.all([
                getLineUpMap(newProject.params),
                getParams(newProject.params)
            ]).then(([lineupmap, params]) => {
                if (!params) return

                const [xSpec, ySpec] = newProject.params.specs.map(s => params.specs.find(_s => _s.id === s))
                setSpecs([xSpec, ySpec, ...params.specs.filter(s => s.id !== xSpec.id && s.id !== ySpec.id)]);
                setXSpec(xSpec);
                setYSpec(ySpec);

                if (newProject.params.selectedStateIds?.step2) {
                    setSelectedStateIdsDefault(newProject.params.selectedStateIds.step2)
                    setSelectedStateIds(newProject.params.selectedStateIds.step2)
                } else {
                    const specsUnordered = (lineupmap.models || newProject.models).reduce((acc, m) => {
                        acc.add(`x_${m.specs.find(m => m.id === xSpec.id).value}`)
                        acc.add(`y_${m.specs.find(m => m.id === ySpec.id).value}`)
                        return acc
                    }, new Set())

                    const selectedStateIds = [
                        ...xSpec.value
                            .filter(x => specsUnordered.has(`x_${x}`))
                            .map(v => `x_${v}`),
                        ...ySpec.value
                            .filter(y => specsUnordered.has(`y_${y}`))
                            .map(v => `y_${v}`),
                    ]
                    setSelectedStateIdsDefault(selectedStateIds)
                    setSelectedStateIds(selectedStateIds)
                }

                setSD(lineupmap.segments);

                if (newProject.models) {
                    setLocalModels(newProject.models.map(m => ({ ...m, changedSpecs: m.specs })))
                } else {
                    setLocalModels(lineupmap.models.map(m => ({
                        ...m,
                        changedSpecs: m.specs
                    })))
                }

            });
        }
    }, [newProject, isSaving]);

    useEffect(() => {
        newProject.models && setLocalModels(newProject.models.map(m => ({
            ...m,
            changedSpecs: m.changedSpecs || m.specs
        })))
    }, [newProject.models])

    const changeModel = (nm) => {
        const ind = _.findIndex(localModels, m => nm.id === m.id);
        const newModels = [
            ...localModels.slice(0, !!~ind ? ind : localModels.length),
            nm,
            ...localModels.slice((!!~ind ? ind : localModels.length) + 1, localModels.length),
        ];
        setLocalModels(newModels)
    }

    const onApply = async () => {
        const _selectedStateIds = {
            ...newProject.params.selectedStateIds,
            step2: selectedStateIds
        }

        return await Promise.all([
            saveModels(newProject, localModels.filter(m => !m.toDelete))
                .then(modelsOrdered => {
                    const newModels = modelsOrdered.map((m, i) => {
                        const _m = {
                            id: `Model #${i + 1}`,
                            specs: Object.entries(m).map(([id, value], acc, i) => ({
                                id,
                                name: id,
                                value
                            }))
                        };
                        _m.changedSpecs = _m.specs;
                        return _m;
                    });

                    setNewProject({
                        ...newProject,
                        params: {
                            ...newProject.params,
                            selectedStateIds: _selectedStateIds,
                        },
                        models: newModels
                    });
                }),

            saveCompetitors(newProject['name'], newProject['owner'], null),

            ((selectedStateIds.length !== selectedStateIdsDefault.length) ||
                (selectedStateIdsDefault.some(s => !selectedStateIds.includes(s)))) ?
                PROJECT_ACTIONS.saveProjectParams(
                    newProject.name,
                    newProject.owner,
                    {...newProject.params,
                        selectedStateIds: _selectedStateIds
                    }
                ) && setSelectedStateIdsDefault(selectedStateIds) : Promise.resolve(),
        ]);
    }

    const onCancel = () => {
        const newModels = localModels.filter(m => !m.isNew).map(m => {
            return {
                ..._.omit(m, 'toDelete'),
                changedSpecs: m.specs,
                isNew: false
            }
        })

        setSelectedStateIds(selectedStateIdsDefault)

        setNewProject({ ...newProject, models: newModels })
    }

    const createNewModel = () => {

        const v = selectedStateIds
                    .filter(s => s.startsWith('x_'))
                    .map(s => s.slice(2))
                    .sort((a, b) => isNaN(a) ? 0 : b - a)[0]

        // TODO drop find(): order by specs on mapping
        const m = localModels.find(m => m.specs.find(s => s.id === specs[0].id && s.value === v))

        const newModels = [
            ...localModels,
            {
                ...m,
                isNew: true,
                changedSpecs: null,
                price: null,
                changedPrice: null
            }
        ];
        setNewProject({ ...newProject, models: newModels })
    }

    return xSpec && ySpec && segmentDescriptions && localModels ? (
        <div className='Step2'>
            <table>
                <thead>
                <tr>

                    <td><MultipleStateButton
                        name={'X-Y axis'}
                        states={(() => {
                            const states = [
                                ...xSpec.value.map(v => ({id: `x_${v}`, label: v})),
                                ...ySpec.value.map(v => ({id: `y_${v}`, label: v})),
                            ]
                            const i = states.findIndex(s => s.id.startsWith('y_'))
                            states.splice(i, 0, {id: ySpec.name, label: ySpec.name, class: 'disabled'})
                            return [
                                {id: xSpec.name, label: xSpec.name, class: 'disabled'},
                                ...states
                            ]
                        })()}
                        selectedStateIds={selectedStateIds}
                        onChangeSelectedState={ids => setSelectedStateIds([
                            ...xSpec.value
                                .filter(x => ids.includes(`x_${x}`))
                                .map(v => `x_${v}`),
                            ...ySpec.value
                                .filter(y => ids.includes(`y_${y}`))
                                .map(v => `y_${v}`),
                        ])}
                    /></td>

                    {selectedStateIds
                        .filter(y => y.startsWith('y_'))
                        .map(y => <td key={y} className='cell'></td>)
                    }

                    <td>
                        <div style={{ display: 'flex', justifyContent: 'center' }}>
                            {!disableEdit && <Button onPress={createNewModel} label={'ADD NEW'} />}
                        </div>
                    </td>

                </tr>

                <HeaderRow
                    columns={ySpec.value
                        .filter(y => selectedStateIds.includes(`y_${y}`))
                        .sort((a, b) => sort(ySpec.name, true, a, b))
                    }
                    newCodes={!disableEdit}
                />

                </thead>
                <tbody>

                    {selectedStateIds
                        .filter(s => s.startsWith('x_'))
                        .sort((a, b) => sort(xSpec.name, false, a.slice(2), b.slice(2)))
                        .map(_xValue => {
                            const xValue = _xValue.slice(2)
                            const segments = segmentDescriptions.filter(sd => sd.x === String(xValue))
                            return (
                                <TableRow
                                    key={xValue}
                                    title={xValue}
                                    subtitle={`${segments.reduce((acc, val) => acc + val.percent, 0)}%`}
                                    ySpec={ySpec}
                                    columnValues={selectedStateIds
                                        .filter(s => s.startsWith('y_'))
                                        .map(s => s.slice(2))
                                        .sort((a, b) => sort(ySpec.name, true, a, b))
                                    }
                                    specs={specs}
                                    segmentDescriptions={segments}
                                    models={localModels.filter(m => {
                                        return String(m.specs.find(s => s.id === xSpec.id).value) === String(xValue) &&
                                            selectedStateIds.includes(`y_${m.specs.find(s => s.id === ySpec.id).value}`)
                                    })}
                                    brand={newProject.params.brand}
                                    onChangeModel={changeModel}
                                    showPrice={false}
                                    isEditable={!disableEdit}
                                />
                            )
                        })
                    }

                </tbody>
            </table>
            <BottomButtons>
                <Button
                    label={'CANCEL'}
                    margined={20}
                    disabled={!isEdited(localModels, selectedStateIds, selectedStateIdsDefault)}
                    onPress={onCancel}
                    isBottomButton={true}
                />
                <Button label={'APPLY'}
                    margined={20}
                    disabled={!isEdited(localModels, selectedStateIds, selectedStateIdsDefault)}
                    showLoader={isSaving.apply}
                    onPress={() => {
                        setIsSaving({...isSaving, apply: true})
                        onApply().then(() => {
                            setIsSaving({...isSaving, apply: false})
                        })
                    }}
                    isBottomButton={true}
                />
                <Button
                    label={'NEXT'}
                    margined={20}
                    disabled={isEdited(localModels, selectedStateIds, selectedStateIdsDefault) || disableEdit}
                    showLoader={isSaving.next}
                    onPress={() => {
                        setIsSaving({...isSaving, next: true})
                        Promise.resolve(!disableEdit ? onApply() : true).then(() => {
                            onNextStep()
                        })
                    }}
                    isBottomButton={true}
                />
            </BottomButtons>
        </div>
    ) : <div className='loaderWrapper'><Loader /></div>
}

function isEdited(models, selectedStateIds, selectedStateIdsDefault) {
    return models.some(m => m.isNew || m.toDelete || !_.isEqual(m.specs, m.changedSpecs)) ||
        (selectedStateIds.length !== selectedStateIdsDefault.length ||
            selectedStateIdsDefault.some(s => !selectedStateIds.includes(s)))
}

export const isStepGenerated = (models) => {
    return _.some(models, model => model.generatedModel)
}

export default Step2;
