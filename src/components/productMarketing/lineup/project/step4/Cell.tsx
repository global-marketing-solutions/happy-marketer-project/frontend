import React, { useState, useEffect } from 'react';
import { getCurrencySymbol, formatNumber, formatPriceNumber } from '../../../../utils/formatPrice';
import './Step4.css';

// export const INPUT_TYPES = {
//     PLAIN: 'plain',
//     PERCENT: 'percent',
//     PRICE: 'price'
// }

export enum CELL_TYPE {
    PLAIN = "plain",
    PERCENT = "percent",
    PRICE = "price"
}

export type CellProps = {
    value?: string | number,
    type?: CELL_TYPE,
    currency?: string,
    isEditable?: boolean,
    onChange?: (value: number | string) => void,
    placeholder?: number | string,
    woDelimeter?: boolean,
    isBold?: boolean
}

export function Cell({
    value = '#',
    type = CELL_TYPE.PRICE,
    currency = '',
    isEditable = false,
    onChange,
    woDelimeter = false,
    isBold = false
}: CellProps) {
    const [showInput, setShowInput] = useState(false);
    const additionalClass = type === CELL_TYPE.PRICE && !isNaN(+value) ? 'PriceCell' : 'MiddleCell';
    const [innerValue, setInnerValue] = useState(`${value}`);

    useEffect(() => {
        setInnerValue(`${isNaN(+value) ? (type === CELL_TYPE.PLAIN ? value : '') : value ? value : 0}`);
    }, [value, type]);

    return !showInput ? (
        <div
            className={`CustomCell ${additionalClass} ${onChange && isEditable ? 'EditableCell' : ''} ${isBold ? 'BoldCell' : ''}`}
            onClick={() => setShowInput(!!onChange && isEditable && true)}
            data-testid="value-presenter"
        >
            {!innerValue ? 'n/a' : null}
            {innerValue && type === CELL_TYPE.PRICE ? (<>
                <span>{getCurrencySymbol(currency)}</span>
                {formatPriceNumber(+value, woDelimeter ? 0 : 2)}
            </>) : null}
            {innerValue && type === CELL_TYPE.PLAIN ? (<>{isNaN(+value) ? value : formatPriceNumber(+value)}</>) : null}
            {innerValue && type === CELL_TYPE.PERCENT ? (<>{formatNumber(+value)}%</>) : null}
        </div>
    ) : <input
            value={innerValue || ''}
            type={'number'}
            className={'CellInput'}
            autoFocus={true}
            onChange={e => setInnerValue(e.target.value)}
            placeholder={`${type === CELL_TYPE.PERCENT ? '%' : type === CELL_TYPE.PRICE ? '0.00' : 0}`}
            onBlur={() => { setShowInput(false); onChange && onChange(innerValue);}}
            data-testid="value-input"
        />;


}
