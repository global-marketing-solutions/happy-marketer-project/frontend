import React, { Fragment } from 'react';
import _ from 'lodash';
import { formatNumber } from '../../../../utils/formatPrice';
import './Step4.css';
import { PERIOD_IDS, sumArray } from './ModelLine';
import { Cell, CELL_TYPE } from './Cell'

export function SumLine({
    models,
    specs,
    selectedSpecs,
    selectedFinParametes,
    finCols,
    period,
    currency,
    kpi,
    kpis
}) {
    const aggrSums = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0].reduce((acc, i, ind) => {
        return {
            volume: [...acc.volume, sumArray(models.map(m => +((m.sums || {}).volume || [])[ind]))],
            value: [...acc.value, sumArray(models.map(m => +((m.sums || {}).value || [])[ind]))],
            margin: [...acc.margin, sumArray(models.map(m => +((m.sums || {}).margin || [])[ind]))]
        }
    }, { volume: [], value: [], margin: [] })

    return (
        <>
        <tr style={{height: '30px'}}></tr>
        <tr key={`sum_line`} className='Row ModelLine SumLine'>
            <td className='Cell min100'></td>
            <td className='Cell Divider'></td>
            {specs.slice(1).filter(s => selectedSpecs.includes(s.id)).map((d, i) => (
                <td className='Cell' key={`_${i}`}></td>
            ))}
            <td className='Cell Divider'></td>
            {finCols.filter(fc => selectedFinParametes.includes(fc.id)).map((fc, i) => (
                <td className='Cell min70' key={`_placeholder_${i}`}></td>
            ))}
            <td className='Cell Divider' style={{ maxWidth: '4px' }}>
                <div style={{ marginLeft: '-45px', textAlign: 'right' }}>TOTAL:</div>
            </td>
            {_.map(kpi, (kpi_id, ind) => {
                const cell_type = kpis.find(k => k.id === kpi_id)?.type === 'price' ? CELL_TYPE.PRICE : CELL_TYPE.PLAIN;
                return (
                    <Fragment key={`fr_sums_${kpi_id}`}>
                        {period === PERIOD_IDS.MONTH && !ind && (
                            _.map(Array(12), (_, ind) => {
                                const value = formatNumber(aggrSums[kpi_id])
                                return (<td className='Cell min70 Border' key={`_${kpi_id}_m_${ind}`}>
                                    <Cell
                                        value={value}
                                        type={cell_type}
                                        currency={currency}
                                    />
                                </td>)
                            })
                        )}
                        {period === PERIOD_IDS.QUARTER && !ind && (
                            _.map(Array(4), (_, ind) => {
                                const value = sumArray(aggrSums[kpi_id].slice(ind * 3, ind * 3 + 3));
                                return (<td className='Cell min70 Border' key={`_${kpi_id}q_${ind}`}>
                                    <Cell
                                        value={value}
                                        type={cell_type}
                                        currency={currency}
                                    />
                                </td>)
                            })
                        )}
                        <td className='Cell min70 Border' key={`_placeholder_fy_smth`}>
                            <Cell
                                value={sumArray(aggrSums[kpi_id])}
                                type={cell_type}
                                currency={currency}
                            />
                        </td>
                    </Fragment>
                )
            })}
        </tr>
        </>
    )
}
