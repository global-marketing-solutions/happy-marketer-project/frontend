import React, { useState, useEffect, useRef, Fragment, useCallback } from 'react';
import _ from 'lodash';
import { Cell, CELL_TYPE } from './Cell';
import './Step4.css';

export const FINANCIAL_IDS = {
    MSRP: 'msrp',
    MGP: 'mgp',
    DISCOUNTS_P: 'discounts_p',
    DISCOUNTS_V: 'discounts_v',
    AFTER_DISCOUNTS_V: 'after_discounts_p',
    TM_DISCOUNTS_P: 'tm_discounts_p',
    TM_DISCOUNTS_V: 'tm_discounts_v',
    GASV: 'gasv',
    TPA_P: 'tpa_p',
    TPA_V: 'tpa_v',
    NASV: 'nasv',
    TOW_P: 'tow_p',
    TOW_V: 'tow_v',
    PRODUCT_COST: 'product_cost',
    F_W: 'f_w',
    CUSTOM_P: 'custom_p',
    CUSTOM_V: 'custom_v',
    MARGIN_P: 'margin_p',
    MARGIN_V: 'margin_v',
};

export const KPI_IDS = {
    VOLUME: 'volume',
    VALUE: 'value',
    MARGIN: 'margin',
};

export const PERIOD_IDS = {
    YEAR: 'by_year',
    QUARTER: 'by_quarters',
    MONTH: 'by_months',
}

export const AGGREGATION = {
    SUM: 'SUM',
    AVG: 'AVG'
}

export const sumArray = (arr) => arr.reduce((a, i) => a + (i || 0), 0);

export const avgArray = (arr) => arr.reduce((a, i) => a + i / arr.length, 0)

const getSums = (sums = {}, kpi, values) => {
    if (kpi === KPI_IDS.VOLUME) {
        return (sums.volume || []).reduce((acc, vol) => {
            return {
                value: [...acc.value, Math.round(values[FINANCIAL_IDS.GASV] * vol * 100) / 100],
                volume: [...acc.volume, vol],
                margin: [...acc.margin, Math.round(values[FINANCIAL_IDS.MARGIN_V] * vol * 100) / 100]
            }
        }, { value: [], volume: [], margin: [] })
    }
    if (kpi === KPI_IDS.VALUE) {
        return (sums.value || []).reduce((acc, val) => {
            return {
                value: [...acc.value, val],
                volume: [...acc.volume, Math.ceil(val / values[FINANCIAL_IDS.GASV])],
                margin: [...acc.margin, Math.ceil(val / values[FINANCIAL_IDS.GASV]) * values[FINANCIAL_IDS.MARGIN_V]]
            }
        }, { value: [], volume: [], margin: [] })
    }
    if (kpi === KPI_IDS.MARGIN) {
        return (sums.margin || []).reduce((acc, mar) => {
            return {
                value: [...acc.value, Math.ceil(mar / values[FINANCIAL_IDS.MARGIN_V]) * values[FINANCIAL_IDS.GASV]],
                volume: [...acc.volume, Math.ceil(mar / values[FINANCIAL_IDS.MARGIN_V])],
                margin: [...acc.margin, mar]
            }
        }, { value: [], volume: [], margin: [] })
    }
}

export const getFinancials = ({mgp, discounts_p, tm_discounts_p, tpa_p, custom_p, product_cost, f_w}) => {
        const discountValue = Math.round(mgp * +discounts_p / 100);
        const afterDiscounts = mgp - discountValue;
        const tmDiscountValue = Math.round(afterDiscounts * +tm_discounts_p / 100);
        const gasv = mgp - discountValue - tmDiscountValue;
        const tpa_v = Math.round(gasv * +tpa_p / 100);
        const nasv = gasv - tpa_v;
        const tow = mgp - nasv;
        const customValue = Math.round(+custom_p * +product_cost / 100);
        const margin = nasv - +product_cost - +f_w - customValue;
        return {
            [FINANCIAL_IDS.MGP]: mgp,
            [FINANCIAL_IDS.DISCOUNTS_P]: discounts_p,
            [FINANCIAL_IDS.DISCOUNTS_V]: discountValue,
            [FINANCIAL_IDS.AFTER_DISCOUNTS_V]: afterDiscounts,
            [FINANCIAL_IDS.TM_DISCOUNTS_P]: tm_discounts_p,
            [FINANCIAL_IDS.TM_DISCOUNTS_V]: tmDiscountValue,
            [FINANCIAL_IDS.GASV]: gasv,
            [FINANCIAL_IDS.TPA_P]: tpa_p,
            [FINANCIAL_IDS.TPA_V]: tpa_v,
            [FINANCIAL_IDS.NASV]: nasv,
            [FINANCIAL_IDS.TOW_V]: tow,
            [FINANCIAL_IDS.TOW_P]: Math.round(tow / mgp * 100),
            [FINANCIAL_IDS.PRODUCT_COST]: product_cost,
            [FINANCIAL_IDS.F_W]: f_w,
            [FINANCIAL_IDS.MARGIN_V]: margin,
            [FINANCIAL_IDS.MARGIN_P]: Math.round(margin / nasv * 100),
            [FINANCIAL_IDS.CUSTOM_P]: custom_p,
            [FINANCIAL_IDS.CUSTOM_V]: customValue
        }
}

export function ModelLine({
    model,
    specs,
    selectedSpecs,
    selectedFinParametes,
    finCols,
    period,
    kpi,
    kpis,
    currency,
    onModelChange = () => { }
}) {
    const [values, setValues] = useState({
        ..._.values(FINANCIAL_IDS).reduce((acc, key) => ({ ...acc, [key]: _.get(model.financials, key, NaN) }), {}),
        mgp: model.price,
    });

    const [sums, setSums] = useState(model.sums || {
        margin: Array(12).fill(NaN),
        value: Array(12).fill(NaN),
        volume: Array(12).fill(NaN)
    });

    const updateModel = useCallback((newValues, newSums) => {
        newValues && setValues(newValues);
        newSums && setSums(newSums);
        onModelChange({
            ...model,
            ...(newValues ? { financials: newValues } : {}),
            ...(newSums ? { sums: newSums } : {})
        })
    }, [model, onModelChange]);

    const prevValues = useRef(values);
    const prevSums = useRef(sums);

    useEffect(() => {
        const newVals = {
            ...values,
            [FINANCIAL_IDS.MGP]: model.price,
            [FINANCIAL_IDS.DISCOUNTS_P]: model.financials?.discounts_p,
            [FINANCIAL_IDS.TM_DISCOUNTS_P]: model.financials?.tm_discounts_p,
            [FINANCIAL_IDS.TPA_P]: model.financials?.tpa_p,
            [FINANCIAL_IDS.PRODUCT_COST]: model.financials?.product_cost,
            [FINANCIAL_IDS.F_W]: model.financials?.f_w,
            [FINANCIAL_IDS.CUSTOM_P]: model.financials?.custom_p,
        }
        if (!_.isEqual(values, newVals)) {
            setValues(newVals)
        }
    }, [model, values]);

    useEffect(() => {
        const newValues = getFinancials({
            mgp: model.price,
            discounts_p: values.discounts_p,
            tm_discounts_p: values.tm_discounts_p,
            tpa_p: values.tpa_p,
            custom_p: values.custom_p,
            product_cost: values.product_cost,
            f_w: values.f_w
        });
        if (!_.isEqual(prevValues.current, newValues)) {
            updateModel(newValues)
        }
        const newSums = getSums(sums, kpi[0], newValues);
        if (!!prevValues.current.gasv && !!prevValues.current.margin_v &&
            ((prevValues.current.gasv !== newValues.gasv && prevValues.current.margin_v !== newValues.margin_v)
                || (!_.isEqual(prevSums.current, newSums)))
        ) {
            updateModel(newValues, newSums);
        }
        prevValues.current = newValues;
        prevSums.current = newSums;
    }, [
        values.discounts_p,
        values.tpa_p,
        values.tm_discounts_p,
        values.product_cost,
        values.f_w,
        values.custom_p,
        values.gasv,
        values.margin_v,
        sums, kpi,
        updateModel, model
    ]);

    const setMonth = (kpi_id, ind, value) => {
        const newSums = [
            ...sums[kpi_id].slice(0, ind),
            +value || 0,
            ...sums[kpi_id].slice(ind + 1, sums[kpi_id].length + 1)
        ];
        updateModel(null, { ...sums, [kpi_id]: newSums });
    };

    const setQuarter = (kpi_id, ind, value) => {
        const newSums = [
            ...sums[kpi_id].slice(0, ind * 3),
            value, 0, 0,
            ...sums[kpi_id].slice(ind * 3 + 3, sums[kpi_id].length + 1)
        ];
        updateModel(null, getSums({ ...sums, [kpi_id]: newSums }, kpi_id, values));
    }

    const setYear = (kpi_id, value) => {
        updateModel(null, { ...sums, [kpi_id]: [value, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] })
    }

    return (
        <tr key={`ml_${model.id}`} className='Row ModelLine'>
            <td className='Cell min100 Border'><div className={'CustomCell'}>{model.id}</div></td>
            <td className='Cell Divider'></td>
            {specs
                .slice(1)
                .filter(s => selectedSpecs.includes(s.id))
                .map((s, i) => model.specs.find(ms => ms.id === s.id))
                .map(s => (
                    <td className='Cell min70 Border' key={`${model.id}_${s.id}_${s.value}`}>
                        <Cell
                            value={s.value}
                            type={CELL_TYPE.PLAIN}
                        />
                    </td>
                ))
            }
            <td className='Cell Divider'></td>
            {selectedFinParametes.includes(finCols[0].id) && (
                <td className='Cell min70 Border'>
                    <Cell
                        value={model.msrp}
                        type={CELL_TYPE.MSRP}
                        currency={currency}
                        woDelimeter={true}
                    />
                </td>
            )}
            {selectedFinParametes.includes(finCols[1].id) && (
                <td className='Cell min70 Border'>
                    <Cell
                        value={model.price}
                        type={CELL_TYPE.PRICE}
                        currency={currency}
                        woDelimeter={true}
                    />
                </td>
            )}

            {finCols.slice(2).filter(fc => selectedFinParametes.includes(fc.id)).map((fc, i) => (
                <td className='Cell min70 Border' key={`${model.id}placeholder_${i}`}>
                    <Cell
                        value={values[fc.id]}
                        type={fc.type === 'percent' ? CELL_TYPE.PERCENT : CELL_TYPE.PRICE}
                        currency={currency}
                        onChange={val => setValues({ ...values, [fc.id]: +val })}
                        isEditable={fc.editing}
                    />
                </td>
            ))}
            <td className='Cell Divider'></td>
            {_.map(kpi, kpi_id => {
                const type = (kpis.find(k => k.id === kpi_id) || {}).type;
                const cell_type = type === 'price' ? CELL_TYPE.PRICE : CELL_TYPE.PLAIN;
                return (
                    <Fragment key={`fr_${kpi_id}`}>
                        {period === PERIOD_IDS.MONTH && (
                            _.map(Array(12), (_, ind) => {
                                const value = (sums || {})[kpi_id];
                                return (<td className='Cell min70 Border' key={`${model.id}${kpi_id}_m_${ind}`}>
                                    <Cell
                                        key={`i_${model.id}${kpi_id}_m_${ind}`}
                                        value={value}
                                        type={cell_type}
                                        currency={currency}
                                        onChange={val => setMonth(kpi_id, ind, +val || 0)}
                                        isEditable={kpi_id === kpi[0]}
                                    />
                                </td>)
                            })
                        )}
                        {period === PERIOD_IDS.QUARTER && (
                            _.map(Array(4), (_, ind) => {
                                const value = sumArray(((sums || {})[kpi_id] || []).slice(ind * 3, ind * 3 + 3));
                                return kpi_id === kpi[0] ? (
                                    <td className='Cell min70 Border' key={`${model.id}${kpi_id}${ind}`}>
                                        <Cell
                                            key={`i_${model.id}${kpi_id}_q_${ind}`}
                                            value={value}
                                            type={cell_type}
                                            currency={currency}
                                            onChange={val => setQuarter(kpi_id, ind, +val || 0)}
                                            isEditable={kpi_id === kpi[0]}
                                        />
                                    </td>
                                ) : null
                            })
                        )}
                        <td className='Cell min70 Border' key={`${model.id}placeholder_fy_smth`}>{
                            <Cell
                                key={`i_${model.id}${kpi_id}_y`}
                                value={sumArray((sums || {})[kpi_id] || [])}
                                type={cell_type}
                                currency={currency}
                                onChange={val => setYear(kpi_id, +val || 0)}
                                isEditable={kpi_id === kpi[0] && period === PERIOD_IDS.YEAR}
                                isBold={true}
                            />
                        }</td>
                    </Fragment>
                )
            })}
        </tr>
    )
}
