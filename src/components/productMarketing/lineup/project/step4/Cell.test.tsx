import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { fireEvent } from '@testing-library/react'
import { act } from "react-dom/test-utils";

import { Cell, CELL_TYPE } from './Cell';

let container: Element | null = null;
beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    container && unmountComponentAtNode(container);
    container?.remove();
    container = null;
});

describe("Step4 Cell Component", () => {

    const getComponent = () => document.querySelector("[data-testid=value-presenter]");
    const getInput = () => document.querySelector("[data-testid=value-input]");

    it('should render n/a if no parameters passed', () => {
        act(() => {
            render(<Cell />, container);
        });
        expect(container?.textContent).toBe("n/a");
    });

    // it('should render n/a if value is NaN', () => {
        
    // });

    // it('should render 0 if value is 0', () => {

    // });

    it('should render price', () => {
        act(() => {
            render(<Cell
                value={1}
                type={CELL_TYPE.PRICE}
                currency={'eur'}
            />, container);
        });
        expect(container?.textContent).toBe("€1");

        act(() => {
            render(<Cell
                value={2}
                type={CELL_TYPE.PRICE}
                currency={'eur'}
            />, container);
        });
        expect(container?.textContent).toBe("€2");

        act(() => {
            render(<Cell
                value={2}
                type={CELL_TYPE.PRICE}
                currency={'usd'}
            />, container);
        });
        expect(container?.textContent).toBe("$2");

        act(() => {
            render(<Cell
                value={2.3}
                type={CELL_TYPE.PRICE}
                currency={'usd'}
            />, container);
        });
        expect(container?.textContent).toBe("$2.3");

        act(() => {
            render(<Cell
                value={2.354}
                type={CELL_TYPE.PRICE}
                currency={'usd'}
            />, container);
        });
        expect(container?.textContent).toBe("$2.35");

        act(() => {
            render(<Cell
                value={2.54}
                type={CELL_TYPE.PRICE}
                currency={'usd'}
                woDelimeter={true}
            />, container);
        });
        expect(container?.textContent).toBe("$3");

        act(() => {
            render(<Cell
                value={2123123.54}
                type={CELL_TYPE.PRICE}
                currency={'usd'}
                woDelimeter={true}
            />, container);
        });
        expect(container?.textContent).toBe("$2,123,124");

        act(() => {
            render(<Cell
                value={-123123.54}
                type={CELL_TYPE.PRICE}
                currency={'usd'}
                woDelimeter={true}
            />, container);
        });
        expect(container?.textContent).toBe("$-123,124");
    });

    it('should render plain and percent value', () => {
        act(() => {
            render(<Cell
                value={'hello'}
                type={CELL_TYPE.PLAIN}
            />, container);
        });
        expect(container?.textContent).toBe("hello");

        act(() => {
            render(<Cell
                value={3}
                type={CELL_TYPE.PERCENT}
            />, container);
        });
        expect(container?.textContent).toBe("3%");

        act(() => {
            render(<Cell
                value={3.456}
                type={CELL_TYPE.PERCENT}
            />, container);
        });
        expect(container?.textContent).toBe("3.46%");
    });

    it('should handle click when editable and onChange', () => {
        act(() => {
            render(<Cell
                value={3.456}
                type={CELL_TYPE.PERCENT}
                isEditable={true}
                onChange={() => { }}
            />, container);
        });

        expect(getComponent()).toBeVisible();
        act(() => {
            getComponent()?.dispatchEvent(new MouseEvent("click", { bubbles: true }));
        });
        expect(getComponent()).not.toBeTruthy();
        expect(getInput()).toBeVisible();
    });

    it('should not handle click when editable is false', () => {
        act(() => {
            render(<Cell
                value={3.456}
                type={CELL_TYPE.PERCENT}
                isEditable={false}
                onChange={() => { }}
            />, container);
        });

        expect(getComponent()).toBeVisible();
        act(() => {
            getComponent()?.dispatchEvent(new MouseEvent("click", { bubbles: true }));
        });
        expect(getInput()).not.toBeTruthy();
    });

    it('should not handle click when onChange is not set', () => {
        act(() => {
            render(<Cell
                value={3.456}
                type={CELL_TYPE.PERCENT}
                isEditable={true}
            />, container);
        });

        expect(getComponent()).toBeVisible();
        act(() => {
            getComponent()?.dispatchEvent(new MouseEvent("click", { bubbles: true }));
        });
        expect(getComponent()).toBeVisible();
        expect(getInput()).not.toBeTruthy();
    });

    it('should call onChange when value changes', () => {
        const onChange = jest.fn();
        act(() => {
            render(<Cell
                value={3.456}
                type={CELL_TYPE.PERCENT}
                isEditable={true}
                onChange={onChange}
            />, container);
        });

        act(() => {
            getComponent()?.dispatchEvent(new MouseEvent("click", { bubbles: true }));
        });
        const input = getInput();
        expect(input).toBeTruthy();
        input && fireEvent.change(input, { target: { value: '23' } })
        expect(onChange).not.toHaveBeenCalled();
        input && fireEvent.blur(input);
        expect(onChange).toHaveBeenCalledWith('23');
    });

    it('should hide input on blur', () => {

        act(() => {
            render(<Cell
                value={3.456}
                type={CELL_TYPE.PERCENT}
                isEditable={true}
                onChange={() => { }}
            />, container);
        });

        act(() => {
            getComponent()?.dispatchEvent(new MouseEvent("click", { bubbles: true }));
        });
        expect(getComponent()).not.toBeTruthy()
        expect(getInput()).toBeTruthy()
        fireEvent.blur(getInput() || window);
        expect(getComponent()).toBeTruthy()
        expect(getInput()).not.toBeTruthy()
    })
});
