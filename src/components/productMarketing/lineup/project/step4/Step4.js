import React, { useState, useEffect, Fragment } from 'react';
import _ from 'lodash';
import './Step4.css';
import Loader from '../../../../EmptyScreen/Loader';
import { BottomButtons, Button, StateButton, MultipleStateButton } from '../../../../common';

import { getParams } from '../../../../../actions/parameters';
import { getLineUpMap } from '../../../../../actions/lineupmap';
import { getPositioning } from '../../../../../actions/positioning';

import sort from '../../../../../utils/sort';
import { ModelLine, PERIOD_IDS, KPI_IDS, FINANCIAL_IDS, AGGREGATION } from './ModelLine';
import { SumLine } from './SumLine';
import { HeaderLine } from './HeaderLine';
import * as PROJECT_ACTIONS from '../../../../../actions/project';
import * as MODEL_ACTIONS from '../../../../../actions/models';

import msrp from '../../../../../utils/msrp';

const DEFAULT_SELECTED_FINANCIAL_COLUMNS = [
    FINANCIAL_IDS.MSRP,
    FINANCIAL_IDS.MGP,
    FINANCIAL_IDS.DISCOUNTS_P,
    FINANCIAL_IDS.TM_DISCOUNTS_P,
    FINANCIAL_IDS.GASV,
    FINANCIAL_IDS.TPA_P,
    FINANCIAL_IDS.PRODUCT_COST,
    FINANCIAL_IDS.F_W,
    FINANCIAL_IDS.CUSTOM_P,
    FINANCIAL_IDS.MARGIN_P
];

export const finCols = [
    { id: FINANCIAL_IDS.MSRP, label: 'MSRP', aggregation: AGGREGATION.SUM, cardLabel: 'MSRP', showOnCard: true },
    { id: FINANCIAL_IDS.MGP, label: 'MGP', aggregation: AGGREGATION.SUM, cardLabel: 'MGP', showOnCard: true },
    { id: FINANCIAL_IDS.DISCOUNTS_P, label: 'Disc., %', type: 'percent', editing: true, aggregation: AGGREGATION.AVG },
    { id: FINANCIAL_IDS.DISCOUNTS_V, label: 'Disc., value', hidden: true, aggregation: AGGREGATION.SUM },
    { id: FINANCIAL_IDS.AFTER_DISCOUNTS_V, label: 'Value disc.', hidden: true, aggregation: AGGREGATION.SUM },
    { id: FINANCIAL_IDS.TM_DISCOUNTS_P, label: 'TM disc., %', type: 'percent', editing: true, aggregation: AGGREGATION.AVG },
    { id: FINANCIAL_IDS.TM_DISCOUNTS_V, label: 'TM disc., value', hidden: true },
    { id: FINANCIAL_IDS.GASV, label: 'GASV', aggregation: AGGREGATION.SUM, cardLabel: 'GASV', showOnCard: true },
    { id: FINANCIAL_IDS.TPA_P, label: 'TPA, %', type: 'percent', editing: true, aggregation: AGGREGATION.AVG, cardLabel: 'TPA', showOnCard: true },
    { id: FINANCIAL_IDS.TPA_V, label: 'TPA', hidden: true, aggregation: AGGREGATION.SUM },
    { id: FINANCIAL_IDS.NASV, label: 'NASV', aggregation: AGGREGATION.SUM, cardLabel: 'NASV', showOnCard: true },
    { id: FINANCIAL_IDS.TOW_V, label: 'TOW', hidden: true, aggregation: AGGREGATION.SUM },
    { id: FINANCIAL_IDS.TOW_P, label: 'TOW, %', type: 'percent', aggregation: AGGREGATION.AVG, cardLabel: 'TOW', showOnCard: true },
    { id: FINANCIAL_IDS.PRODUCT_COST, label: 'Product cost', editing: true, aggregation: AGGREGATION.SUM, cardLabel: 'Pr. cost', showOnCard: true },
    { id: FINANCIAL_IDS.F_W, label: 'F&W', editing: true, aggregation: AGGREGATION.SUM },
    { id: FINANCIAL_IDS.CUSTOM_P, label: 'Custom dut., %', type: 'percent', editing: true, aggregation: AGGREGATION.AVG },
    { id: FINANCIAL_IDS.CUSTOM_V, label: 'Cust. dut., value', aggregation: AGGREGATION.SUM },
    { id: FINANCIAL_IDS.MARGIN_V, label: 'Margin, value', aggregation: AGGREGATION.SUM, cardLabel: 'Margin', showOnCard: true },
    { id: FINANCIAL_IDS.MARGIN_P, label: 'Margin, %', type: 'percent', aggregation: AGGREGATION.AVG, cardLabel: 'Margin', showOnCard: true },
];

export const kpis = [
    { id: KPI_IDS.VOLUME, label: 'VOLUME', type: 'number', cardLabel: 'Volume FY', showOnCard: true },
    { id: KPI_IDS.VALUE, label: 'VALUE', type: 'price', cardLabel: 'Value FY', showOnCard: true },
    { id: KPI_IDS.MARGIN, label: 'MARGIN', type: 'price', cardLabel: 'Margin FY', showOnCard: true }
];

const periods = [
    { id: PERIOD_IDS.YEAR, label: 'BY YEARS' },
    { id: PERIOD_IDS.QUARTER, label: 'BY QUARTERS' },
    { id: PERIOD_IDS.MONTH, label: 'BY MONTHS', hidden: true }
]

function Step4({
    newProject,
    setNewProject,
    onNextStep,
    disableEdit
}) {
    const [xSpec, setXSpec] = useState(null);
    const [ySpec, setYSpec] = useState(null);
    const [specs, setSpecs] = useState(null);
    const [segmentDescriptions, setSD] = useState(null);
    const [localModels, setLocalModels] = useState(null);
    const [isSaving, setIsSaving] = useState({ apply: false, next: false });

    const [period, setPeriod] = useState(PERIOD_IDS.QUARTER);
    const [selectedSpecs, setSelectedSpecs] = useState([]);
    const [selectedFinParametes, setSelectedFinParametes] = useState(DEFAULT_SELECTED_FINANCIAL_COLUMNS);
    const [kpi, setKpi] = useState([KPI_IDS.VOLUME, KPI_IDS.VALUE, KPI_IDS.MARGIN]);

    useEffect(() => {
        if (newProject && !_.isEmpty(newProject)) {
            const priceIndexRange = newProject.params.priceIndexRange;
            Promise.all([
                getParams(newProject.params) || {},
                getLineUpMap(newProject.params),
                getPositioning(newProject.owner, newProject.name, priceIndexRange ? JSON.stringify([
                    priceIndexRange.Value[0] * priceIndexRange.Percent | 0,
                    priceIndexRange.Value[1] * priceIndexRange.Percent | 0
                ]) : '')
            ]).then(([params, lineupmap, positioning]) => {
                const sortedSpecs = _.sortBy(
                    params?.specs,
                    spec => (!!~_.findIndex(newProject.params.specs, s => s === spec.name) ? _.findIndex(newProject.params.specs, s => s === spec.name) : 999)
                );
                const xSpec = { ...(sortedSpecs[0] || {}), value: _.reverse([...(sortedSpecs[0]?.value || [])]) };
                const ySpec = sortedSpecs[1];
                setSpecs(_.take(sortedSpecs, newProject.params.specs.length));
                setXSpec(xSpec);
                setYSpec(ySpec);
                setSD(lineupmap?.segments);
                setLocalModels(newProject.models.map(m => {
                    const compareSpecs = (specs) => specs.reduce((acc, item) => ({
                        ...acc,
                        [item.id]: item.value
                    }), {});
                    const positionedModel = positioning.models.find(posModel => {
                        return _.isEqual(compareSpecs(posModel.specs), compareSpecs(m.specs))
                    });
                    const modelPrice = m.price ||
                        Math.round(newProject.params.priceIndex * (positionedModel?.price || 0) / 100);
                    return {
                        ...m,
                        changedSpecs: m.specs,
                        price:        modelPrice,
                        changedPrice: modelPrice,
                        msrp:        m.msrp || msrp(modelPrice, newProject.params.msrpVsMgp),
                        changedMsrp: m.msrp || msrp(modelPrice, newProject.params.msrpVsMgp)
                    };
                }));

                setPeriod(_.get(newProject, 'businessCase.period', PERIOD_IDS.QUARTER));
                setKpi([_.get(newProject, 'businessCase.kpi', KPI_IDS.VOLUME), ..._.xor(_.values(KPI_IDS), [_.get(newProject, 'businessCase.kpi', KPI_IDS.VOLUME)])])
                setSelectedSpecs(_.get(newProject, 'businessCase.specs', sortedSpecs.map(s => s.id)));
                setSelectedFinParametes(_.get(newProject, 'businessCase.financials', DEFAULT_SELECTED_FINANCIAL_COLUMNS));
            });
        }
    }, [newProject]);

    useEffect(() => {
        setSelectedSpecs((specs || []).map(a => a.id));
    }, [specs]);

    const onCancel = () => {
        setPeriod(_.get(newProject, 'businessCase.period', PERIOD_IDS.QUARTER));
        setKpi([_.get(newProject, 'businessCase.kpi', KPI_IDS.VOLUME), ..._.xor(_.values(KPI_IDS), [_.get(newProject, 'businessCase.kpi', KPI_IDS.VOLUME)])])
        setSelectedSpecs(_.get(newProject, 'businessCase.specs', []));
        setSelectedFinParametes(_.get(newProject, 'businessCase.financials', DEFAULT_SELECTED_FINANCIAL_COLUMNS));
        setLocalModels(newProject.models)
    }

    const onApply = async () => {
        await MODEL_ACTIONS.saveModels(newProject, localModels);
        setNewProject({
            ...newProject,
            businessCase: {
                financials: selectedFinParametes,
                specs: selectedSpecs,
                kpi: kpi[0],
                period: period
            },
            models: localModels.map(model => ({ ...model, price: model.changedPrice }))
        });
        PROJECT_ACTIONS.saveProjectBusinessCase(newProject.name, newProject.owner, {
            financials: selectedFinParametes,
            kpi: kpi[0],
            period: period,
            specs: selectedSpecs
        });
    }

    if (!(xSpec && ySpec && localModels && segmentDescriptions)) return <div className='loaderWrapper'><Loader /></div>;
    return (
        <div className='Step4'>
            <div className='wrapper'>
                <div className='header-buttons'>
                    <MultipleStateButton
                        name={'SPECS'}
                        states={specs.slice(1).map(s => ({ id: `${s.id}`, label: s.name }))}
                        selectedStateIds={selectedSpecs}
                        onChangeSelectedState={setSelectedSpecs}
                    />
                    <MultipleStateButton
                        name={'FINANCIALS'}
                        states={finCols.filter(f => !f.hidden)}
                        selectedStateIds={selectedFinParametes}
                        onChangeSelectedState={setSelectedFinParametes}
                    />
                    <StateButton
                        name={'KPIs'}
                        states={kpis}
                        selectedStateId={kpi[0]}
                        onChangeSelectedState={kpi_id => setKpi([kpi_id, ..._.xor(_.values(KPI_IDS), [kpi_id])])}
                    />
                    <StateButton
                        name={'PERIOD'}
                        states={periods.filter(p => !p.hidden)}
                        selectedStateId={period}
                        onChangeSelectedState={setPeriod}
                    />
                </div>
                <table className='Table'>
                    <tbody>
                        <HeaderLine
                            finCols={finCols}
                            kpis={kpis}
                            kpi={kpi}
                            period={period}
                            specs={specs}
                            selectedSpecs={selectedSpecs}
                            selectedFinParametes={selectedFinParametes}
                        />

                        {
                            xSpec.value
                                .sort((a, b) => sort(xSpec.name, false, a, b))
                                .map(xValue => {
                                    const models = localModels.filter(
                                        model => model.specs.find(s => s.id === xSpec.id).value === `${xValue}`
                                    )
                                    if (models.length === 0) return null

                                    return (<Fragment key={`hm_${xValue}`}>
                                        <tr className='Row SpecHeader' key={`h_${xValue}`}>
                                            <td className='Cell'>
                                                <h3>{xValue}</h3>
                                            </td>
                                        </tr>
                                        {models.map((model, i, arr) => {
                                            return (
                                                <ModelLine
                                                    key={`ml${model.id}`}
                                                    model={model}
                                                    specs={specs}
                                                    selectedSpecs={selectedSpecs}
                                                    selectedFinParametes={selectedFinParametes}
                                                    finCols={finCols}
                                                    period={period}
                                                    kpi={kpi}
                                                    kpis={kpis}
                                                    currency={newProject.params.currency}
                                                    modelsCount={(arr || []).length}
                                                    isFirst={!i}
                                                    onModelChange={nm => {
                                                        const ind = localModels.findIndex(m => nm.id === m.id);
                                                        setLocalModels(_localModels => [
                                                            ..._localModels.slice(0, ~ind ? ind : localModels.length),
                                                            nm,
                                                            ..._localModels.slice(
                                                                (~ind ? ind : localModels.length) + 1,
                                                                localModels.length
                                                            ),
                                                        ]);
                                                    }}
                                                />
                                            )
                                        })}

                                    </Fragment>)
                            })
                        }
                        <SumLine
                            models={localModels}
                            specs={specs}
                            selectedSpecs={selectedSpecs}
                            selectedFinParametes={selectedFinParametes}
                            finCols={finCols}
                            period={period}
                            kpi={kpi}
                            kpis={kpis}
                            currency={newProject.params.currency}
                        />
                    </tbody>
                </table>

            </div>

            <BottomButtons>
                <Button
                    label={'SKIP'}
                    margined={20}
                    disabled={false}
                    destruction={true}
                    onPress={onNextStep}
                    isBottomButton={true}
                />
                <Button
                    label={'CANCEL'}
                    margined={20}
                    disabled={!isEdited(newProject, selectedFinParametes, kpi, period, selectedSpecs, localModels)}
                    onPress={onCancel}
                    isBottomButton={true}
                />
                <Button
                    label={'APPLY'}
                    margined={20}
                    disabled={!isEdited(newProject, selectedFinParametes, kpi, period, selectedSpecs, localModels) || disableEdit}
                    showLoader={isSaving.apply}
                    onPress={() => {
                        setIsSaving({ ...isSaving, apply: true });
                        onApply().then(() => {
                            setIsSaving({ ...isSaving, apply: false })
                        });
                    }}
                    isBottomButton={true}
                />
                <Button
                    label={'NEXT'}
                    margined={20}
                    disabled={isEdited(newProject, selectedFinParametes, kpi, period, selectedSpecs, localModels)}
                    showLoader={isSaving.next}
                    onPress={() => {
                        setIsSaving({ ...isSaving, next: true });
                        Promise.resolve(!disableEdit ? onApply() : true).then(() => {
                            setIsSaving({ ...isSaving, next: false })
                            onNextStep()
                        })
                    }}
                    isBottomButton={true}
                />
            </BottomButtons>
        </div>
    );
}

const isEdited = (project, financials, kpi, period, specs, localModels) => {
    return !(_.isEqual(_.get(project, 'businessCase.period'), period) &&
        _.isEqual(_.get(project, 'businessCase.kpi'), kpi[0]) &&
        _.isEqual(_.get(project, 'businessCase.specs'), specs) &&
        _.isEqual(_.get(project, 'businessCase.financials'), financials)) || isModelsEdited(project.models, localModels);
}

const isModelsEdited = (models, localModels) => {
    for (const m of localModels) {
        const modelToCompare = models.find(mm => mm.id === m.id);
        const fin = modelToCompare.financials ? _.pick(m.financials, _.keys(modelToCompare.financials)) : undefined;
        if (!_.isEqual(fin, modelToCompare.financials) || !_.isEqual(modelToCompare.sums, m.sums)) {
            return true;
        }
    };
    return false;
}

export default Step4;
