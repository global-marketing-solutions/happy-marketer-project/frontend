import React, { Fragment } from 'react';
import './Step4.css';
import { PERIOD_IDS } from './ModelLine';

export function HeaderLine({
    specs,
    selectedSpecs,
    selectedFinParametes,
    finCols,
    period,
    kpis,
    kpi,
}) {
    return (
        <tr key={`header_line`} className='Row Header'>
            <td className='Cell Border'>Model Name</td>
            <td className='Cell Divider'></td>
            {specs.slice(1).filter(s => selectedSpecs.includes(s.id)).map(s => (
                <td className={`Cell Border`} key={`h_${s.id}`}>{s.name}</td>
            ))}
            <td className='Cell Divider'></td>
            {finCols.filter(fc => selectedFinParametes.includes(fc.id)).map(fc => (
                <td className={`Cell min70 Border`} key={`h_${fc.id}`}>{fc.label}</td>
            ))}
            <td className='Cell Divider'></td>
            {kpi.map(kpi => kpis.find(k => kpi === k.id).label).map((name, ind) => (
                <Fragment key={`hm_${name}`}>
                    {period === PERIOD_IDS.MONTH && !ind &&
                        ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'].map(m => (
                            <td className={`Cell min70 Border`} key={`hm_${name}_${m}`}>{m}<br />{name}</td>
                        ))
                    }
                    {period === PERIOD_IDS.QUARTER && !ind &&
                        ['Q1', 'Q2', 'Q3', 'Q4'].map(m => (
                            <td className={`Cell min70 Border`} key={`hq_${name}_${m}`}>{name}<br />{m}</td>
                        ))
                    }
                    <td className={`Cell min70 Border`} key={`h_${name}_${ind}_FY`}>{name}<br />FY</td>
                </Fragment>)
            )}
        </tr>
    )
}
