import _ from 'lodash';
import React, { useState, useEffect, useContext } from 'react';
import './Project.css'
import Step1 from './step1/Step1';
import Step2 from './step2/Step2';
import Step3 from './step3/Step3';
import Step4 from './step4/Step4';
import Step5 from './step5/Step5';
import EmptyScreen from '../../../EmptyScreen/EmptyScreen';
import Loader from '../../../EmptyScreen/Loader';
import { Switch, Route, Redirect } from 'react-router-dom';
import Breadcrumbs from './breadcrumbs/Breadcrumbs';
import AuthContext from '../../../../contexts/auth-context';

import * as PROJECT_ACTIONS from '../../../../actions/project';

const steps = [{
    id: 'step1',
    component: Step1,
    title: 'Step 1',
    description: 'Parameters',
    enabled: true
}, {
    id: 'step2',
    component: Step2,
    title: 'Step 2',
    description: 'Line Up Map',
    enabled: true
}, {
    id: 'step3',
    component: Step3,
    title: 'Step 3',
    description: 'Positioning',
    enabled: true
},
{
    id: 'step4',
    component: Step4,
    title: 'Step 4',
    description: 'Business Case',
    enabled: true,
},
{
    id: 'step5',
    component: Step5,
    title: 'Step 5',
    description: 'Summary',
    enabled: true
}]

const isBreadcrumbActive = (
    breadcrumbId,
    project
) => {
    let active = true;
    const allRequeredFieldsAreFilled = project.params ? Object.entries(project.params)
        .every(([k, v]) => v || k === 'priceIndexRange' || k === 'restrictions' || k === 'selectedStateIds') : false;

    if (breadcrumbId === 'step1') { return active; }
    active = active && allRequeredFieldsAreFilled;
    if (breadcrumbId === 'step2') { return active; }
    active = active && !_.isEmpty(project.models);
    if (breadcrumbId === 'step3') { return active; }
    active = active && allRequeredFieldsAreFilled;
    if (breadcrumbId === 'step4') { return active; }
    if (breadcrumbId === 'step5') { return active; }
    return false;
}

function Project({ match, location }) {
    const { params } = match;

    const [newProject, _setNewProject] = useState({});

    const setNewProject = (nn) => {
        _setNewProject(nn)
    }

    const [redir, _setRedir] = useState(null)

    const setRedir = (r) => {
        _setRedir(r);
        setTimeout(() => _setRedir(null), 500)
    }

    const archived = location.pathname.startsWith('/archive');

    useEffect(() => {
        PROJECT_ACTIONS.getProject(params.projectId, archived).then((project) => {
            setNewProject(project);
        })
    }, [params.projectId, archived])

    const auth = useContext(AuthContext);

    const sharedMode = ((newProject.shared || []).find(a => auth.user.email.startsWith(`${a.username}@`)) || {}).mode;
    return newProject ? (
        <div className='Project'>
            <Breadcrumbs
                breadcrumbs={steps}
                isActive={(bId) => isBreadcrumbActive(bId, newProject)}
            />
            <label>Project Name: <h1 style={{display: 'inline'}}>{newProject.name}</h1></label>
            {!!redir ? <Redirect
                to={`${archived ? '/archive/' : '/product-management/line-up-creator/'}${params.projectId}/${redir}`}
            /> : null}
            <Switch>
                <Redirect
                    exact
                    from={`${archived ? '/archive/' : '/product-management/line-up-creator/'}:projectId`}
                    to={`${archived ? '/archive/' : '/product-management/line-up-creator/'}${params.projectId}/${steps[0].id}`}
                />
                {steps.map((step, ind) => {
                    const CurrentComponent = step.component || EmptyScreen
                    return (
                        <Route
                            key={step.id}
                            path={`${archived ? '/archive/' : '/product-management/line-up-creator/'}:projectId/${step.id}`}
                            render={props => (<CurrentComponent
                                {...props}

                                newProject={newProject}
                                setNewProject={setNewProject}

                                disableEdit={archived || (sharedMode ? sharedMode !== 'edit' : false)}

                                onNextStep={() => {
                                    setRedir(steps[step.nextIndex || ((ind + 1 === steps.length) ? 0 : (ind + 1))].id)
                                }}
                            />)}
                        />
                    )
                }
                )}
            </Switch>
        </div>
    ) : <Loader />
}

export default Project;
