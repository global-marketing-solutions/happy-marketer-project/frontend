import React from 'react';
import SegmentCell from '../SegmentCell/SegmentCell';
import './TableRow.css';

function TableRow({
    title,
    subtitle,
    ySpec,
    columnValues,
    models = [],
    rowspan,
    currency,
    competitors,
    modelsForSegment = (models, yValue) => (models || []).filter(m => {
        return String(m.specs.find(s => s.id === ySpec.id).value) === String(yValue)
    }),
    competitorsLessRanked = [],
    changeCompetitor = () => {},

    segmentDescriptions = [],
    showSegmentInfo = true,
    editProperties = true,
    showPrice = true,
    specs,
    brand,
    modelsDeletable = true,
    showFinancials = false,
    shownSpecs,
    shownFinancials,
    shownKpis,

    isEditable,
    onChangeModel
}) {
    const newModels = models.filter(m => m.isNew)
    return (
        <tr>
            {title && <th className='content-block header-element' rowSpan={rowspan}>
                <div>{title.toString()}</div>
                {title.callback && <div style={{display: 'flex'}}><input
                    type='number'
                    min='1'
                    defaultValue={title.percent}
                    onChange={e => title.callback(title.toString(), +e.target.value)}
                    style={{
                        marginLeft: '4px',
                        fontSize: '13px',
                        width: '25px',
                        background: '#eee',
                        border: 'none',
                        textAlign: 'left'
                    }}
                /><span style={{
                    fontSize: '15px',
                    position: 'relative',
                    top: '3px',
                    left: '-17px',
                    marginRight: '-9px'
                }}>%</span></div>}
                <div style={{margin: '20px 8px'}}>{subtitle}</div>
            </th>}
            {columnValues.map((yValue, i) => {
                const segmentPercent = (segmentDescriptions.find(sd => sd.y === String(yValue)))?.percent;
                return (<td key={`${yValue}${models.length}`} style={{height: competitors ? '288px' : ''}}>
                    <SegmentCell
                        showSegmentInfo={showSegmentInfo}

                        models={modelsForSegment(models, yValue, competitors, i).filter(m => !m.isNew)}
                        competitorsLessRanked={competitorsLessRanked.filter(c => c.brand === yValue)}
                        changeCompetitor={changeCompetitor}

                        currency={currency}
                        isEditable={typeof isEditable === 'boolean' ? isEditable : isEditable(i)}
                        percent={segmentPercent}
                        onChangeModel={onChangeModel}
                        editProperties={editProperties}
                        showPrice={showPrice}
                        specs={specs}
                        brand={brand}
                        modelsDeletable={modelsDeletable}
                        showFinancials={showFinancials}

                        shownSpecs={shownSpecs}
                        shownFinancials={shownFinancials}
                        shownKpis={shownKpis}
                    />
                </td>);
            })}
            <td>
                {newModels.length > 0 && (
                    <SegmentCell
                        specs={specs}
                        models={newModels}
                        isEditable={true}
                        onChangeModel={onChangeModel}
                        brand={brand}
                        showPrice={false}
                    />
                )}
            </td>
        </tr>
    );
}

export default TableRow;
