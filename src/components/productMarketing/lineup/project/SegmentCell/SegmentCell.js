import React from 'react';
import './SegmentCell.css';
import StatsSet from '../StatsSet/StatsSet';
import ItemCard from '../Model/ItemCard';

function SegmentCell({
    showSegmentInfo = true,

    models,
    competitorsLessRanked,
    changeCompetitor,

    currency,
    isEditable,
    percent,
    onChangeModel,
    editProperties = true,
    showPrice = true,
    specs,
    brand,
    modelsDeletable = true,
    showFinancials = false,

    shownSpecs,
    shownFinancials,
    shownKpis
}) {
    return (
        <div className='cell-wrapper'>
            <div className='SegmentCell'>
                {!(models || [{}])[0]?._id && (
                    percent ? <StatsSet percent={percent}
                        hide={!showSegmentInfo} /> :
                        <StatsSet empty={true}
                            hide={!showSegmentInfo} />
                )}
                <div className='items'>
                    {models.map((model, i) => {
                        const m = isEditable || competitorsLessRanked || shownKpis ? model : {};
                        return <ItemCard
                            key={i}
                            model={m}
                            currency={currency}
                            isEditable={isEditable}
                            editProperties={editProperties}
                            onChangeModel={onChangeModel}
                            specs={specs}
                            brand={brand}
                            showPrice={showPrice}
                            changeCompetitor={(fromId, toId) => changeCompetitor(fromId, toId, i)}
                            competitorsLessRanked={competitorsLessRanked}
                            hideDelete={!modelsDeletable}
                            showFinancials={showFinancials}
                            shownSpecs={shownSpecs}
                            shownFinancials={shownFinancials}
                            shownKpis={shownKpis}
                    />})}
                </div>
            </div>
        </div>
    )
}

export default SegmentCell;
