import React, { useState, useEffect, useRef } from 'react';
import _ from 'lodash';

import './ItemCard.css';

import formatPrice from '../../../../utils/formatPrice';
import { formatPriceNumber } from '../../../../utils/formatPrice';
import { StateButton, Inputs } from '../../../../common';

import { sumArray, getFinancials } from '../step4/ModelLine';
import { finCols, kpis } from '../step4/Step4';

import { prod } from '../../../../../config.json';

function ItemCard({
    model,
    currency,
    specs,
    isEditable = true,
    onChangeModel,
    editProperties = true,
    competitorsLessRanked = [],
    changeCompetitor,
    brand,
    hideDelete = false,
    showPrice = true,
    showFinancials = false,
    shownSpecs,
    shownFinancials,
    shownKpis
}) {
    const [mode, setMode] = useState(isEdited(model, showPrice) ? 'change' : 'confirm');

    const [isZoom, setIsZoom] = useState();

    useEffect(() => {
        if (mode === 'change' && !model.isNew) {
            setMode('confirm')
        }
    }, [model.isNew]);  // eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
        if (!model.price || !model.changedPrice) return;
        if (model.price === model.changedPrice) setMode('confirm')
    }, [model.price, model.changedPrice]);

    useEffect(() => {
        if (!model.msrp || !model.changedMsrp) return;
        if (model.msrp === model.changedMsrp) setMode('confirm')
    }, [model.msrp, model.changedMsrp]);

    const prevModel = useRef(model);
    useEffect(() => {
        isEdited(prevModel.current) && !isEdited(model) && setMode('confirm')
        prevModel.current = model;
    }, [model]);

    const updateMode = (mode) => {
        onChangeModel({ ...model, toDelete: mode === 'delete' })
        setMode(mode)
    }

    const updateLocalSpec = (specId, value) => {
        onChangeModel({
            ...model,
            changedSpecs: _.map(model.changedSpecs, (spec) => {
                return spec.id === specId ? { ...spec, value } : spec
            })
        })
    }

    const onChangeMgp = (value) => {
        onChangeModel({
            ...model,
            changedPrice: +value
        })
    }

    const onChangeMsrp = (value) => {
        onChangeModel({
            ...model,
            changedMsrp: +value
        })
    }

    const modelFinancials = model.financials ? getFinancials({ ...model.financials, mgp: model.price }) : null;

    const isStep3 = model.changedMsrp || (model.params && model.params[0]['id'] === 'distribution');

    return (
        <div className='ItemCard content-block'>

            {isStep3 ?
                <div style={{display: 'flex'}}>
                    {model.brand && <a
                        title='Click for more information about this model'
                        style={{fontFamily: 'Material Icons', fontSize: '22px', color: 'blue', cursor: 'pointer'}}
                        href={`${prod.stage}models/${model.id.replaceAll('/', '%2F').replaceAll('\\', '%5C')}?brand=${model.brand}`}
                        target='_blank' rel='noopener noreferrer'
                    >link</a>}
                    {competitorsLessRanked.length > 0 ? <Inputs.Select
                        options={
                            [{ label: model.id || 'No match', key: model.id, value: model.id }].concat(
                                competitorsLessRanked.map(c => Object({ label: c.id, key: c.id, value: c.id }))
                            )
                        }
                        onChange={id => changeCompetitor(model.id, id)}
                        isOld={true}
                    /> : <div style={{width: '123px', textAlign: 'center'}}>{model.id}</div>}
                </div>
            : mode === 'confirm' && model.id}

            {competitorsLessRanked.length === 0 && <div>{model.typeOfPrice && mode === 'change' && <Inputs.Input
                    placeholder='Model name'
                    value={model.id}
                    onChange={id => onChangeModel({...model, id})}
                />}
            </div>}

            {(model.name || brand) && <div className='brand' style={{textAlign: 'center'}}>{`${model.name || brand}`}</div>}

            {isStep3 && (model.brand ?
                <img
                    src={`${prod.stage}models/${model.id.replaceAll('/', '%2F')}?brand=${model.brand}&isImage=true`}
                    width='120px'
                    className={isZoom ? 'isZoom' : ''}
                    style={{
                        margin: 'auto',
                        fontSize: '26px',
                        color: '#999'
                    }}
                    onClick={e => !e.target.classList.contains('error') && setIsZoom(!isZoom)}
                    onError={e =>  e.target.classList.add('error')}
                    onLoad={e  =>  e.target.classList.remove('error')}
                    alt=''
                /> : <div style={{fontSize: '29px', color: '#999', margin: 'auto'}}>No image</div>
            )}

            <div className='properties'>
                {model.changedSpecs && specs
                    .filter(s => model.changedSpecs.map(s => s.id).includes(s.id))
                    .filter(s => shownSpecs ? shownSpecs.includes(s.id) : true)
                    .map(spec => (
                        <Property
                            key={spec.id}
                            name={spec.name}
                            {...(editProperties ? { mode } : {})}
                            spec={spec}
                            specValues={model.changedSpecs}
                            onChange={updateLocalSpec}
                        />
                    ))
                }
                {model.params && model.params.map(param => (
                    <Property
                        key={param.id}
                        name={param.name}
                        value={param.value}
                    />
                ))}

                {model.typeOfPrice &&
                    <div style={{display: 'flex', justifyContent: 'space-between'}}>
                        <span>{model.typeOfPrice}</span>
                        {mode === 'change' ? <input
                            type='number'
                            min='1'
                            defaultValue={model.price}
                            style={{width: '60%'}}
                            onChange={e => onChangeModel({
                                ...model,
                                price: +e.target.value
                            })} /> : <div className='price-block'>{currency + model.price}</div>}
                    </div>
                }

                {modelFinancials && showFinancials && finCols
                        .filter(c => c.showOnCard)
                        .filter(c => shownFinancials ? shownFinancials.includes(c.id) : true)
                        .map(({ id, cardLabel }) => {
                            return <Property
                                key={id}
                                name={cardLabel}
                                value={
                                    isNaN(modelFinancials[id]) ? '-' :
                                    `${formatPriceNumber(modelFinancials[id] || 0)}${id.endsWith('_p') ? '%' : ''}`
                                }
                            />;
                        })
                }
                {model.sums && showFinancials && <Property name={' '} value={' '} />}
                {model.sums && showFinancials && kpis
                        .filter(k => k.showOnCard)
                        .filter(k => shownKpis ? shownKpis.includes(k.id) : true)
                        .map(({ id, cardLabel, type }) => {
                    return <Property
                        key={id}
                        name={cardLabel}
                        value={Object.values(modelFinancials || {}).some(isNaN) ? '-' :
                            (type === 'price') ? formatPriceNumber(sumArray(model.sums[id])) :
                                sumArray(model.sums[id])}
                    />;
                })}
            </div>
            {showPrice && <PriceBlock
                currency={currency}
                mgp={showFinancials ? null : model.changedPrice || model.price}
                msrp={model.msrp}
                {...(isEditable ? { mode } : {})}
                onChangeMgp={onChangeMgp}
                onChangeMsrp={onChangeMsrp}
            />}
            {isEditable && <div className='button-block'><StateButton
                selectedStateId={mode}
                onChangeSelectedState={updateMode}
                ignore={isEdited(model) ? 'confirm' : hideDelete ? 'delete' : ''}
            /></div>}
        </div>
    )
}

function PriceBlock({ currency = 'eur', mgp, msrp, mode = 'confirm', onChangeMgp, onChangeMsrp }) {
    if (mgp || msrp) {
        if (mode === 'change') {
            return <>

            <div className='price-row edit'>
                <span>MGP</span>
                <span className='price-block'>
                    <Inputs.Input
                        type='number'
                        value={mgp}
                        onChange={onChangeMgp}
                    />
                </span>
            </div>

            <div className='price-row edit'>
                <span>MSRP</span>
                <span className='price-block'>
                    <Inputs.Input
                        type='number'
                        value={msrp}
                        onChange={onChangeMsrp}
                    />
                </span>
            </div>

            </>
        }
        return (
            <>

            {mgp && <div className='price-row'>
                <span>MGP</span>
                <span className='price-block'>{formatPrice(+mgp, currency, 0)}</span>
            </div>}

            {msrp && <div className='price-row'>
                <span>MSRP</span>
                <span className='price-block'>{formatPrice(+msrp, currency, 0)}</span>
            </div>}

            </>
        )
    }
    return <div></div>;
}

const isEdited = (model, showPrice) => {
    return model.isNew || !_.isEqual(model.specs, model.changedSpecs) ||
        (showPrice && model.changedPrice ?
            model.price !== model.changedPrice
            : false
        );
}

function Property({ name, value, specValues, spec = {}, mode = 'confirm', onChange }) {
    const displayValue = value === undefined ? specValues.find(s => spec.id === s.id).value : value;
    const [opened, setOpened] = useState(false);
    const onClick = () => mode === 'change' && setOpened(!opened)
    const changeClass = mode === 'change' ? 'change' : ''

    return (
        <div className={`property ${opened ? 'opened' : ''} ${changeClass}`} key={name} onClick={onClick}>
            <div className='cell property-name'>{`${name}`}</div>
            <div className='cell'>{`${displayValue}`}</div>
            {mode === 'change' && <div className='cell chevron'>keyboard_arrow_down</div>}
            <div className='list'>
                {spec.value && spec.value.map(val => (
                    <div className={'listitem'} key={val} onClick={() => {
                        onChange && onChange(spec.id, `${val}`)
                        setOpened(false);
                    }}>
                        {val}
                    </div>
                ))}
            </div>
        </div>
    )
}

export default ItemCard;
