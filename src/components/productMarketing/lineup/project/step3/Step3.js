import React, { useState, useEffect } from 'react';
import './Step3.css';

import HeaderRow from '../HeaderRow';
import TableRow from '../TableRow';
import sort from '../../../../../utils/sort';
import Loader from '../../../../EmptyScreen/Loader';
import { BottomButtons, Button, MultipleStateButton } from '../../../../common';
import { getPositioning } from '../../../../../actions/positioning';
import { getLineUpMap } from '../../../../../actions/lineupmap';
import { getParams } from '../../../../../actions/parameters';
import { saveModels } from '../../../../../actions/models';
import { saveCompetitors } from '../../../../../actions/competitors';

import msrp from '../../../../../utils/msrp';

import * as PROJECT_ACTIONS from '../../../../../actions/project';

import _ from 'lodash';

function Step3({
    newProject,
    setNewProject,
    onNextStep,
    disableEdit
}) {
    const [xSpec, setXSpec] = useState(null);
    const [ySpec, setYSpec] = useState(null);
    const [specs, setSpecs] = useState(null);

    const [selectedStateIdsDefault, setSelectedStateIdsDefault] = useState();
    const [selectedStateIds, setSelectedStateIds] = useState();

    const [segmentDescriptions, setSD] = useState(null);
    const [localModels, setLocalModels] = useState(null);

    const [competitors, setCompetitors] = useState([]);
    const [competitorsIds, setCompetitorsIds] = useState([]);
    const [competitorsLessRanked, setCompetitorsLessRanked] = useState([]);

    const [isSaving, setIsSaving] = useState({ apply: false, next: false });

    const [pageValue, setPageValue] = useState(null)

    useEffect(() => {
        if (Object.keys(newProject).length && isSaving.apply === false && isSaving.next === false) {
            const priceIndexRange = newProject.params.priceIndexRange
            Promise.all([
                getPositioning(newProject.owner, newProject.name, priceIndexRange ? JSON.stringify([
                    priceIndexRange.Value[0] * priceIndexRange.Percent | 0,
                    priceIndexRange.Value[1] * priceIndexRange.Percent | 0
                ]) : ''),
                getLineUpMap(newProject.params),
                getParams(newProject.params)
            ]).then(([positioning, lineupmap, params]) => {
                if (!params) return

                const [xSpec, ySpec] = newProject.params.specs.map(s => params.specs.find(_s => _s.id === s))
                setSpecs([xSpec, ySpec]);
                setXSpec(xSpec);
                setYSpec(ySpec);

                if (newProject.params.selectedStateIds?.step3) {
                    setSelectedStateIdsDefault(newProject.params.selectedStateIds.step3)
                    setSelectedStateIds(newProject.params.selectedStateIds.step3)
                    setPageValue(old => old || Object.keys(newProject.params.selectedStateIds.step3)[0])
                } else {

                    const specsUnordered = (newProject.models || lineupmap.models).reduce((acc, m) =>
                        acc.add(m.specs.find(m => m.id === xSpec.id).value)

                    , new Set())

                    const selectedStateIdsDefault = xSpec.value
                        .filter(x => specsUnordered.has(x))
                        .reduce((acc, x) => {

                            acc[x] = [
                                ...ySpec.value
                                .filter(y => (newProject.models || lineupmap.models)
                                    .some(m => m.specs.find(m => m.id === xSpec.id).value === x &&
                                               m.specs.find(m => m.id === ySpec.id).value === y))
                                .map(y => `y_${y}`),
                                ...newProject.params.competitors.map(v => `b_${v}`)
                            ]
                            return acc
                        }, {})

                    setPageValue(Object.keys(selectedStateIdsDefault)[0])

                    setSelectedStateIds(selectedStateIdsDefault)
                    setSelectedStateIdsDefault(selectedStateIdsDefault)
                }

                setSD(lineupmap?.segments);
                setLocalModels(newProject.models.map(m => {
                    const positionedModel = positioning.models.find(posModel => {
                        return _.isEqual(posModel.specs, m.specs)
                    });
                    const modelPrice = m.price ||
                        Math.round(((positionedModel || {}).price || 0) * newProject.params.priceIndex / 100);
                    return ({
                        ...m,
                        changedSpecs: m.specs,
                        price:        modelPrice,
                        changedPrice: modelPrice,
                        msrp:        m.msrp || msrp(modelPrice, newProject.params.msrpVsMgp),
                        changedMsrp: m.msrp || msrp(modelPrice, newProject.params.msrpVsMgp)
                    });
                }));
                if (newProject.competitors) {
                    setCompetitors(newProject.competitors);
                    setCompetitorsIds(newProject.competitors.map(c => c.id))
                } else {
                    const comps = [];
                    newProject.models.forEach(m => {
                        for (const c of (positioning.competitors || [])) {
                            if (_.isEqual(c.specs, m.specs)) {
                                comps.push(c);
                            }
                        }
                    });
                    setCompetitors(comps);

                    setCompetitorsIds(positioning.competitors.map(c => c.id))
                }
                setCompetitorsLessRanked(positioning.competitorsLessRanked);
            });
        }
    }, [newProject, isSaving]);

    const onChangeModel = (nm) => {
        const ind = _.findIndex(localModels, m => nm.id === m.id);
        setLocalModels([
            ...localModels.slice(0, !!~ind ? ind : localModels.length),
            nm,
            ...localModels.slice((!!~ind ? ind : localModels.length) + 1, localModels.length),
        ]);
    }

    const onApply = async () => {
        const promises = []

        const _selectedStateIds = {
            ...newProject.params.selectedStateIds,
            step3: selectedStateIds
        }

        if (JSON.stringify(selectedStateIds) !== JSON.stringify(selectedStateIdsDefault)) {
            promises.push(PROJECT_ACTIONS.saveProjectParams(
                newProject.name,
                newProject.owner,
                {...newProject.params,
                    selectedStateIds: _selectedStateIds
                }
            ))
            setSelectedStateIdsDefault(selectedStateIds)
        }

        promises.push(saveModels(newProject, localModels))

        promises.push(saveCompetitors(newProject.name, newProject.owner, competitors))

        setCompetitorsIds(competitors.map(c => c.id))

        setNewProject({
            ...newProject,
            params: {
                ...newProject.params,
                selectedStateIds: _selectedStateIds,
            },
            models: localModels.map(m => ({
                ...m,
                price: m.changedPrice,
                msrp: m.changedMsrp
            })),
            competitors
        });

        return Promise.all(promises)
    }

    const onCancel = () => {
        setLocalModels(localModels.map(m => ({ ...m, changedPrice: m.price, changedMsrp: m.msrp })));
        let oldPositioned = _.reduce(_.flatten(_.values(competitorsLessRanked)), (acc, item) => {
            return !_.map(acc, i => i.id).includes(item.id) && competitorsIds.includes(item.id)
                ? [...acc, item] : acc
        }, []);
        oldPositioned = _.reduce(_.flatten(_.values(competitors)), (acc, item) => {
            return !_.map(acc, i => i.id).includes(item.id) && competitorsIds.includes(item.id)
                ? [...acc, item] : acc
        }, oldPositioned);

        let oldLessRanked = _.reduce(_.flatten(_.values(competitorsLessRanked)), (acc, item) => {
            return !_.map(acc, i => i.id).includes(item.id) && !competitorsIds.includes(item.id)
                ? [...acc, item] : acc
        }, []);
        oldLessRanked = _.reduce(_.flatten(_.values(competitors)), (acc, item) => {
            return !_.map(acc, i => i.id).includes(item.id) && !competitorsIds.includes(item.id)
                ? [...acc, item] : acc
        }, oldLessRanked);

        setSelectedStateIds(selectedStateIdsDefault)

        setCompetitors(oldPositioned);
        setCompetitorsLessRanked(_.groupBy(oldLessRanked, 'name'))
    }

    if (!(xSpec && ySpec && localModels && segmentDescriptions)) return <div className='loaderWrapper'><Loader /></div>;

    const ySpecValues = ySpec.value
        .filter(y => selectedStateIds[pageValue].includes(`y_${y}`))
        .sort((a, b) => sort(ySpec.name, false, a, b))

    return (
        <div className='Step3'>
            <div id='controls' style={{ display: 'flex' }}>

                <MultipleStateButton
                    name={'X-Y axis'}
                    states={(() => {
                        const states = [
                            ...ySpec.value.map(v => ({id: `y_${v}`, label: v})),
                            ...newProject.params.competitors.map(v => ({id: `b_${v}`, label: v})),
                        ]
                        const i = states.findIndex(s => s.id.startsWith('b_'))
                        states.splice(i, 0, {id: 'brands', label: 'Brands', class: 'disabled'})
                            return [
                                {id: ySpec.name, label: ySpec.name, class: 'disabled'},
                                ...states
                            ]
                    })()}
                    selectedStateIds={selectedStateIds[pageValue]}
                    onChangeSelectedState={ids => setSelectedStateIds({
                        ...selectedStateIds,
                        [pageValue]: [
                            ...specs[1].value
                                .filter(y => ids.includes(`y_${y}`))
                                .map(y => `y_${y}`),
                            ...newProject.params.competitors
                                .filter(b => ids.includes(`b_${b}`))
                                .map(b => `b_${b}`),
                        ]
                    })}
                />

                {[...localModels.reduce((acc, m) => acc.add(m.specs.find(m => m.id === xSpec.id).value), new Set())]
                    .sort((a, b) => sort(xSpec.name, true, a, b))
                    .map(xValue => (
                        <Button
                            label={xValue}
                            key={xValue}
                            onPress={() => setPageValue(xValue)}
                            margined={pageValue === xValue ? 0 : 5}
                            warning={isModelsChanged(localModels.filter(
                                model => model.specs.find(s => s.id === xSpec.id).value === xValue
                            ))}
                        />
                    ))
                }
            </div>

            <table>

                <thead>

                <HeaderRow
                    firstTitle={pageValue}
                    columns={[newProject.params.brand, ...selectedStateIds[pageValue]
                        .filter(s => s.startsWith('b_'))
                        .map(b => b.slice(2))
                    ]}
                />

                </thead>

                <tbody>

                {ySpecValues.map((v, i) => {
                    return (

                        localModels.filter(
                             m => String(m.specs.find(s => s.id === xSpec.id).value) === String(pageValue) &&
                                  String(m.specs.find(s => s.id === ySpec.id).value) === v
                        ).map((model, i, arr) => {

                            return <TableRow
                                key={model.id}
                                title={arr.length === 1 || i === 0 ? v : null}
                                models={[model]}
                                rowspan={arr.length}
                                brand={newProject.params.brand}
                                ySpec={xSpec}
                                columnValues={[newProject.params.brand, ...selectedStateIds[pageValue]
                                    .filter(s => s.startsWith('b_'))
                                    .map(s => s.slice(2))
                                    .sort((a, b) => sort(ySpec.name, true, a, b))
                                ]}
                                modelsForSegment = {(models = [], br, competitors, _i) => {
                                    if (_i === 0) {
                                        return models
                                    }
                                    if (competitors?.length) {
                                        return competitors.filter(c => c.brand === br)
                                    }
                                    return []
                                }}
                                specs={specs}
                                isEditable={i => i === 0 && !disableEdit}
                                showSegmentInfo={false}
                                currency={newProject.params.currency}
                                competitors={competitors.filter(
                                    c => c.specs.find(s => s.id === ySpec.id).value === v &&
                                         c.specs.find(a => a.id === xSpec.id).value === String(pageValue) &&
                                         (_.isEqual(model.specs, c.specs) || c.changedOnRow === i)
                                )}
                                competitorsLessRanked={(competitorsLessRanked || []).filter(c =>
                                    c.specs.find(a => a.id === xSpec.id).value === String(pageValue) &&
                                    c.specs.find(a => a.id === ySpec.id).value === v
                                )}
                                onChangeModel={onChangeModel}
                                changeCompetitor={(fromId, toId) => {
                                    const oldCompetitorIndex = competitors.findIndex(a => a.id === fromId);
                                    const oldLessRankedCmpttrI = competitorsLessRanked
                                        .findIndex(a => a.id === toId);
                                    const oldLessRankedCompetitor = competitorsLessRanked[oldLessRankedCmpttrI];
                                    oldLessRankedCompetitor.changedOnRow = i;

                                    if (fromId) {
                                        setCompetitors([
                                            ...competitors.slice(0, oldCompetitorIndex),
                                            oldLessRankedCompetitor,
                                            ...competitors.slice(oldCompetitorIndex + 1)
                                        ])

                                        setCompetitorsLessRanked([
                                            ...competitorsLessRanked.slice(0, oldLessRankedCmpttrI),
                                            competitors[oldCompetitorIndex],
                                            ...competitorsLessRanked.slice(oldLessRankedCmpttrI + 1)
                                        ])

                                    } else {
                                        setCompetitors([...competitors, oldLessRankedCompetitor])
                                    }

                                }}
                            />

                        })

                    )
                })}

                </tbody>

            </table>

            <BottomButtons>
                <Button
                    label={'CANCEL'}
                    margined={20}
                    disabled={!isEdited(localModels, competitorsIds, competitors, selectedStateIds, selectedStateIdsDefault)}
                    onPress={onCancel}
                    isBottomButton={true} />
                <Button
                    label={'APPLY'}
                    margined={20}
                    disabled={!isEdited(localModels, competitorsIds, competitors, selectedStateIds, selectedStateIdsDefault)}
                    showLoader={isSaving.apply}
                    onPress={() => {
                        setIsSaving({ ...isSaving, apply: true })
                        onApply().then(() => {
                            setIsSaving({ ...isSaving, apply: false })
                        })
                    }}
                    isBottomButton={true}
                />
                <Button
                    label={'NEXT'}
                    margined={20}
                    disabled={isEdited(localModels, competitorsIds, competitors, selectedStateIds, selectedStateIdsDefault)
                        || disableEdit}
                    showLoader={isSaving.next}
                    onPress={() => {
                        setIsSaving({ ...isSaving, next: true })
                        Promise.resolve(!disableEdit ? onApply() : true).then(() => {
                            onNextStep()
                        })
                    }}
                    isBottomButton={true}
                />
            </BottomButtons>
        </div>
    );
}

const isModelsChanged = (models) => {
    return _.some(models, model => !_.isEqual(model.price, model.changedPrice));
}

export const isEdited = (models, competitorsIds, competitors, selectedStateIds, selectedStateIdsDefault) => {
    return models.some(m => m.price !== m.changedPrice) ||
           models.some(m => m.msrp !== m.changedMsrp) ||
           competitors.some(c => !competitorsIds.includes(c.id)) ||
           JSON.stringify(selectedStateIds) !== JSON.stringify(selectedStateIdsDefault)
}

export default Step3;
