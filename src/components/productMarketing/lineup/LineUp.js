import React from 'react';
import './LineUp.css'
import EntityList from './projectList/EntityList';
import Project from './project/Project';
import { Switch, Route } from 'react-router-dom';

function LineUp({location, isArchived = false}) {
    return (
        <section className='LineUp' style={{flex: '1'}}>
            {(location.pathname === '/product-management/line-up-creator' || location.pathname === '/archive') && (
                <EntityList type='lineup' isArchived={isArchived} />
            )}
            <Switch>
                <Route path='/product-management/line-up-creator/:projectId' component={Project} />
                <Route path='/archive/:projectId'                            component={Project} />
            </Switch>
        </section>
    )
}

export default LineUp;
