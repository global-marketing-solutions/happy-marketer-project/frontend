import React, { useState, useEffect, useContext } from 'react';

import './EntityList.css'

import { prod } from '../../../../config.json';

import Loader from '../../../EmptyScreen/Loader';
import { Button, Inputs, Popups } from '../../../common';
import { getHeaders } from '../../../../actions/headers';
import AuthContext from '../../../../contexts/auth-context';

import { Link } from 'react-router-dom';

const defaultNewEntity = { Name: '', DueDate: '', StartDate: '' };

function EntityList({ isArchived, type: _type }) {
    const [trigger, triggerLoad] = useState(true);
    const [type, setType] = useState(_type);

    const [isCreatingActive, setIsCreatingActive] = useState(false);
    const [entities, setEntities] = useState([]);
    const [isTriedToGet, setIsTriedToGet] = useState();

    const [isShowAsk, setIsShowAsk] = useState();
    const [isShowPopup, setIsShowPopup] = useState();

    const [shrd, setShrd] = useState({});
    const [canShare, setCanShare] = useState(true);
    const [updateOwnerName, setUpdateOwnerName] = useState();
    const [entityName, setEntityName] = useState('');
    const [askHeader, setAskHeader] = useState('');
    const [askBody, setAskBody] = useState('');
    const [askButton, setAskButton] = useState('');
    const [askCallback, setAskCallback] = useState(_ => { });
    const [entityCreationError, setEntityCreationError] = useState('');

    const auth = useContext(AuthContext);

    useEffect(_ => {
        const url = `${type === 'lineup' ? 'projects' : 'documents'}${isArchived ? '?status=Archived' : ''}`;
        getHeaders().then(headers => fetch(prod.stage + url, { headers })
            .then(resp => resp.json())
            .then(resp => {
                setEntities(resp)
                setIsTriedToGet(true)
            }))
    }, [type, isArchived, trigger]);

    if (!isTriedToGet) return <div className='loaderWrapper'><Loader /></div>;

    return (
        <div className='EntityList'>

            <div style={{display: 'flex'}}>

            {isArchived && (<><Button
                    label={'Line Up Creator'}
                    onPress={_ => {
                        setType('lineup')
                    }}
                    margined={type === 'lineup' ? 0 : 5}
                />

                <Button
                    label={'Tools'}
                    onPress={_ => {
                        setType('tools')
                    }}
                    margined={type === 'tools' ? 0 : 5}
                /></>)}

            </div>

            <div className='header'>
                <h1>{isArchived ? '' : type === 'lineup' ? 'Line Up Creator' : 'Tools'}</h1>
            </div>
            <div className='container'>
                <h1>{isArchived ? `Archived ${type === 'lineup' ? 'Line Up Creator' : 'Tools'}`
                    : type === 'lineup' ? 'Current projects' : 'Current documents'
                }</h1>
                <table>
                    <thead>
                        <tr>
                            <th className='dynamic-width'>{type === 'lineup' ? 'Project name' : 'Document name'}</th>

                            {
                                type === 'tools' && <th className='static-width'>Tool</th>
                            }

                            <th className='static-width'>Owner</th>
                            <th className='static-width'>Start Date</th>
                            <th className='static-width'>Due Date</th>
                            {type === 'lineup' && !isArchived && <th className='static-width'>Status</th>}
                            <th className='static-width'>Operations</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        (entities || []).map((entity, i) => {
                            const isView = ((entity.Shrd || {})[auth.user.login] || {}).mode === 'view';
                            const isEdit = ((entity.Shrd || {})[auth.user.login] || {}).mode === 'edit';

                            return <EntityLine
                                key={`proj${i}`}
                                isArchived={isArchived}
                                type={type}
                                entity={entity}
                                isView={isView}
                                isEdit={isEdit}
                                onShare={setEntity => {
                                    setEntityName(entity.Name);
                                    setCanShare(!isView && !isEdit);

                                    setIsShowPopup(true);
                                    setShrd(entity.Shrd);

                                    setUpdateOwnerName(_ => ownerName => {
                                        updateEntityOnView(entities, {
                                            ...entity,
                                            OwnerName: ownerName
                                        }, i, setEntities);
                                        setEntity(entity);
                                    });
                                }}
                                onArchive={_ => {
                                    setAskHeader(`This action will move "${entity.Name}" to the archive`)
                                    setAskBody("This wouldn't delete entity and you can access it later on in Archive at any time.")
                                    setAskButton('ARCHIVE')
                                    setIsShowAsk(true)
                                    setAskCallback(_ => _ => {
                                        deleteEntityOnView(entities, i, setEntities);
                                        archiveEntityOnBackend(type, entity);
                                    });
                                }}
                                onRestore={() => {
                                    setAskHeader(`Do you want to restore the "${entity.Name}"?`)
                                    setAskBody('After clicking on “RESTORE” entity will be moved from archive to the list of the projects.')
                                    setAskButton('RESTORE')
                                    setIsShowAsk(true)
                                    setAskCallback(_ => _ => {
                                        deleteEntityOnView(entities, i, setEntities);
                                        restoreProjectOnBackend(type, entity.Name, entity.OwnerUsername);
                                    });
                                }}
                                onDelete={_ => {
                                    setAskHeader(`You are trying to delete "${entity.Name}"`);
                                    setAskBody('This action is permanent and cannot be undone in future. Are you sure you want to delete project?')
                                    setAskButton('DELETE')
                                    setIsShowAsk(true)
                                    setAskCallback(_ => _ => {
                                        const email = auth.user.email;
                                        const username = entity.OwnerUsername;
                                        deleteEntityOnView(entities, i, setEntities);
                                        deleteEntityOnBackend(
                                            type,
                                            entity.Name,
                                            email.startsWith(username + '@') ? '' : username
                                        );
                                    });
                                }}
                            />
                        })
                    }
                    {isCreatingActive && (<EntityLine
                        type={type}
                        entityCreationError={entityCreationError}
                        onStart={ent => createEntity(
                            ent,
                            type,
                            setEntityCreationError,
                            _ => {
                                triggerLoad(v => !v)
                                setIsCreatingActive(false)
                            }
                        )}
                    />)}
                </tbody>
                </table>

                {!isArchived && (<Button
                    onPress={_ => setIsCreatingActive(!isCreatingActive)}
                    label={type === 'lineup' ? 'CREATE NEW PROJECT' : 'CREATE NEW DOCUMENT'}
                    margined={20}
                />)}
            </div>

            {isShowAsk &&<Popups.Ask
                header={askHeader}
                body={askBody}
                button={askButton}
                callback={askCallback}
                setIsShowAsk={setIsShowAsk}
            />}

            {isShowPopup && <Popups.Share
                type={type}
                entityName={entityName}
                setEntities={setEntities}
                shrd={shrd}

                canShare={canShare}

                setIsShowPopup={setIsShowPopup}

                setAskHeader={setAskHeader}
                setAskButton={setAskButton}
                setAskCallback={setAskCallback}
                setIsShowAsk={setIsShowAsk}

                updateOwnerName={updateOwnerName}
            />}

        </div>
    )
}

function EntityLine({ isArchived, type, entityCreationError, entity, isView, isEdit, onStart, onShare, onArchive, onRestore, onDelete }) {
    const [mode, setMode] = useState(entity ? 'view' : 'edit');
    const [ent, setEntity] = useState(entity || defaultNewEntity)
    useEffect(_ => setEntity(entity || defaultNewEntity), [entity])

    return (
        <tr>
            <td className='dynamic-width'>
                {
                    mode === 'view' ? (
                        <Link to={
                            isArchived ? '/archive/' :
                                type === 'lineup' ? `/product-management/line-up-creator/${entity.Name}/step1` :
                                `/product-management/tools/${entity.Name}`
                        }>
                            {ent.Name}
                        </Link>
                    ) : (
                            <Inputs.Input
                                value={ent.Name}
                                onChange={v => setEntity({ ...ent, Name: v.replace(/[\\/]/, '') })}
                                placeholder={'name'}
                                error={entityCreationError}
                            />
                        )
                }
            </td>

            {
                type === 'tools' && <td className='static-width'>Line up map</td>
            }

            <td className='static-width'>{
                ent.OwnerName
            }</td>

            <td className='static-width'>{
                mode === 'view' ? ent.StartDate : (
                    <Inputs.Input
                        type='date'
                        value={ent.StartDate}
                        onChange={v => setEntity({ ...ent, StartDate: v })}
                    />
                )
            }</td>
            <td className='static-width'>{
                mode === 'view' ? ent.DueDate : (
                    <Inputs.Input
                        type='date'
                        value={ent.DueDate}
                        onChange={v => setEntity({ ...ent, DueDate: v })}
                    />
                )
            }</td>

            { type === 'lineup' && !isArchived && <td className='static-width'>{entity?.Status}</td> }

            <td className='static-width actions'>
                {entity ? (
                    mode === 'view' ? (<>
                        <div title='Delete' className='action-icon action-icon-remove' onClick={onDelete}></div>
                        <div
                            title={isView ? 'Edit: blocked for view mode sharee' : 'Edit'}
                            className={`action-icon action-icon-edit ${isView && 'disabled'}`}
                            onClick={isView ? _ => {} : _ => setMode('edit')}>
                        </div>
                        {isArchived
                            ? (<div title={isView ? 'Restore: blocked for view mode sharee' : 'Restore'}
                                className={`action-icon action-icon-restore ${isView && 'disabled'}`}
                                onClick={isView ? _ => {} : onRestore}></div>)
                            : (<div title={isView ? 'Archive: blocked for view mode sharee' : 'Archive'}
                                className={`action-icon action-icon-archive ${isView && 'disabled'}`}
                                onClick={isView ? _ => {} : onArchive}></div>)
                        }
                        <div
                            title='Share'
                            className='action-icon action-icon-share'
                            onClick={_ => onShare(setEntity)}
                        ></div>
                    </>) : (<>
                        <i className='action-green' onClick={_ => {
                            updateEntity(type, entity.Name, ent)
                            setMode('view');
                        }} >done</i>
                        <i className='action-red' onClick={_ => {
                            setEntity(entity || defaultNewEntity);
                            setMode('view');
                        }} >clear</i>
                    </>)
                ) :
                    <Button
                        onPress={_ => onStart(ent)}
                        label={'START'}
                    />
                }

            </td>
        </tr>
    )
}

async function createEntity(entity, type, setEntityCreationError, callback) {
    const res = await fetch(`${prod.stage}${type === 'lineup' ? 'projects' : 'documents'}`, {
        'method': 'POST',
        'headers': {
            ...(await getHeaders()),
            'Content-Type': 'application/json'
        }, body: JSON.stringify(entity)
    });
    const txt = await res.text();
    if (txt.includes('ConditionalCheckFailedException')) {
        setEntityCreationError('Project with that name already exists.');
        return;
    }
    setEntityCreationError('');
    callback && callback()
}

async function updateEntity(type, nameOriginal, entity) {
    await fetch(`${prod.stage}${type === 'lineup' ? 'projects' : 'documents'}/${nameOriginal}`, {
        'method': 'PATCH',
        'headers': await getHeaders(),
        body: JSON.stringify({
            ...nameOriginal === entity.Name ? {} : { NameNew: entity.Name },
            'StartDate': entity['StartDate'],
            'DueDate': entity['DueDate'],
            'OwnerUsername': entity['OwnerUsername']
        })
    });
}


function updateEntityOnView(entities, newProject, i, setEntities) {
    setEntities([
        ...entities.slice(0, i),
        newProject,
        ...entities.slice(i + 1)
    ])
}

function deleteEntityOnView(entities, i, setEntities) {
    setEntities([
        ...entities.slice(0, i),
        ...entities.slice(i + 1)
    ])
}

async function archiveEntityOnBackend(type, { Name, OwnerUsername, Status }) {
    const url = `${prod.stage}${type === 'lineup' ? 'projects' : 'documents'}/${Name}/status`
    const params =`?owner=${OwnerUsername}&status=Archived&ex=${Status || 'In Progress'}`
    await fetch(url + params, {
        'method': 'PATCH',
        'headers': await getHeaders()
    });
}

async function restoreProjectOnBackend(type, id, owner) {
    await fetch(`${prod.stage}${type === 'lineup' ? 'projects' : 'documents'}/${id}/status?owner=${owner}&status=Restore`, {
        'method': 'PATCH',
        'headers': await getHeaders(),
    });
}

async function deleteEntityOnBackend(type, name, ownerUsername) {
    await fetch(`${prod.stage}${type === 'lineup' ? 'projects' : 'documents'}/${name}?ownerUsername=${ownerUsername}`, {
        'method': 'DELETE',
        'headers': await getHeaders(),
    });
}

export default EntityList;
