import LineUp from './productMarketing/lineup/LineUp';
import Tools from './productMarketing/tools/Tools';
import Profile from './settings/Profile';
import Settings from './settings/Settings';

export default [{
    id: 'index',
    link: '//',
    redirect: '/product-management/line-up-creator',
    label: '',
    skip: true
}, {
    id: 'profile',
    link: '/profile',
    label: '',
    component: Profile,
    skip: true,
    disabled: false
}, {
    id: 'home',
    link: '/home',
    label: 'Home',
}, {
    id: 'academy',
    link: '/academy',
    label: 'Academy',
    disabled: true,
    skip: true
}, {
    id: 'dashboards',
    link: '/dashboards',
    label: 'Dashboards',
    disabled: true,
    skip: true
}, {
    id: 'product',
    label: 'Product Management',
    link: '/product-management',
    redirect: '/product-management/line-up-creator',
    innerItems: [{
        id: 'trend-analyser',
        label: 'Trend Analyser',
        link: '/trend-analyser',
        disabled: true
    }, {
        id: 'line-up-creator',
        label: 'Line Up Creator',
        link: '/line-up-creator',
        component: LineUp
    }, {
        id: 'tracking',
        label: 'Tracking',
        link: '/tracking',
        disabled: true
    }, {
        id: 'tools',
        label: 'Tools',
        link: '/tools',
        component: Tools
    }, {
        id: 'assessment',
        label: 'Assessment',
        link: '/assessment',
        disabled: true
    }]
}, {
    id: 'trade-marketing',
    link: '/trade-marketing',
    label: 'Trade Marketing',
    disabled: true,
    skip: true
}, {
    id: 'brand-and-communication',
    link: '/brand-and-communication',
    label: 'Brand and communication',
    disabled: true,
    skip: true
}, {
    id: 'digital-marketing',
    link: '/digital-marketing',
    label: 'Digital Marketing',
    disabled: true,
    skip: true
}, {
    id: 'trade-partner-management',
    link: '/trade-partner-management',
    label: 'Trade Partner Management',
    disabled: true,
    skip: true
}, {
    id: 'strategy',
    link: '/strategy',
    label: 'Strategy',
    disabled: true,
    skip: true
}, {
    id: 'settings',
    link: '/settings',
    label: 'Settings',
    component: Settings
}, {
    id: 'archive',
    link: '/archive',
    label: 'Archive',
    component: LineUp,
    props: { isArchived: true }
}]
