import React from 'react';
import './EmptyScreen.css';

function EmptyScreen() {
    return (
        <div className='EmptyScreen'>
            <div className='image'></div>
        </div>
    )
}

export default EmptyScreen;