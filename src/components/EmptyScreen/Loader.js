import React from 'react';
import './Loader.css';

function Loader({ size = 50 }) {
    return (
        <div className='loader' style={{width: `${size}px`, height: `${size}px`}}></div>
    )
}

export default Loader;