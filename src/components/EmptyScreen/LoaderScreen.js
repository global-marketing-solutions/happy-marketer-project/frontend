import React from 'react';
import './EmptyScreen.css';
import Loader from './Loader';

function EmptyScreen() {
    return (
        <div className='EmptyScreen LoaderScreen'>
            <Loader size={40} />
        </div>
    )
}

export default EmptyScreen;