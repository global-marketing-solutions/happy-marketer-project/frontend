export const getCurrencySymbol = (currency: string): string => {
    switch (currency.toLowerCase()) {
        case 'eur':
            return '€'
        case 'rub':
            return '₽'
        default:
            return '$'
    }
}

export const formatNumber = (n: number, precision = 2) => {
    return !precision ? Math.round(n) : (Math.round(n * Math.pow(10, precision)) / Math.pow(10, precision));
}

export const formatPriceNumber  = (n: number, precision = 2): string => {
    const str = `${formatNumber(n, precision)}`.split('.');
    const int_part = n < 0 ? str[0].slice(1) : str[0];
    let i = 1; let newStr = `${int_part.slice(-3)}`;
    while (i * 3 < int_part.length) {
        newStr = `${int_part.slice(-3 * (i + 1), -3 * i)},${newStr}`;
        i++;
    }
    return `${n < 0 ? '-' : ''}${newStr}${str[1] ? `.${str[1]}` : ''}`;
}

export default function formatPrice(price: number, currency = 'eur', precision = 2) {
    return `${getCurrencySymbol(currency)}${formatNumber(price, precision)}`;
}
