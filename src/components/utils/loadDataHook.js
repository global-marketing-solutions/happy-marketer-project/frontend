import { useEffect, useState } from 'react';
import { getHeaders } from '../../actions/headers';

const useLoadDataHook = (loadDataFunc, ...additionalTriggers) => {
    const [data, setData] = useState({});
    const [trigger, triggerLoad] = useState(true);
    const [isLoading, setIsLoading] = useState(false);
    const [isError, setIsError] = useState(false);

    useEffect(() => {
        const fetchData = async () => {
            setIsError(false);
            setIsLoading(true);
            try {
                const data = await loadDataFunc(await getHeaders());
                setData(data);
            } catch (error) {
                setIsError(true);
            }
            setIsLoading(false);
        };
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [trigger, ...additionalTriggers]);
    return [{ data, isLoading, isError }, () => triggerLoad(!trigger)];
}

export default useLoadDataHook