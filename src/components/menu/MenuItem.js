import React from 'react';
import './MenuItem.css';
import { useLocation, Link } from "react-router-dom";

function MenuInnerItemList({ innerItems = [], parentPath = '' }) {
    let location = useLocation();
    return (
        <div className={`
            subitem-list
            ${location.pathname.startsWith(parentPath) && (!!innerItems.length) ? 'selected' : ''}
        `}>
            {
                innerItems.map((item) => {
                    const innerPath = `${parentPath}${item.link || ''}`
                    const isSelected = location.pathname.startsWith(innerPath)
                    return (
                        <Link
                            to={innerPath}
                            key={item.id}
                            className={`${item.disabled ? 'disabled' : ''}`}
                        >
                            <div className={`subitem ${isSelected ? 'selected' : ''}`}>{item.label}</div>
                        </Link>
                    )
                })
            }
        </div>
    )
}

function MenuItem({ menuItem, menuHidden = false }) {
    let location = useLocation();
    return menuItem && (
        <div className={
            `menu-item
            ${location.pathname.startsWith(menuItem.link) ? 'selected' : ''}
            ${menuItem.disabled ? 'disabled' : ''}
        `}>
            <Link to={menuItem.link || '/'}>
                <div className="item-header">
                    <div className={`item-icon menu-icon menu-icon-${menuItem.id}`}></div>
                    {!menuHidden && <span className="item-label">{menuItem.label}</span>}
                </div>
            </Link>
            {!menuHidden && <MenuInnerItemList
                parentPath={menuItem.link}
                innerItems={menuItem.innerItems}
            />}

        </div>

    );
}

export default MenuItem

//<div
//className='item-icon'
//style={{backgroundImage: `url(../../${menuItem.icon})`}}
//></div>
