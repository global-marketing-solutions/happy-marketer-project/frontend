import React, { useState } from 'react';
import './Menu.css';
import MenuItem from './MenuItem';
import menuItems from "../menuItems";

function Menu() {
    let [isCollapsed, setIsCollapsed] = useState(localStorage.getItem('isCollapsed') === 'true');
    return (
        <div className={`Menu ${isCollapsed ? 'isCollapsed' : ''}`}>
            <div className={`wrapper`} >
                <div style={{ marginBottom: isCollapsed ? 150 : 20, alignSelf: 'center' }}>
                    {!isCollapsed && (<div className='logo'></div>)}
                </div>
                {menuItems.filter(a => !a.skip).map((item) =>
                    <MenuItem
                        key={item.id}
                        menuItem={item}
                        menuHidden={isCollapsed}
                    />)
                }
            </div>
            <CollapseToggle
                isCollapsed={isCollapsed}
                setIsCollapsed={setIsCollapsed}
            />
        </div>
    )
}

function CollapseToggle({ isCollapsed, setIsCollapsed }) {
    return (
        <div
            style={{ alignSelf: isCollapsed ? 'center' : 'flex-end' }}
            onClick={() => {
                setIsCollapsed(!isCollapsed)
                localStorage.setItem('isCollapsed', !isCollapsed)
            }}
        >
            {isCollapsed ? 'keyboard_arrow_right' : 'keyboard_arrow_left'}
        </div>
    )
}

export default Menu;
