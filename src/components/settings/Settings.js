import React, {useEffect, useState} from 'react'

import './Settings.css'

import { prod } from '../../config.json';
import { getHeaders } from '../../actions/headers'


export default function() {

    const [data, setData] = useState([])

    useEffect(_ => {
        getData()
            .then(resp => resp.json())
            .then(resp => setData(resp))
    }, [])

    return (
        <div id='Settings'>

            <div className='header'>
                <h1>SETTINGS</h1>
            </div>

            <div className='container'>

                <div id='buttons'>
                    <button className='active'>SPECIAL FUNCTIONS DATABASE</button>
                    <button>WEBSITES DATABASE</button>
                </div>

                <table>
                    <thead>
                        <tr>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Special function</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        Object.entries(data).map(([c, brands]) =>
                            Object.entries(brands).map(([b, funcs]) =>
                                funcs.map(f => <tr key={c + b + f}><td>{c}</td><td>{b}</td><td>{f}</td></tr>)
                            )
                        )
                    }
                    </tbody>
                </table>

                <a href='mailto:support@gms-ai.com?subject=[ADD REQUEST]'>ADD REQUEST</a>

            </div>

        </div>

    )
}

async function getData() {
    return fetch(`${prod.stage}specialFunctions`, {
        'headers': await getHeaders(),
    })
}
