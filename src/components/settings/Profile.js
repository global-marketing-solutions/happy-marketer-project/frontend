import React, { useContext, useState } from 'react';

import './Profile.css'

import { prod } from '../../config.json';
import { getHeaders } from '../../actions/headers'

import Loader from '../EmptyScreen/Loader';
import AuthContext from '../../contexts/auth-context';

function Profile() {
    const auth = useContext(AuthContext);
    const [imageLoading, setImageLoading] = useState(false);

    const email = auth.user.email;
    const name = auth.user.name;
    const mediaLink = auth.user.pictureUrl;
    return (
        <div className='Profile'>
            <div className='header'>
                <h1>Profile</h1>
            </div>
            <div className='container'>
                <div className='section'>
                    <div className='title'>
                        <h2>Who</h2>
                    </div>
                    <div className='content' style={{ flex: '0.6 1' }}>
                        <div className='item'>
                            <label>Email: </label>
                            <input type='text' disabled value={email} />
                        </div>
                        <div className='item'>
                            <label>Name: </label>
                            <input type='text' disabled value={name} />
                        </div>
                    </div>
                </div>
                <div className='section'>
                    <div className='title'>
                        <h2>Picture</h2>
                    </div>
                    <div className='content'>
                        <div className='mediaItemWrapper'>
                            {imageLoading ? <div className='loaderWrapper'><Loader /></div> : <></>}
                            <div className='mediaItem' style={{ backgroundImage: `url(${mediaLink})` }}></div>
                        </div>
                        <div className='item'>
                            <label style={{ marginBottom: '7px' }}>Upload new picture: </label>
                            <input
                                type='file'
                                accept='image/*'
                                onChange={e => {
                                    setImageLoading(true);
                                    avatarUploadOnchange(e, () => {
                                        auth.updateUserPicture();
                                        setImageLoading(false);
                                    })
                                }}
                            />
                        </div>
                        <label style={{ color: '#999', fontSize: '12px' }}>Use only jpg, png or webp formats</label>
                    </div>
                </div>
            </div>
        </div>
    )
}

async function avatarUploadOnchange(e, callback) {
    e.persist();

    await fetch(`${prod.stage}avatar`, {
        'method': 'PUT',
        'headers': await getHeaders(),
        'body': e.target.files[0]
    });

    callback && callback();
}

export default Profile;
