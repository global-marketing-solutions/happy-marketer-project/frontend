import React from 'react';
import _ from 'lodash';
import menuItems from './menuItems';
import EmptyScreen from './EmptyScreen/EmptyScreen';
import { Redirect, Route, Switch } from 'react-router-dom';

function getRoute({link, component, redirect, props = {}}, index) {
    if (!link) return;
    if (redirect) {
        return (<Redirect key={link + index} exact from={link} to={redirect} />)
    }
    const Comp = component
    return (<Route
        key={link + index}
        path={link}
        {...(component ? {
            render: (routeProps) => (<Comp {...routeProps} {...props} />)
        } : {
            component: EmptyScreen
        })}
    />)
}

function AppSwitch() {
    let allRoutes = menuItems.reduce((acc, item) => {
        let newAcc = [...acc, _.pick(item, ['link', 'component', 'redirect', 'props'])];
        if (item.innerItems) {
            return [
                ...newAcc,
                ...item.innerItems.map(iitem => ({
                    ..._.pick(iitem, ['component', 'redirect']),
                    link: item.link + _.get(iitem, 'link')
                })),
                ...item.innerItems.reduce((acc, innerRoute) => {
                    return [
                        ...acc,
                        (innerRoute.children || []).map(iitem => ({
                            ..._.pick(iitem, ['component', 'redirect']),
                            link: item.link + _.get(iitem, 'link')
                        }))
                    ]
                }, [])
            ];
        }
        return newAcc;
    }, []);
    return (
        <Switch>
            {allRoutes.map(getRoute).filter(a => !!a)}
        </Switch>
    )
}

export default AppSwitch;
