import React, { useEffect, useState, useContext } from 'react';
import logout from '../../images/icons/logout-24px.svg';
import { Inputs } from '../common';
import { Link } from 'react-router-dom';
import './Header.css';
import AuthContext from '../../contexts/auth-context';
import { prod } from '../../config.json'

function Zoomer({ value, onChange }) {
    return <div className='zoom-range'><Inputs.SliderSelect
        min={40}
        max={120}
        value={value}
        onChange={onChange}
    /></div>;
}

//<div id='search' className='search'>
//  <input type='search' placeholder='Search' />
//</div>

function Header() {
    const [zoom, setZoom] = useState(100);
    useEffect(() => {
        zoomOnChange(Math.floor(zoom))
    }, [zoom]);
    const auth = useContext(AuthContext);

    const name = auth.user.name;
    const mediaLink = auth.user.pictureUrl;

    return (
        <header className='TopHeader'>
            <Zoomer value={zoom} onChange={setZoom} />
            <a
                className='header-print-button'
                onClick={e => {
                    window.print();
                    e.preventDefault();
                }}
                href='/#'
            >
                <div className='header-icon-print'></div>
                <p>Print &amp; Download</p>
            </a>

            <nav className='user'>
                <Link to={`/profile`}>
                    {mediaLink ? (
                        <div className='avatarImage' style={{backgroundImage: `url(${mediaLink})`}}></div>
                    ) : (
                            <i className='avatar'>account_circle</i>
                        )}
                    <span className='name'>{name}</span>
                </Link>
            </nav>

            <nav id='logout' onClick={() => {
                localStorage.clear()
                const p = `client_id=${prod.client_id}&response_type=code&redirect_uri=${prod.redirect_uri}`
                window.location.href = `https://${prod.cognitoHost}/logout?${p}`
            }}>
                <img
                    src={logout}
                    title='logout'
                    alt='logout'/>
            </nav>

        </header>
    )
}

function zoomOnChange(zoom) {
    document.body.style.setProperty('--zoom', zoom);
}

export default Header;
