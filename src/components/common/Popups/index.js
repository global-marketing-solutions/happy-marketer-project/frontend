import Ask from './Ask';
import Notification from './Notification';
import Share from './Share';
import Close from './Close';

export default {
    Ask,
    Notification,
    Share,
    Close
}
