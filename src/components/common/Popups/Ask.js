import React from 'react';
import './common.css';

function Ask({ header, body, button, callback, setIsShowAsk, cancelCallback }) {
    return (
        <div>

            <div id='popupAsk'>
                <h3>{header}</h3>
                <p>{body}</p>
                <div className='buttons'>
                    <button onClick={() => {
                        setIsShowAsk(false);
                        callback && callback();
                    }}>{button}</button>
                    <button onClick={() => {
                        setIsShowAsk(false);
                        cancelCallback && cancelCallback();
                    }}>CANCEL</button>
                </div>
            </div>

            <p id='dim'></p>

        </div>
    )
}

export default Ask;
