import React, { useState, useEffect, useContext } from 'react';

import './common.css';

import { prod } from '../../../config.json';

import Loader from '../../EmptyScreen/Loader';
import { Popups } from '../';
import { getHeaders } from '../../../actions/headers';
import AuthContext from '../../../contexts/auth-context';

function Share({
    type,
    entityName,
    setEntities,
    shrd,

    canShare,

    setIsShowPopup,

    setIsShowAsk,
    setAskHeader,
    setAskButton,
    setAskCallback,

    updateOwnerName
}) {
    const [username, setUsername] = useState('');
    const [mode, setMode] = useState('view');

    const [notificationText, setNotificationText] = useState('');
    const [isShowNotification, setIsShowNotification] = useState();

    const [shrdView, setShrdView] = useState([]);
    const [shrdEdit, setShrdEdit] = useState([]);

    const [isLoader, setIsLoader] = useState(false);

    const auth = useContext(AuthContext);

    const unshareReact = username => {
        setEntities(entities => {
            delete entities.find(sh => sh.Name === entityName).Shrd[username]
            return entities
        })
    }

    const shareeWrapper = (username, humanName) => {
        return <ShareeElement
            key={username}

            entityName={entityName}
            username={username}
            humanName={humanName}
            isRemovable={canShare}

            setAskHeader={setAskHeader}
            setAskButton={setAskButton}
            setIsShowAsk={setIsShowAsk}
            setAskCallback={setAskCallback}
            setIsShowPopup={setIsShowPopup}

            unshareReact={_ => unshareReact(username)}
        />
    }

    useEffect(() => {
        const allUsers = Object.entries(shrd || []);
        setShrdView(allUsers.filter(comparator('view')));
        setShrdEdit(allUsers.filter(comparator('edit')));
    }, [shrd])

    const isView = (shrdView || []).length > 0;
    const isEdit = (shrdEdit || []).length > 0;

    const mp = mode => ([username, {humanName}]) => {
        return shareeWrapper(
            username,
            humanName
        )
    }

    return (
        <div id='shareWrapper'>
            <div id='share'>

                <Popups.Close setIsShowPopup={setIsShowPopup}/>

                <h3>{canShare ? `Do you want to share project '${entityName}'?` :
                    'Sharing function is available only for project owners'}</h3>

                {
                    (isView || isEdit) && <h4>Already shared with:</h4>
                }

                {
                    isView && <span style={{fontWeight: 'bold'}}>View: </span>
                }
                {
                    isView && shrdView.map(mp('view'))
                }
                {
                    isView && <div><br /></div>
                }
                {
                    isEdit && <span style={{fontWeight: 'bold'}}>Edit: </span>
                }
                {
                    shrdEdit.map(mp('edit'))
                }

                {canShare && (<>
                <p>Please enter username, select sharing type and click 'Share'.</p>
                <p>
                    <input
                        value={username}
                        onChange={e => setUsername(e.target.value)}
                        placeholder='username'
                    />{'@' + auth.user.domain}
                </p>

                <input
                    type='radio'
                    id='view'
                    name='access'
                    onChange={e => setMode(e.target.id)}
                    checked={mode === 'view'}
                />
                <label htmlFor='view'>View</label>

                <input
                    type='radio'
                    id='edit'
                    name='access'
                    onChange={e => setMode(e.target.id)}
                    checked={mode === 'edit'}
                />
                <label htmlFor='edit'>Edit</label>

                <input
                    type='radio'
                    id='admin'
                    name='access'
                    onChange={e => setMode(e.target.id)}
                    checked={mode === 'admin'}
                />
                <label htmlFor='admin'>Admin</label>

                <div className='buttons'>
                    <button onClick={() => {
                        setIsShowPopup(false);
                        const email = username + '@' + auth.user.domain;
                        const cbOk = humanName => {
                            setNotificationText(`Shared to ${email} with ${mode} mode`);
                            setIsShowNotification(true);
                            if (mode === 'view' || mode === 'edit') {
                                setEntities(entities => {

                                    entities.find(ent => ent.Name === entityName)
                                        .Shrd = {
                                            ...shrd,
                                            ...{[username]: {humanName, mode}}
                                        }
                                    return entities

                                })
                            } else if (mode === 'admin') {
                                updateOwnerName(humanName);
                            }
                            setUsername('');
                        }
                        const cbFail = () => alert(`Email ${email} is not found in your organization :(`);
                        share(
                            type,
                            entityName,
                            {[username.toLowerCase()]: mode},
                            (!!(shrdView || shrdEdit || []).length),
                            cbOk,
                            cbFail,
                            setIsLoader
                        );
                    }}>SHARE</button>
                    <button onClick={() => setIsShowPopup(false)}>CANCEL</button>
                </div>
                </>)}
            </div>

            <p id='dim'></p>

            {isShowNotification && <Popups.Notification
                text={notificationText}
                setIsShowNotification={setIsShowNotification}
            />}

            {isLoader && <div className='loaderWrapper'><Loader /></div>}

        </div>
    )
}

function comparator(mode) {
    return function(shrd) {
        return mode === shrd[1]['mode']
    }
}

function ShareeElement({
    entityName,
    username,
    humanName,

    isRemovable,

    setIsShowAsk,
    setAskHeader,
    setAskButton,
    setAskCallback,
    setIsShowPopup,

    unshareReact
}) {
    return (<span style={{ margin: '5px'}}>
        <span style={{whiteSpace: 'nowrap'}}>{humanName}</span> {isRemovable && <img
            src='../../../images/icons/remove.svg'
            style={{
                width: '20px',
                margin: '0px 10px -4px 0px',
                cursor: 'pointer'
            }}
            title='Take away access'
            onClick={e => {
                    setIsShowPopup(false);
                    setAskHeader(`Are you sure you want to unshare from ${humanName}?`);
                    setAskButton('UNSHARE');
                    setIsShowAsk(true);
                    setAskCallback(() => () => {
                        unshareReact();
                        unshare(entityName, username);
                    })
            }}
            alt='' />}
    </span>)
}

async function share(type, entityName, shrd, isShrd, cbOk, cbFail, setIsLoader) {
    setIsLoader(true);

    const resp = await fetch(`${prod.stage}${type === 'lineup' ? 'projects' : 'documents'}/${entityName}/share`, {
        'method': 'PATCH',
        'headers': await getHeaders(),
        'body': JSON.stringify({shrd, isShrd})
    });
    if (resp.ok) {
        const body = await resp.text();
        cbOk(body);
    } else if (resp.status === 400) {
        cbFail();
    } else {
        alert('ERROR, please try later :(');
    }

    setIsLoader(false);
}

async function unshare(entityName, username) {
    await fetch(`${prod.stage}projects/${entityName}/unshare?username=${username}`, {
        'method': 'PATCH',
        'headers': await getHeaders()
    })
}

export default Share;
