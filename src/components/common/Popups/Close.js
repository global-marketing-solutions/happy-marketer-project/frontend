import React from 'react';

import './Close.css'

function Close({ setIsShowPopup }) {
    return <i
        className='Close'
        style={{
            position: 'absolute',
            fontFamily: 'Material Icons',
            fontSize: '25px',
            fontStyle: 'normal',
            top: 0,
            right: 0,
            width: '20px',
            margin: '0px 10px -4px 0px',
            cursor: 'pointer'
        }}
        title='Close'
        onClick={_ => setIsShowPopup(false)}
        alt=''
    >close</i>
}

export default Close
