import React from 'react';
import './common.css';

function Notification({ text, setIsShowNotification }) {
    return (
        <div>

            <div id='popup'>
                <p style={{fontSize: '20px', marginBottom: 0}}>{text}</p>
                <i onClick={() => setIsShowNotification(false)}>check</i>
            </div>

            <p id='dim'></p>

        </div>
    )
}

export default Notification;
