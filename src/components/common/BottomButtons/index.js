import React from 'react';
import './BottomButtons.css';

function BottomButtons({ children }) {
    return (
        <div className={'BottomButtons'}>
            <div className='wrapper'>
                {children}
            </div>
        </div>
    )
}

export default BottomButtons;
