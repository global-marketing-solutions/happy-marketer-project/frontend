import React, { useState } from 'react';
import './StateButton.css';

const defaultStates = []

function MultipleStateButton({
    name,
    style,
    width,
    states = defaultStates,
    selectedStateIds = [],
    onChangeSelectedState = () => {}
}) {
    const [opened, setOpened] = useState(false);

    return (
        <div
            className={`StateButton other ${opened ? 'opened' : ''}`}
            {...(width ? {style: {width}} : {})}
            style={style}
            onClick={() => setOpened(!opened)}
        >
            <div className='label'>
                {name}
            </div>
            <div className='dropdown'></div>
            <div className='list' onClick={e => e.stopPropagation()}>
                {states.map(state => {
                    const sel = selectedStateIds || [];
                    return (
                        <div
                            className={`listitem other ${sel.includes(state.id) ? 'selected' : ''} ${state.class}`}
                            key={state.id}
                            onClick={state.class === 'disabled' ? null : () => {
                                const elemIndex = sel.findIndex(s => s === state.id);
                                onChangeSelectedState && onChangeSelectedState(~elemIndex ? [
                                    ...sel.slice(0, elemIndex),
                                    ...sel.slice(elemIndex + 1)

                                ] : [...sel, state.id]);
                            }}
                        >
                            {state.label}
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default MultipleStateButton;
