import React, { useState, useEffect } from 'react';
import './StateButton.css';

const dfl = [{
    id: 'confirm',
    label: 'CONFIRM'
}, {
    id: 'change',
    label: 'CHANGE'
}, {
    id: 'delete',
    label: 'DELETE'
}]

function StateButton({
    name,
    width,
    states = dfl,
    ignore = '',
    selectedStateId = 'change',
    onChangeSelectedState = () => {}
}) {
    const [opened, setOpened] = useState(false);
    const [selectedState, setSelectedState] = useState(states.find(state => state.id === selectedStateId))
    useEffect(() => {
        setSelectedState(states.find(state => state.id === selectedStateId))
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectedStateId])
    const st = (selectedState || states[0]).id;
    const cls = dfl.map(s => s.id).includes(st) ? st : 'other';
    return (
        <div
            className={`StateButton ${cls} ${opened ? 'opened' : ''}`}
            {...(width ? {style: {width}} : {})}
            onClick={() => setOpened(!opened)}
        >
            <div className='label'>
                {name || (selectedState || states[0]).label}
            </div>
            <div className='dropdown'></div>
            <div className='list' onClick={e => e.stopPropagation()}>
                {(ignore ? states.filter(s => s.id !== ignore) : states).map(state => {
                    return (
                        <div
                            className={`listitem ${dfl.map(a => a.id).includes(state.id) ? state.id : 'other'}`}
                            key={state.id}
                            onClick={() => {
                                onChangeSelectedState && onChangeSelectedState(state.id)
                                setSelectedState(state);
                                setOpened(false);
                            }}
                        >
                            {state.label}
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default StateButton;
