import React from 'react';
import './Button.css';
import { Link } from 'react-router-dom';
import Loader from '../../EmptyScreen/Loader';

function linked({ link, component }) {
    return (
        <Link to={link}>
            <component />
        </Link>
    )
}

function Button({ label, onPress, centered, margined, warning, destruction, disabled, isBottomButton = false, showLoader = false }) {
    return (<div
        className={`Button
            ${centered ? 'centered' : ''}
            ${disabled ? 'disabled' : ''}
            ${destruction ? 'destruction' : ''}
            ${warning ? 'warning' : ''}`
        }
        style={margined ? {
            marginTop: isBottomButton ? 0 : (margined || 20),
            marginLeft: margined || 20,
            marginRight: margined || 20,
            marginBottom: isBottomButton ? 0 : (margined || 20),
        } : {}}
    >

        <button
            onClick={disabled ? () => { } : onPress}
        >
            {showLoader ? <Loader size={15} /> : label}
        </button>
    </div>)
}

function ButtonWrapper({ link, label, onPress, ...other }) {
    const button = (<Button
        label={label}
        {...(onPress ? { onPress } : {})}
        {...other}
    />)
    return link ? linked(button) : button
}

export default ButtonWrapper;
