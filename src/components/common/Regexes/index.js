export const Regexes = {
    someText: '.*\\S.*',
}

export const Validators = {
    numberFrom1to99: n => n > 0 && n < 100,
    lt: n => k => k < n,
    lte: n => k => k <= n,
    gt: n => k => k > n,
    gte: n => k => k >= n,
}
