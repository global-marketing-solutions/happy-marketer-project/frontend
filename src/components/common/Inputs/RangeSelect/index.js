import React, { useState, useEffect } from 'react';
import { Inputs } from '../..';
import './RangeSelect.css';

const events = {
    startEvent: ['mousedown', 'touchstart', 'pointerdown'],
    moveEvent: ['mousemove', 'touchmove', 'pointermove'],
    endEvent: ['mouseup', 'touchend', 'pointerup']
};

function RangeSelect({
    min,
    max,
    value,
    showInputs = true,
    onChange = () => { }
}) {
    const [innerValue, setInnerValue] = useState(value || [min, max]);
    useEffect(() => {
        onChange(innerValue);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [innerValue])

    if (!value) return;

    const onInnerChange = (newInnerValue) => {
        setInnerValue(newInnerValue);
    }
    const labelStyle = (isRight = false) => ({
        marginLeft: `${isRight ? 10 : 0}px`,
        marginRight: `${isRight ? 0 : 10}px`,
    })
    const limits = [min, ...innerValue, max];
    const updateValue = (index, val) => {
        onInnerChange([
            ...(innerValue.slice(0, index)),
            val < limits[index] ? limits[index] : val > limits[index + 2] ? limits[index + 2] : val,
            ...(innerValue.slice(index + 1, innerValue.length + 1))
        ])
    }
    const mouseDownHandler = (mouseDownEvent, ind) => {
        const startPosition = mouseDownEvent.pageX;
        const list = e => {
            const val = innerValue[ind] - (startPosition - e.pageX);
            updateValue(ind, val)
        };
        document.addEventListener(events.moveEvent[0], list);
        document.addEventListener(events.endEvent[0], () => {
            document.removeEventListener(events.moveEvent[0], list);
        });
        if (mouseDownEvent.preventDefault) mouseDownEvent.preventDefault();
    }
    return (
        <div className='RangeSelect'>
            {showInputs && (<div className='inputs-wrapper'>
                {innerValue.map((val, ind) => {
                    return (<Inputs.Input
                        key={`inp${ind}`}
                        classAdditional='line noMargin'
                        type={'number'}
                        value={val}
                        onBlur={v => updateValue(ind, +v)}
                        onSubmit={v => updateValue(ind, +v)}
                    />);
                })}
            </div>)}

            <div className='line-wrapper'>
                <div style={labelStyle()}>{min}</div>
                <div className='range-line'>
                    <div className='bubble'
                        style={{left: `${Math.floor((innerValue[0] - min) / (max - min + 5) * 100)}%`}}
                        onMouseDown={(e) => mouseDownHandler(e, 0)}
                    ></div>
                    <div className='filled-line'
                        style={{
                            left: `${Math.floor((innerValue[0] - min) / (max - min) * 100)}%`,
                            right: `${100 - Math.floor((innerValue[1] - min) / (max - min) * 100)}%`,
                        }}
                    ></div>
                    <div className='bubble'
                        style={{left: `${Math.floor((innerValue[1] - min) / (max - min + 5) * 100)}%`}}
                        onMouseDown={(e) => mouseDownHandler(e, 1)}
                    ></div>
                </div>
                <div style={labelStyle(true)}>{max}</div>
            </div>
        </div>
    )
}

export default RangeSelect;
