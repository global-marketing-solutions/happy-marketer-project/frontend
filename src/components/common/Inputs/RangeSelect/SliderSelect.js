import React, { useState, useEffect } from 'react';
import './SliderSelect.css';

const events = {
    startEvent: ['mousedown', 'touchstart', 'pointerdown'],
    moveEvent: ['mousemove', 'touchmove', 'pointermove'],
    endEvent: ['mouseup', 'touchend', 'pointerup']
};

function SliderSelect({
    min = 0,
    max = 100,
    value = min,
    onChange = () => { }
}) {
    const [innerValue, setInnerValue] = useState(value || min);
    useEffect(() => {
        onChange(innerValue);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [innerValue])
    const labelStyle = (isRight = false) => ({
        marginLeft: `${isRight ? 10 : 0}px`,
        marginRight: `${isRight ? 0 : 10}px`,
    })
    const updateValue = (val) => {
        setInnerValue(val < min ? min : val > max ? max : val)
    }
    const mouseDownHandler = (mouseDownEvent) => {
        const startPosition = mouseDownEvent.pageX;
        const list = e => {
            const val = innerValue - (startPosition - e.pageX);
            updateValue(val)
        };
        document.addEventListener(events.moveEvent[0], list);
        document.addEventListener(events.endEvent[0], () => {
            document.removeEventListener(events.moveEvent[0], list);
        });
        if (mouseDownEvent.preventDefault) mouseDownEvent.preventDefault();
    }
    return (
        <div className='SliderSelect'>
            <div className='line-wrapper'>
                <i style={labelStyle()}>remove</i>
                <div className='range-line'>
                    <div className='bubble'
                        style={{ left: `${Math.floor((innerValue - min) / (max - min) * 100)}%` }}
                        onMouseDown={(e) => mouseDownHandler(e)}
                    ></div>
                </div>
                <i style={labelStyle(true)}>add</i>
                <label>{`${innerValue}%`}</label>
            </div>
        </div>
    )
}

export default SliderSelect;
