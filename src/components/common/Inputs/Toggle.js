import React from 'react';
import './Toggle.css';

function Toggle({
    id = '',
    label = '',
    type = '',

    value = false,
    onChange = () => { },
    disabled = false
}) {
    return (
        <div className={`input-group line`}>
            {
                label && (<label className={disabled ? 'disabled' : ''}>{label}</label>)
            }
            <label className='switch' style={{margin: '4px'}}>
                <input
                    type={'checkbox'}
                    disabled={disabled}
                    checked={value}
                    onChange={e => onChange(e.target.value)}
                />
                <span className='slider round'></span>
            </label>
        </div>
    );
}

export default Toggle;
