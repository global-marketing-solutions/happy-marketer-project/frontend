import React, { useState, useEffect } from 'react';

import { Button } from '../index';

import countries from './countriesList';
import './countries.css';

import Select, { components } from 'react-select';

import _ from 'lodash';

const SingleValue = ({ children, ...props }) => {
    return <components.SingleValue {...props}>
        {props.data.className && <div className={`flag ${props.data.className}`} alt={props.data.name} />}
        {children}
    </components.SingleValue>
};

const Option = ({ children, ...props }) => {
    return <components.Option {...props}>
        {props.data.className && <div className={`flag ${props.data.className}`} alt={props.data.name} />}
        {children}
    </components.Option>
};

function SelectInput({
    placeholder,
    options,
    value,
    onChange,
    disabled,
    fullWidth = false,
}) {
    const updatedOptions = options.map(option => {
        const country = _.find(countries, c => c.name === option.label);
        return country ? {
            ...option,
            label: `${option.label}`,
            className: `${country.className}`,
        } : option;
    });
    const customStyles = {
        container: (provided, state) => {
            return {
                ...provided,
                flex: '1',
                // width: fullWidth ? '100%' : '92%',
                minWidth: '93px',
                fontSize: '14px'
            }
        },
        indicatorsContainer: (provided, state) => {
            return {
                ...provided,
                padding: '0 8px'
            }
        },
        dropdownIndicator: (provided, state) => {
            return {
                ...provided,
                padding: '0px'
            }
        },
        indicatorSeparator: () => ({ display: 'none' }),
        control: (provided, state) => {
            return {
                ...provided,
                minHeight: '20px',
                height: '32px',

                border: `1px solid #aaa`,
                padding: '1px 1px'
            }
        },
        option: (provided, opt) => {
            return {
                ...provided,
                display: 'flex', flexFlow: 'row',
                height: '35px'
            }
        },
        singleValue: (provided, opt) => {
            return {
                ...provided,
                display: 'flex', flexFlow: 'row'
            }
        }
    }

    return (
        <Select
            key={value}
            {...(!!placeholder ? { placeholder } : {})}
            value={updatedOptions.find(a => a.value === value)}
            options={updatedOptions}
            styles={customStyles}
            onChange={e => onChange(e.value)}
            width='200px'
            isDisabled={disabled}
            components={{ SingleValue, Option }}
            theme={(theme) => ({
                ...theme,
                colors: {
                    ...theme.colors,
                    primary: '#ddd',
                    primary25: '#eee',
                    primary50: '#ddd',
                    primary75: '#ddd'
                }
            })}
        />
    )
}


function SelectInputOld({
    placeholder,
    options,
    value,
    onChange,
    disabled,
}) {
    const updatedOptions = options.map(option => {
        const country = _.find(countries, c => c.name === option.label);
        return country ? {
            ...option,
            label: `${country.emoji} ${option.label}`
        } : option;
    });
    return (
        <select
            key={`${value}`}
            value={value || ''}
            onChange={e => onChange(e.target.value)}
            disabled={disabled}
        >
            {!!placeholder && (<option hidden>{placeholder}</option>)}
            {updatedOptions.map((opt, ind) => {
                return (<option
                    key={`${opt.value}_opt_${ind}`}
                    value={opt.value}
                    disabled={opt.disabled}
                >{opt.label}</option>)
            })}
        </select>
    )
}

function Select1({
    style = {},
    classAdditional = '',
    label = '',
    placeholder = '',
    options = [],
    isMultiple = false,
    minCountOfValues = 1,
    addLabel = 'ADD',
    value = null,
    onChange,
    showAddLabel = true,
    disabled: outDisabled,
    isOld = false,
    onDelete,
    hiddenDelete = false,
    fullWidth = false,
}) {
    const [localValue, setLocalValue] = useState(value)
    const disabled = outDisabled || _.isEmpty(options);
    useEffect(() => {
        setLocalValue(value)
    }, [value]);
    const SelectComponent = isOld ? SelectInputOld : SelectInput;
    return (
        <div className={`input-group ${classAdditional}`} style={style}>
            {!!label && (<label>{label}</label>)}
            {isMultiple || Array.isArray(localValue) ? (
                localValue.map((v, index) => (
                    <div
                        className='select-wrapper'
                        key={`${v}${index}`}
                    >
                        <SelectComponent
                            placeholder={placeholder}
                            options={options.filter(opt => !localValue.includes(opt.value) || opt.value === v)}
                            value={v}
                            onChange={(val) => onChange([...localValue.slice(0, index), val, ...localValue.slice(index + 1)])}
                            disabled={disabled}
                            fullWidth={fullWidth}
                        />
                        {!fullWidth && <div
                            className={`delete-button ${index > minCountOfValues - 1 && !disabled ? '' : 'hidden'}`}
                            onClick={() => index > minCountOfValues - 1 && !disabled && onChange([...localValue.slice(0, index), ...localValue.slice(index + 1)])}
                        ></div>}
                    </div>
                ))
            ) :
                <div className='select-wrapper'>
                    <SelectComponent
                        placeholder={placeholder}
                        options={options}
                        value={localValue}
                        onChange={onChange}
                        disabled={!!disabled}
                        fullWidth={fullWidth}
                    />
                    {!fullWidth && <div
                        className={`delete-button ${hiddenDelete || !onDelete ? 'hidden' : ''}`}
                        onClick={hiddenDelete || !onDelete ? () => { } : () => onDelete()}
                    ></div>}
                </div>
            }

            {isMultiple && showAddLabel && !disabled && (<div className='input-group' style={{ marginTop: 10 }}>
                <Button label={addLabel}
                    centered={true}
                    disabled={(localValue || []).length >= (options || []).length}
                    onPress={() => {
                        onChange([...localValue, ''])
                    }} />
            </div>)}
        </div>
    );
}

export default Select1;
