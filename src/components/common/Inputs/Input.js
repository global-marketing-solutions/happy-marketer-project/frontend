import React, { useState, useEffect } from 'react';
import { Regexes } from '../Regexes'
import './Input.css'

function Input({
    id = '',
    label = '',
    type = '',
    classAdditional = '',
    placeholder = 'Input...',
    error = '',

    pattern = Regexes.someText,
    min,
    max,

    value = '',
    onChange = () => { },
    onBlur = () => { },
    onSubmit = () => { },
    disabled = false,
    after = ''
}) {
    const [innerValue, setInnerValue] = useState(value);
    useEffect(() => {
        setInnerValue(value);
    }, [value])
    return (
        <div className={`input-group ${classAdditional} ${after ? 'after' : ''}`}>
            {
                label && (<label className={disabled ? 'disabled' : ''}>{label}</label>)
            }
            <input
                style={{ border: error && '1px solid red' }}
                {...(!!type ? { type } : {})}
                disabled={disabled}
                placeholder={placeholder}
                value={innerValue}
                onChange={e => {
                    setInnerValue(e.target.value)
                    onChange(e.target.value)
                }}
                onBlur={e => onBlur(e.target.value)}
                onSubmit={e => onSubmit(e.target.value)}
                pattern={pattern}
                onKeyDown={(e) => e.key === 'Enter' && onSubmit(e.target.value)}
                min={min}
                max={max}
            />
            {
                error && (<p style={{ color: 'red', position: 'absolute', margin: '27px 0 0 9px' }}>{error}</p>)
            }
            {
                after && <span>{after}</span>
            }
        </div>
    );
}

export default Input;
