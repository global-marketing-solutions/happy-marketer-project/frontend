import Input from './Input';
import Select from './Select';
import Toggle from './Toggle';
import RangeSelect from './RangeSelect';
import SliderSelect from './RangeSelect/SliderSelect';

export default {
    Input,
    Select,
    Toggle,
    RangeSelect,
    SliderSelect
}