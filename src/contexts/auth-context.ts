import React from 'react';
import { prod } from '../config.json'

class IdToken {
    jwtString: string;
    jwtObject: any;

    constructor(jwt: string) {
        this.jwtString = jwt;
        this.set(jwt);
    }

    set(jwt: string) {
        if (jwt) {
            this.jwtString = jwt;
            localStorage.setItem('id_token', jwt);
            const payloadString = this['jwtString'].split('.')[1];
            const jsonString = atob(payloadString);
            this.jwtObject = JSON.parse(jsonString);
        }
    }

    get isExpired() {
        const secondsEnoughForHTTPCall = 3;
        return this['jwtObject']['exp'] < Date.now() / 1000 - secondsEnoughForHTTPCall;
    }

    async refresh() {
        const resp = await fetch(
            'https://marketegy.auth.us-east-1.amazoncognito.com/oauth2/token', {
            'method': 'POST',
            'headers': {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            'body': 'grant_type=refresh_token' +
                '&client_id=' + prod.client_id +
                '&refresh_token=' + localStorage.getItem('refresh_token')
        });
        const resp_json = await resp.json();
        this.set(resp_json['id_token']);
        return resp_json['id_token'];
    }

    toString() {
        if (this.isExpired) {
            return this.refresh();
        }
        return Promise.resolve(this.jwtString);
    }
}

// let token: IdToken | null = null;

const getCurrentToken = async () => {
    const id_token = localStorage.getItem('id_token');
    if (id_token) {
        // token = new IdToken(id_token);
        return new IdToken(id_token);
    } else {
        const m = window.location.search.match(/code=(.+)/);
        if (!m) {
            const parameters = `client_id=${prod.client_id}&response_type=code&redirect_uri=${prod.redirect_uri}`;
            window.location.href = `https://${prod.cognitoHost}/login?${parameters}`;
        }
        const code = (m || [])[1];
        return await fetch('https://marketegy.auth.us-east-1.amazoncognito.com/oauth2/token', {
            'method': 'POST',
            'headers': {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            'body': 'grant_type=authorization_code' +
                '&client_id=' + prod.client_id +
                '&code=' + code +
                '&redirect_uri=' + prod.redirect_uri
        })
            .then(resp => resp.json())
            .then(resp => {
                if (resp['refresh_token']) {
                    localStorage.setItem('refresh_token', resp['refresh_token']);
                    return new IdToken(resp['id_token']);
                }
            })
    }
}

interface User {
    id: string;
    login: string;
    domain: string;
    email: string;
    name: string;
    pictureUrl: string;
}

export const getCurrentUser = async (): Promise<User> => {
    const j = (await getCurrentToken())?.jwtObject || {};
    return {
        id: j['sub'] || '',
        login: (j['email'] || '').replace(/@.+/, ''),
        domain: (j['email'] || '').replace(/.+?@/, ''),
        email: j['email'] || '',
        name: j['name'] || '',
        pictureUrl: `https://marketegy--avatars.s3.amazonaws.com/${j['sub'] || ''}?hash=${Math.floor(Math.random() * 1000)}`
    }
}

const AuthContext = React.createContext({
    user: {},
    updateUserPicture: () => { },
});

export const getToken = async () => {
    return (await getCurrentToken())?.toString()
}

export default AuthContext;
