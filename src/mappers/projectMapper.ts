import _ from 'lodash';
import specReduce from '../utils/specReduce';
import { CompetitorModelDTO, CompetitorModel, competitorDtoToCompetitor } from './positioningMapper';

interface ProjectParamsDTO {
    Countries?: string[],
    Category?: string,
    Segment?: string,
    PriceIndexRange?: { Value: [number, number], Percent: number },
    Channel?: string,
    Specs?: string[] | null

    Brand?: string,
    Restrictions?: { specId: '', value: string | number }[],
    Models?: number,

    Price?: number,
    MsrpVsMgp: number,
    Currency?: string,
    Competitors?: string[],

    SelectedStateIds?: {
        Step2: string[],
        Step3: string[],
    }

}

export interface ModelDTO {
    Fin: {
        string: string
    },
    Specs: {
        string: string
    },
    Sums: {
        string: string
    },
    Price: string,
    Msrp: string
}

interface SharedValueDTO {
    humanName: string,
    mode: 'view' | 'edit'
}
interface SharedDTO {
    [key: string]: SharedValueDTO
}

interface BusinessCaseDTO {
    Financials: string[],
    Specs: string[],
    Kpi: string,
    Period: string
}

interface Summary {
    SelFinParams: string[],
    SelKpi: string[],
    SelSpecs: string[],
}

export interface ProjectDTO {
    Name: string,
    OwnerUsername: string,
    OwnerName: string,
    Shrd: SharedDTO | null,
    StartDate: string,
    DueDate: string,
    Status: string,
    Params: ProjectParamsDTO | null,
    Models: ModelDTO[] | null,
    Competitors: CompetitorModelDTO[],
    Businesscase?: BusinessCaseDTO | null,
    Summary?: Summary | null
};

export interface ProjectParams {
    countries?: string[],
    category?: string,
    segment?: string,

    priceIndexRange?: { Value: [number, number], Percent: number },

    specs?: string[],
    channel?: string,

    brand?: string,
    restrictions?: { specId: '', value: string | number }[],
    numberOfModels?: number,

    priceIndex?: number,
    msrpVsMgp: number,
    currency?: string,
    competitors?: string[],

    selectedStateIds?: {
        step2: string[],
        step3: string[],
    }
}

export interface Shared {
    username: string,
    name: string,
    mode: 'view' | 'edit'
}

export interface ModelSpec {
    id: string,
    name: string,
    value: string,
}

export interface ModelFinancial {
    discounts_p?: number,
    tm_discounts_p?: number,
    tpa_p?: number,
    product_cost?: number,
    f_w?: number,
    custom_p?: number
}

export interface ModelSums {
    volume: number[],
    value: number[],
    margin: number[]
}

export interface Model {
    id: string,
    name: string,
    specs: ModelSpec[],
    price: number | null,
    msrp: number | null,
    changedSpecs?: ModelSpec[],
    changedPrice?: number | null,
    changedMsrp?: number | null,
    toDelete?: boolean | null
    financials?: ModelFinancial | null,
    sums?: ModelSums | null
}

export interface BusinessCase {
    financials: string[],
    specs: string[],
    kpi: string,
    period: string
}

export interface Project {
    name: string,
    owner: string,
    shared: Shared[] | null,
    startDate: string,
    dueDate: string,
    status: string,
    params: ProjectParams | null,
    models: Model[] | null,
    competitors: CompetitorModel[] | null,
    businessCase: BusinessCase | null,
    summary: Summary | null
}

const sharedDtoToShared = (dto: SharedDTO): Shared[] => {
    return _.map(_.keys(dto), key => {
        return {
            username: key,
            name: dto[key].humanName,
            mode: dto[key].mode
        }
    })
}

const projectParamsDtoToProjectParams = (dto: ProjectParamsDTO): ProjectParams => {
    return {
        countries: dto.Countries,
        category: dto.Category,
        segment: dto.Segment,

        priceIndex: dto.Price,
        msrpVsMgp: dto.MsrpVsMgp,
        priceIndexRange: dto.PriceIndexRange && dto.PriceIndexRange,

        channel: dto.Channel,
        specs: dto.Specs || [],

        brand: dto.Brand,
        restrictions: dto.Restrictions,
        numberOfModels: dto.Models,
        currency: dto.Currency,
        competitors: dto.Competitors,

        selectedStateIds: dto.SelectedStateIds ? {
            step2: dto.SelectedStateIds.Step2,
            step3: dto.SelectedStateIds.Step3,
        } : undefined
    }
}

const modelFinancialDtoToModelFinancial = (dto: any): ModelFinancial => {
    return {
        discounts_p: dto.Fin?.discounts_p || NaN,
        custom_p: dto.Fin?.custom_p || NaN,
        f_w: dto.Fin?.f_w || NaN,
        product_cost: dto.Fin?.product_cost || NaN,
        tm_discounts_p: dto.Fin?.tm_discounts_p || NaN,
        tpa_p: dto.Fin?.tpa_p || NaN,
    }
}

const getKpiArray = (dto: any, kpi: string): number[] => {
    return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0].reduce((acc: number[], i, ind) => {
        return [...acc, +_.get(dto, `${kpi}_${ind}`, NaN)]
    }, [])
}
const modelKpiDtoToModelSums = (dto: any): ModelSums => {
    return {
        volume: getKpiArray(dto.Sums, 'vol'),
        value: getKpiArray(dto.Sums, 'val'),
        margin: getKpiArray(dto.Sums, 'mar')
    }
}

const projectModelsDtoToProjectModels = (dto: ModelDTO[]): Model[] => {
    return _.map(dto, (m, i) => {
        const model = {
            id: `Model #${i + 1}`,
            name: '',
            specs: specReduce(m.Specs),
            price: m.Price ? +m.Price : null,
            msrp: m.Msrp ? +m.Msrp : null,
            financials: modelFinancialDtoToModelFinancial(m),
            sums: modelKpiDtoToModelSums(m),
        };
        return model;
    })
}

const projectBusinessCaseDtoToProjectBusinessCase = (dto: BusinessCaseDTO): BusinessCase => {
    return {
        financials: dto.Financials,
        kpi: dto.Kpi,
        period: dto.Period,
        specs: dto.Specs
    }
}

export const projectDtoToProject = (dto: ProjectDTO): Project => {
    return {
        name: dto.Name,
        owner: dto.OwnerUsername,
        shared: dto.Shrd ? sharedDtoToShared(dto.Shrd) : null,
        startDate: dto.StartDate,
        dueDate: dto.DueDate,
        status: dto.Status,
        params: dto.Params ? projectParamsDtoToProjectParams(dto.Params) : null,
        models: dto.Models ? projectModelsDtoToProjectModels(dto.Models) : null,
        competitors: dto.Competitors ? dto.Competitors.map(competitorDtoToCompetitor) : null,
        businessCase: dto.Businesscase ? projectBusinessCaseDtoToProjectBusinessCase(dto.Businesscase) : null,
        summary: dto.Summary || null
    }
}
