import _ from 'lodash';
import { ModelSpec } from './projectMapper';
import specReduce from '../utils/specReduce';

export function noop() { return _.noop }

export interface ModelSpecDTO {
    [key: string]: string
}

export interface ModelDTO {
    AverageSalesValue: number,
    Specs: ModelSpecDTO[]
}

export interface CompetitorModelDTO {
    Rank?: number | string,
    Id?: string,
    Brand: string,
    Model: string,
    ChangedOnRow?: string,
    Distribution: string,
    TOS: string,
    Price: string,
    Specs: ModelSpecDTO
}

export interface PositioningDTO {
    Models: ModelDTO[],
    Competitors: CompetitorModelDTO[],
    CompetitorsLessRanked: CompetitorModelDTO[]
}

export interface CompetitorModel {
    id: string,
    name: string,
    brand: string,
    changedOnRow?: string,
    specs: ModelSpec[],
    params: ModelSpec[],
    price: number | null,
}

interface PositioningModel {
    price: number,
    specs: ModelSpec[]
}

export interface Positioning {
    models: PositioningModel[],
    competitors: CompetitorModel[] | [],
    competitorsLessRanked: CompetitorModel[] | []
}

export function competitorDtoToCompetitor(dto: CompetitorModelDTO): CompetitorModel {
    return {
        id: dto.Model,
        name: dto.Brand,
        brand: dto.Brand,
        changedOnRow: dto.ChangedOnRow,
        specs: specReduce(dto.Specs),
        // @ts-ignore
        params: specReduce(_.pick(dto, ['Rank', 'Distribution', 'TOS'])),
        price: +dto.Price
    }
}

export function positioningDtoToPositioning(dto: PositioningDTO): Positioning {
    return {
        models: dto.Models.map(posModelDto => {
            return {
                price: posModelDto.AverageSalesValue,
                // @ts-ignore
                specs: specReduce(posModelDto.Specs)
            }
        }),
        competitors: dto.Competitors ? dto.Competitors.map(competitorDtoToCompetitor) : [],
        competitorsLessRanked: dto.CompetitorsLessRanked ? dto.CompetitorsLessRanked.map(competitorDtoToCompetitor)
                                                         : []
    }
}
