export interface DocumentDTO {
    Name: string,
    OwnerUsername: string,
    Shared: Shared[] | null,
    StartDate: string,
    DueDate: string,
    Status: string,

    Params: DocumentParamsDTO | null,

    Models: ModelDTO[]
}

export interface Document {
    Name:          string,
    OwnerUsername: string,
    StartDate:     string,
    DueDate:       string,
    Params:        DocumentParams | null,
    Models:        Model[] | null
}

export interface DocumentParams {
    Brand:          string,
    Currency:       string,
    TypeOfPrice:    string,
    Specs:          ParamsSpecs[],
    SpecsPercentsX: number[]
    SpecsPercentsY: number[]
}

export interface DocumentParamsDTO extends DocumentParams {

}

interface ParamsSpecs {
    Name:   string,
    Values: string[]
}

export interface ModelDTO {
    Id: string,
    Specs: {[key: string]: string},
    Price: number
}

export interface Model {
    id: string,
    specs: ModelSpec[],
    price: number,
}

export interface ModelSpec {
    id:    string,
    name:  string,
    value: string,
}

export interface Shared {
    Username: string,
    Name: string,
    Mode: 'view' | 'edit'
}

export function documentDtoToDocument(dto: DocumentDTO): Document {
    return {
        ...dto,
        Models: dto.Models && dto.Params?.Specs ? documentModelsDtoToDocumentModels(dto.Models, dto.Params.Specs) : null,
    }
}

function documentModelsDtoToDocumentModels(modelsDto: ModelDTO[], specs: ParamsSpecs[]): Model[]{
    return modelsDto.map(m => ({
        id: m.Id,
        specs: specs.reduce((acc, val) => {
            acc.push({
                id: val.Name,
                name: val.Name,
                value: m.Specs[val.Name]
            })
            return acc
        }, [] as ModelSpec[]),
        price: m.Price
    }))
}
