import { ModelSpec } from './projectMapper';
import { ModelSpecDTO } from '../mappers/positioningMapper';
import specReduce from '../utils/specReduce';

interface RowsDTO {
    [key: string]: string[]
}

interface SegmentDTO {
    X: string,
    Y: string,
    Percent: number
}

interface LineUpMapDTO {
    Segments: SegmentDTO[]
    Models: {
        Specs: ModelSpecDTO
    }[]
}

export interface LineUpMap {
    segments: Segment[],
    models: {
        id: string,
        specs: ModelSpec[]
    }[]
}

export interface Row {
    name: string,
    percent: number
}

export interface Segment {
    x: string,
    y: string,
    percent: number
}

export function lineupmapDtoTolineupmap(dto: LineUpMapDTO): LineUpMap {
    return {
        segments: dto.Segments.map(segment => {
            return {
                x: segment.X,
                y: segment.Y,
                percent: segment.Percent
            }
        }),
        models: dto.Models.map((modelDTO, ind) => {
            return {
                id: `Model #${ind + 1}`,
                specs: specReduce(modelDTO.Specs)
            }
        })
    }
}
