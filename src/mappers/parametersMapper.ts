import specReduce from '../utils/specReduce';

interface PossibleSpecsDTO {
    [key: string]: string[]
}

export interface ParametersDTO {
    Countries: string[],
    Categories: string[] | null,
    Segments: string[] | null,
    Channels: string[] | null,
    Currencies: string[] | null,
    Competitors: string[] | null,
    SpecsAllPossible: PossibleSpecsDTO,

    PriceIndexRange: priceIndexRange
    PricePercent: number
}

export interface ParameterValue {
    id: string,
    name: string,
}

export interface priceIndexRange {
    Min: number,
    Max: number,
    Percent: number
}

export interface Parameter<T = ParameterValue[] | null> {
    id: string,
    name: string,
    values: T
}

export interface Spec {
    id: string,
    name: string,
    value: string[],
}

export interface Parameters {
    countries: Parameter<ParameterValue[]>,
    categories: Parameter,
    segments: Parameter,
    channels: Parameter,
    currencies: Parameter,
    competitors: Parameter,
    specs: Spec[],

    priceIndexRange: priceIndexRange,
    pricePercent: number
}

function parameterValuesBuilder(values: string[]): ParameterValue[] {
    return values.map((value: string) => ({
        id: value.toLowerCase(),
        name: value
    }));
}

function parameterBuilder<T = ParameterValue[] | null>(name: string, values: T): Parameter<T> {
    return {
        id: name.toLowerCase(),
        name: name,
        values,
    };
}

function possibleSpecsDtoToPossibleSpecs(dto: PossibleSpecsDTO): Spec[] {
    // @ts-ignore
    return specReduce(dto).map((s: Spec): Spec => {
        return {
            id: s.id,
            name: s.name,
            value: ([...s.value] || []).sort((a, b) => a > b ? 1 : -1)
        }
    })
}

export function parametersDtoToParameters(dto: ParametersDTO): Parameters {
    return {
        countries: parameterBuilder<ParameterValue[]>('Countries', parameterValuesBuilder(dto.Countries)),
        categories: parameterBuilder('Categories', parameterValuesBuilder(dto.Categories || [])),
        channels: parameterBuilder('Channels', parameterValuesBuilder(dto.Channels || [])),
        competitors: parameterBuilder('Competitors', parameterValuesBuilder(dto.Competitors || [])),
        currencies: parameterBuilder('Currencies', parameterValuesBuilder(dto.Currencies || [])),
        segments: parameterBuilder('Segments', parameterValuesBuilder(dto.Segments || [])),
        specs: possibleSpecsDtoToPossibleSpecs(dto.SpecsAllPossible),

        priceIndexRange: dto.PriceIndexRange,
        pricePercent: dto.PricePercent
    }
}
