import React, { useState, useEffect } from 'react';
import Header from './components/header/Header';
import Menu from './components/menu/Menu';
import AppSwitch from './components/AppSwitch';
import LoaderScreen from './components/EmptyScreen/LoaderScreen';
import { BrowserRouter as Router } from "react-router-dom";
import './App.css';
import AuthContext, { getCurrentUser } from './contexts/auth-context';

const getUser = (setUser) => getCurrentUser().then(user => setUser(user));

function App() {
  const [user, setUser] = useState(null);

  useEffect(() => {
    getUser(setUser)
  }, [])

  return (
    <Router>
      <AuthContext.Provider value={{ user, updateUserPicture: () => getUser(setUser) }}>
        {user && user.email ? (
          <div className="App">
            <div className="menu">
              <Menu />
            </div>
            <div className="wrapper">
              <div className="header">
                <Header />
              </div>
              <div className="content">
                <AppSwitch />
              </div>
            </div>
          </div>
        ) : (<LoaderScreen />)}
      </AuthContext.Provider>
    </Router>
  );
}

export default App;
