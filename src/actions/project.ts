import { prod } from '../config.json';
import { ProjectDTO, Project, projectDtoToProject, ProjectParams, BusinessCase } from '../mappers/projectMapper';
import { getHeaders } from './headers';
import { logError } from './logError';
import { saveModels } from './models'

export async function getProjects(archived = false): Promise<Project[]> {
    try {
        const getProjectsUrl = `projects${archived ? '?status=Archived' : ''}`;
        const res = await fetch(prod.stage + getProjectsUrl, { headers: await getHeaders() })
        const projectsRes = await res.json();
        return projectsRes.map((projectDto: ProjectDTO) => projectDtoToProject(projectDto));
    } catch (error) {
        logError('ERROR LOADING PROJECTS:', error)
    }
    return [];
}

export async function getProject(projectId: string, archived = false): Promise<Project | null> {
    try {
        const projects = await getProjects(archived);
        return projects.find(project => project.name === projectId) || null;
    } catch (error) {
        logError('ERROR LOADING PROJECT:', error)
    }
    return null;
}

export async function saveProjectParams(name: string, owner: string, parameters: ProjectParams): Promise<boolean> {
    try {
        const headers = await getHeaders();

        await fetch(`${prod.stage}projects/${name}/parameters?ownerUsername=${owner}`, {
            method: 'PUT',
            headers,
            body: JSON.stringify({
                countries: parameters.countries,
                category: parameters.category,
                segment: parameters.segment,
                priceIndexRange: parameters.priceIndexRange ? parameters.priceIndexRange : null,
                channel: parameters.channel,
                specs: parameters.specs,
                brand: parameters.brand,
                models: +(parameters.numberOfModels || 0),
                restrictions: parameters.restrictions,
                price: +(parameters.priceIndex || 0),
                msrpVsMgp: +parameters.msrpVsMgp,
                currency: parameters.currency,
                competitors: parameters.competitors,

                selectedStateIds: parameters.selectedStateIds
            })
        });
    } catch (error) {
        logError('ERROR SAVING PROJECT PARAMS', error)
    }
    return true;
}

export async function saveProject(project: Project): Promise<boolean> {
    try {
        project.params && await saveProjectParams(project.name, project.owner, project.params);
        await saveModels(project, project.models || []);
    } catch (error) {
        logError('ERROR SAVING PROJECT', error)
    }
    return true;
}

export async function saveProjectBusinessCase(name: string, owner: string, businessCase: BusinessCase): Promise<boolean> {
    try {
        const headers = await getHeaders();

        await fetch(`${prod.stage}projects/${name}/businesscase?owner=${owner}`, {
            method: 'PUT',
            headers,
            body: JSON.stringify(businessCase)
        });
    } catch (error) {
        logError('ERROR SAVING PROJECT PARAMS', error)
    }
    return true;
}
