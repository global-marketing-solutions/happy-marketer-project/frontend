import _ from 'lodash';
import { prod } from '../config.json';
import { getHeaders } from './headers';
import { ProjectParams } from '../mappers/projectMapper';
import { LineUpMap, lineupmapDtoTolineupmap } from '../mappers/lineupmapMapper';

let abortController = new AbortController();

export async function getLineUpMap(parameters: ProjectParams): Promise<LineUpMap | null> {
    abortController.abort();
    abortController = new AbortController();

    const prms = {
        'countries': JSON.stringify(parameters.countries || []),
        'category': parameters.category || '',
        'segment': parameters.segment || '',
        'channel': parameters.channel || '',
        'specs': JSON.stringify(parameters.specs || []),
        'restrictions': JSON.stringify(parameters.restrictions || []),
        'numberOfModels': `${parameters.numberOfModels || 0}`,
    }

    if (parameters.priceIndexRange) {
        (prms as any).priceRange = JSON.stringify([
            parameters.priceIndexRange.Value[0] * parameters.priceIndexRange.Percent | 0,
            parameters.priceIndexRange.Value[1] * parameters.priceIndexRange.Percent | 0
        ])
    }

    const a = _.pick(parameters, Object.keys(prms));

    _.mapValues(a, element => {
        if (_.isNil(element)) return;
    });

    try {
        const res = await fetch(
            prod.stage + 'lineupmaps?' + new URLSearchParams(prms),
            { headers: await getHeaders(), signal: abortController.signal }
        )
        let lineupmaps = await res.json()
        return lineupmapDtoTolineupmap(lineupmaps);
    } catch {
        return null;
    }
}
