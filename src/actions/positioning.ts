import { getHeaders } from './headers';
import { logError } from './logError';
import { Positioning, positioningDtoToPositioning } from '../mappers/positioningMapper';
import { prod } from '../config.json';


export async function getPositioning(owner: string, projectName: string, pr: string): Promise<Positioning | null> {
    try {
        const headers = await getHeaders();
        const res = await fetch(
            `${prod.stage}/projects/${projectName}/positioning?owner=${owner}&priceRange=${pr}`, {
            headers
        });
        const result = await res.json()
        return positioningDtoToPositioning(result)

    } catch (error) {
        logError('ERROR SAVING MODELS', error)
    }

    return null
}
