import _ from 'lodash';
import { prod } from '../config.json';
import { getHeaders } from './headers';
import { Model, ModelFinancial, ModelSums, Project } from '../mappers/projectMapper';
import { logError } from './logError';

function getFinancialObject(modelFinancial: ModelFinancial) {
    return {
        ...(modelFinancial.discounts_p ? { discounts_p: modelFinancial.discounts_p } : {}),
        ...(modelFinancial.tm_discounts_p ? { tm_discounts_p: modelFinancial.tm_discounts_p } : {}),
        ...(modelFinancial.tpa_p ? { tpa_p: modelFinancial.tpa_p } : {}),
        ...(modelFinancial.product_cost ? { product_cost: modelFinancial.product_cost } : {}),
        ...(modelFinancial.f_w ? { f_w: modelFinancial.f_w } : {}),
        ...(modelFinancial.custom_p ? { custom_p: modelFinancial.custom_p } : {}),
    }
}

function getSumsObject(modelSums: ModelSums) {
    return {
        ...modelSums.volume.reduce((acc, vol, i) => ({...acc, [`vol_${i}`]: vol || 0}), {}),
        ...modelSums.value.reduce((acc, val, i) => ({...acc, [`val_${i}`]: val || 0}), {}),
        ...modelSums.margin.reduce((acc, mar, i) => ({...acc, [`mar_${i}`]: mar || 0}), {}),
    }
}

export async function saveModels(project: Project, models: Model[]) {
    try {
        const headers = await getHeaders();

        const prms = {
            'countries': JSON.stringify(project.params?.countries || []),
            'category': project.params?.category || '',
            'segment': project.params?.segment || '',
            'channel': project.params?.channel || '',
            'specs': JSON.stringify(project.params?.specs || []),
            'restrictions': JSON.stringify(project.params?.restrictions || []),
            'numberOfModels': `${project.params?.numberOfModels || 0}`,
        }
        const usp = new URLSearchParams(prms);

        const resp =
            await fetch(`${prod.stage}projects/${project.name}/models?ownerUsername=${project.owner}&${usp}`, {
            method: 'PUT',
            headers,
            body: JSON.stringify(models.map(model => {
                return {
                    specs: {...(model.changedSpecs || model.specs).reduce((acc, s) => {
                        return {
                            ...acc,
                            [s.name]: s.value
                        }
                    }, {})},

                    ...(model.changedPrice || model.price ? {
                        price: model.changedPrice || model.price
                    } : {}),

                    ...(model.changedPrice || model.price ? {
                        msrp: model.changedMsrp || model.msrp
                    } : {}),

                    fin: {
                            ...(model.financials && !_.isEmpty(model.financials) ?
                                getFinancialObject(model.financials) : {})
                    },
                    sums: {
                        ...(model.sums && !_.isEmpty(model.sums) ? getSumsObject(model.sums) : {})
                    }
                }
            }))
        });
        return await resp.json();
    } catch (error) {
        logError('ERROR SAVING MODELS', error)
    }
    return null;
}
