import { prod } from '../config.json';
import { getHeaders } from './headers';
import { Document, DocumentDTO, DocumentParams, Model, documentDtoToDocument } from '../mappers/documentMapper';

export async function getDocuments(archived = false): Promise<Document[]> {
    try {
        const url = `documents${archived ? '?status=Archived' : ''}`
        const resp = await fetch(prod.stage + url, { headers: await getHeaders() })
        const respJson = await resp.json()
        return respJson.map((documentDto: DocumentDTO) => documentDtoToDocument(documentDto))
    } catch (error) {
        console.log('ERROR LOADING DOCUMENT:', error)
    }
    return [];
}

export async function getDocument(documentId: string, archived = false): Promise<Document | null> {
    try {
        const documents = await getDocuments(archived)
        return documents.find(d => d.Name === documentId) || null
    } catch (error) {
        console.log('ERROR LOADING DOCUMENT:', error)
    }
    return null
}

export async function saveDocumentParams(name: string, owner: string, documentParams: DocumentParams): Promise<boolean> {
    try {
        await fetch(`${prod.stage}documents/${name}/parameters?ownerUsername=${owner}`, {
            method: 'PUT',
            headers: await getHeaders(),
            body: JSON.stringify(documentParams)
        })
    } catch (error) {
        console.log(error)
        return false
    }

    return true
}

export async function saveModels(id: string, owner: string, models: Model[]): Promise<Response> {
    return fetch(`${prod.stage}documents/${id}/models?ownerUsername=${owner}`, {
        method: 'PUT',
        headers: await getHeaders(),
        body: JSON.stringify(models.map(m => {
            return {
               ...m,
                specs: m.specs.reduce((acc, {id, value}) => {
                    acc[id] = value
                    return acc
                }, {} as {[key: string]: string}),
                price: typeof m.price === 'number' ? m.price : Object.values(m.price)[0]
           }
        }))
    })
}
