import _ from 'lodash';
import { prod } from '../config.json';
import { collectionMap, filterEmptyValues, stringifyArrays } from '../utils/arrayHelpers';
import { getHeaders } from './headers';
import { Parameters, parametersDtoToParameters } from '../mappers/parametersMapper';
import { ProjectParams } from '../mappers/projectMapper';
import { logError } from './logError';

let abortController = new AbortController();

export async function getParams(previousParameters: ProjectParams): Promise<Parameters | null> {
    abortController.abort();
    abortController = new AbortController();

    try {
        const prms = collectionMap(
            _.pickBy(previousParameters, (value, key) => {
                if (!['countries', 'category', 'segment', 'channel', 'restrictions'].includes(key)) {return false;}
                if (_.isEmpty(filterEmptyValues(value))) return false;
                return true;
            }),
            (value: any, key: any) => {
                if (['category', 'segment', 'channel'].includes(key)) return _.isArray(value) ? value[0] : value;
                return value;
            },
            filterEmptyValues,
            stringifyArrays
        );

        const res = await fetch(
            prod.stage + 'parameters?' + new URLSearchParams(prms),
            { headers: await getHeaders(), signal: abortController.signal }
        );
        const js = await res.json();

        return parametersDtoToParameters(js);
    } catch (error) {
        logError('ERROR LOADING PARAMETERS:', error)
    }
    return null;
}
