import { prod } from '../config.json';
import { getHeaders } from './headers';
import { CompetitorModelDTO, CompetitorModel } from '../mappers/positioningMapper';
import { logError } from './logError';

const competitorToCompetitorDto = (competitor: CompetitorModel): CompetitorModelDTO => {
    return {
        'Brand': competitor.brand,
        'Distribution': competitor.params.find(param => param.id === 'distribution')?.value || '',
        'Id': competitor.id,
        'Model': competitor.id,
        'ChangedOnRow': competitor.changedOnRow,
        'Price': `${competitor.price}`,
        'Rank': competitor.params.find(param => param.id === 'rank')?.value || '',
        'TOS': competitor.params.find(param => param.id === 'tos')?.value || '',
        'Specs': {
            ...competitor.specs.reduce((acc, s) => {
                return {
                    ...acc,
                    [s.name]: s.value
                }
            }, {}),
        }
    }
}

export async function saveCompetitors(name: string, ow: string, comps: CompetitorModel[] | null): Promise<boolean | null> {
    try {
        const headers = await getHeaders();
        await fetch(`${prod.stage}/projects/${name}/competitors?owner=${ow}`, {
            method: 'PUT',
            headers,
            body: comps ? JSON.stringify(comps.map(competitorToCompetitorDto)) : null
        });

        return true;
    } catch (error) {
        logError('ERROR SAVING COMPETITORS', error)
    }
    return null;
}
