import { getToken } from '../contexts/auth-context';

export async function getHeaders() {
    return {
        'Authorization': await getToken(),
    }
}